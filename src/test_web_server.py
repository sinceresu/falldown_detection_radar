#!/usr/bin/env python3
import os
import PiFiServer
from PiFiServer import pifi_web_server
from PiFiServer.wifi_controller import  WifiController
def getConfigDir():
  configDirectory = ''
  try:
      # current_dir = os.path.dirname(sys.argv[0])
      current_dir = os.getcwd()
      configDirectory = os.path.join(current_dir, 'configs')

  except:
      configDirectory = ''
  return configDirectory


if __name__ == '__main__':


    # os.system("sudo nmcli con down my-hotspot")
    # os.system("sudo nmcli radio all off")
    # time.sleep(2)
    # os.system("sudo nmcli radio all on")
    # # print("nmcli radio wwan off")
    # time.sleep(1)
    wifi_controller_ = WifiController('wlan0', 'healthtrack','www.xiaomianao.net.cn')
    config_dir = getConfigDir()

    pifi_web_server.startWeb(wifi_controller_=wifi_controller_,  config_file_ = os.path.join(config_dir, 'app_config.json'), wifi_init_page = True)                                   # Starting the Flask App and giving it permission to be accessable by all the ip addresses.
