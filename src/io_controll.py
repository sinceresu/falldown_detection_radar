#!/usr/bin/python
#-- coding:utf8 --
import subprocess
import re
import time
import os

from multiprocessing import Process

from threading import Thread


import OPi.GPIO as GPIO
import pi5B

BUTTON_ALARM_PIN = 7
AUDIO_ALARM_PIN = 11
FALL_ALARM_PIN = 3
LED_R_PIN   = 8
LED_G_PIN   = 10
LED_B_PIN   = 12
START_AUDIO_PIN   = 13
RESET_WIFI_PIN = 15

OUTPUT_PULSE_DURATION_MS = 1000 #ms
MONIOTR_PULSE_DURATION = 100 #ms


pulse_callback = None
pre_pin_value = 0

monitor_pulse_flag = False



def _outputPulse(pin, pulse_duration) :
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(pulse_duration / 1000.0)
    GPIO.output(pin, GPIO.LOW)
    


def _triggerPinPulse(pin, pulse_duration=OUTPUT_PULSE_DURATION_MS) :
    Thread(target=_outputPulse, args=(pin,pulse_duration)).start()


def initialize() :
    GPIO.setmode(pi5B.BOARD)
    GPIO.setwarnings(False)

    GPIO.setup(BUTTON_ALARM_PIN, GPIO.IN)
    GPIO.setup(AUDIO_ALARM_PIN, GPIO.IN)

    GPIO.setup(FALL_ALARM_PIN, GPIO.OUT)

    GPIO.setup(LED_R_PIN, GPIO.OUT)
    GPIO.setup(LED_G_PIN, GPIO.OUT)
    GPIO.setup(LED_B_PIN, GPIO.OUT)

    GPIO.setup(START_AUDIO_PIN, GPIO.OUT)

    GPIO.setup(RESET_WIFI_PIN, GPIO.IN)

def triggerRedLed(pulse_duration=OUTPUT_PULSE_DURATION_MS) :
    _triggerPinPulse(LED_R_PIN, pulse_duration)
                     
 
def triggerGreenLed(pulse_duration=OUTPUT_PULSE_DURATION_MS) :
    _triggerPinPulse(LED_G_PIN, pulse_duration)

 
def triggerBlueLed(pulse_duration=OUTPUT_PULSE_DURATION_MS) :
    _triggerPinPulse(LED_B_PIN, pulse_duration)


def triggerFalldownAlarm(pulse_duration=OUTPUT_PULSE_DURATION_MS) :
    _triggerPinPulse(FALL_ALARM_PIN, pulse_duration)

def triggerStartAudio(pulse_duration=OUTPUT_PULSE_DURATION_MS) :
    _triggerPinPulse(START_AUDIO_PIN, pulse_duration)


def _monitorPinEdge(pin, fall_edge, edge_cb) :
    global monitor_pulse_flag

    monitor_level = GPIO.LOW if fall_edge else GPIO.HIGH

    pre_value = GPIO.input(pin)
    monitor_pulse_flag = True

    while monitor_pulse_flag:
        current_value = GPIO.input(pin)
        if (current_value == monitor_level and current_value != pre_value) :
            if (edge_cb != None) :
                edge_cb()

        pre_value = current_value

        time.sleep(MONIOTR_PULSE_DURATION  / 1000.0 / 16)

def stopMonitorAlarm() : 
    global monitor_pulse_flag
    monitor_pulse_flag = False


def setRedLed(value) : 
    global LED_R_PIN
    if value :
        GPIO.output(LED_R_PIN, GPIO.HIGH)
    else :
        GPIO.output(LED_R_PIN, GPIO.LOW)

def setGreenLed(value) : 
    if value :
        GPIO.output(LED_G_PIN, GPIO.HIGH)
    else :
        GPIO.output(LED_G_PIN, GPIO.LOW)
    

def setBlueLed(value) : 
    if value :
        GPIO.output(LED_B_PIN, GPIO.HIGH)
    else :
        GPIO.output(LED_B_PIN, GPIO.LOW)

def setStartAudio(value) : 
    if value :
        GPIO.output(START_AUDIO_PIN, GPIO.HIGH)
    else :
        GPIO.output(START_AUDIO_PIN, GPIO.LOW)


def getResetWifiPin() : 
    return GPIO.input(RESET_WIFI_PIN)


        
def monitorButtonAlarm(button_alarm_cb) : 
    Thread(target=_monitorPinEdge, args=(BUTTON_ALARM_PIN, True, button_alarm_cb)).start()
        
def monitorAudioAlarm(audio_alarm_cb) : 
    Thread(target=_monitorPinEdge, args=(AUDIO_ALARM_PIN, True, audio_alarm_cb)).start()

def monitorResetWifi(reset_wifi_cb) : 
    Thread(target=_monitorPinEdge, args=(RESET_WIFI_PIN, True, reset_wifi_cb)).start()


def recycleLED(cycle) :

    setRedLed(True)
    time.sleep(cycle)   
    setRedLed(False)
    time.sleep(cycle)   

    setGreenLed(True)
    time.sleep(cycle)   
    setGreenLed(False)
    time.sleep(cycle)   

    setBlueLed(True)
    time.sleep(cycle)   
    setBlueLed(False)


def testLED() :
    Thread(target=recycleLED, args=(1,)).start()

# unit test

def buttonAlarmCB() :
    print("button alarmed!\n")


def audioAlarmCB() :
    print("audio alarmed!\n")


def resetWifiCB() :
    print("reset wifi triggered!\n")
    
def test_output() :
    while True:
        triggerRedLed()
        triggerGreenLed()
        triggerBlueLed()
        time.sleep(2)

        triggerFalldownAlarm()
        time.sleep(2)

        triggerStartAudio()
        time.sleep(2)

if __name__ == "__main__":

    initialize()

    monitorButtonAlarm(buttonAlarmCB)
    monitorAudioAlarm(audioAlarmCB)
    monitorResetWifi(resetWifiCB)

    test_output()