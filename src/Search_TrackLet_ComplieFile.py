from numba.pycc import CC
import numpy as np
from numba import complex128, float64, int64, boolean
from numba import njit

cc = CC('Numba_Module')


@cc.export('PhaseDiff_VS', 'f8[:, :](i8, c16[:, :, :])')
def PhaseDiff_VS(ind_bin, cmplx_Foreground_all):
    gl_numVirtAnt = cmplx_Foreground_all.shape[2]
    gl_numRangeBins = cmplx_Foreground_all.shape[1]
    Len_cmplx = cmplx_Foreground_all.shape[0]

    cmplx_Foreground = cmplx_Foreground_all[:, ind_bin, :]
    max_norm = np.abs(cmplx_Foreground)
    if ind_bin > 0 :
        norm = np.abs(cmplx_Foreground_all[:, ind_bin - 1, :])
        cmplx_Foreground =  np.where(norm > max_norm, cmplx_Foreground_all[:, ind_bin - 1, :], cmplx_Foreground)
        max_norm = np.maximum(max_norm, norm)
        
    if ind_bin < gl_numRangeBins - 1:
        norm = np.abs(cmplx_Foreground_all[:, ind_bin + 1, :])
        cmplx_Foreground =  np.where(norm > max_norm, cmplx_Foreground_all[:, ind_bin + 1, :], cmplx_Foreground)

    phase_Foreground = np.zeros((gl_numVirtAnt, Len_cmplx), dtype=np.float64)  # gl_numVirtAnt * framelen_vital
    for k_phase in range(gl_numVirtAnt):
        phase_Foreground[k_phase, :] = np.angle(cmplx_Foreground[:, k_phase])

    phase_diff = phase_Foreground[:, np.arange(1, Len_cmplx)] - \
                 phase_Foreground[:, np.arange(0, Len_cmplx - 1)]
    for k_numVirtAnt in range(gl_numVirtAnt):
        for k_diff in range(Len_cmplx - 1):
            if phase_diff[k_numVirtAnt, k_diff] > np.pi:
                phase_diff[k_numVirtAnt, k_diff] = phase_diff[k_numVirtAnt, k_diff] - 2 * np.pi
            elif phase_diff[k_numVirtAnt, k_diff] < - np.pi:
                phase_diff[k_numVirtAnt, k_diff] = phase_diff[k_numVirtAnt, k_diff] + 2 * np.pi
            else:
                phase_diff[k_numVirtAnt, k_diff] = phase_diff[k_numVirtAnt, k_diff]

    return phase_diff

# @cc.export('GetPhase', 'f8[:](i8, c16[:, :])')
# def GetPhase(ind_bin, cmplx_Foreground_all):
#     numVirtAnt = cmplx_Foreground_all.shape[1]
#     numRangeBins = cmplx_Foreground_all.shape[0]

#     cmplx_Foreground = cmplx_Foreground_all[ind_bin, :]
#     max_norm = np.abs(cmplx_Foreground)
#     if ind_bin > 0 :
#         norm = np.abs(cmplx_Foreground_all[ind_bin - 1, :])
#         cmplx_Foreground =  np.where(norm > max_norm, cmplx_Foreground_all[ind_bin - 1, :], cmplx_Foreground)
#         max_norm = np.maximum(max_norm, norm)
        
#     if ind_bin < numRangeBins - 1:
#         norm = np.abs(cmplx_Foreground_all[ind_bin + 1, :])
#         cmplx_Foreground =  np.where(norm > max_norm, cmplx_Foreground_all[ind_bin + 1, :], cmplx_Foreground)

#     phase_Foreground = np.empty((numVirtAnt,), dtype=np.float64)  # gl_numVirtAnt * framelen_vital
#     for k_phase in range(numVirtAnt):
#         phase_Foreground[k_phase] = np.angle(cmplx_Foreground[k_phase])

#     return phase_Foreground


@cc.export('PhaseDiff', 'f8[:](f8[:], f8[:])')
def unwrap( phase_Foreground, pre_phase_Foreground):
    numVirtAnt = phase_Foreground.shape[0]
 
    phase_diff = phase_Foreground - pre_phase_Foreground
    for k_numVirtAnt in range(numVirtAnt):
        if phase_diff[k_numVirtAnt] > np.pi:
            phase_diff[k_numVirtAnt] = phase_diff[k_numVirtAnt] - 2 * np.pi
        elif phase_diff[k_numVirtAnt] < - np.pi:
            phase_diff[k_numVirtAnt] = phase_diff[k_numVirtAnt] + 2 * np.pi
        else:
            phase_diff[k_numVirtAnt] = phase_diff[k_numVirtAnt]
            

    return phase_diff


@njit()
@cc.export('binom', 'i8(i8, i8)')
def binom(n, k):
    if 0 <= k <= n:
        a = 1
        b = 1
        for t in range(1, min(k, n - k) + 1):
            a *= n
            b *= t
            n -= 1
        return a // b
    else:
        return 0


@njit()
@cc.export('derivative_n2', 'f8[:](f8[:], f8, i8)')
def derivative_n2(data, dt, filter_length):
    M = int((filter_length - 1) / 2)
    m = int((filter_length - 3) / 2)
    coefs = np.zeros(M, dtype=np.float64)
    for k in range(1, M + 1):
        coefs[k - 1] = (1 / 2 ** (2 * m + 1)) * (binom(2 * m, m - k + 1) - binom(2 * m, m - k - 1))

    result = np.zeros(len(data), dtype=np.float64)
    for i in range(M, len(data) - M):
        copy_result = np.zeros(M, dtype=np.float64)
        for k in range(1, M + 1):
            copy_result[k - 1] = coefs[k - 1] * (data[i + k] - data[i - k])
        result[i] = (1 / dt) * np.sum(copy_result)
    return result

@cc.export('Noise_Suppression', 'f8[:, :](f8[:, :], i8, i8, f8)')
def Noise_Suppression(phase_diff, gl_numVirtAnt, Len_cmplx, Ts):
    # Minimize the impact of noise
    phase_diff_smoothed = np.zeros((gl_numVirtAnt, Len_cmplx - 1 - 4), dtype=np.float64)
    for k_heartbeat in range(gl_numVirtAnt):
        phase_diff_smoothed[k_heartbeat, :] = \
            derivative_n2(phase_diff[k_heartbeat, :], Ts, 5)[np.arange(2, Len_cmplx - 1 - 2)]
    return phase_diff_smoothed

#endregion



if __name__ == "__main__":
    cc.compile()
