# ----- Imports -------------------------------------------------------
# Standard Imports
import sys
import numpy as np
import time
import math
import os
import statistics
import signal
from enum import Enum
import scipy.signal as sci_signal


# Local File Imports
from gui_parser import uartParser
from gui_common import *
from  info_sender import InfoSender, Position, Box, VitalStatus, FalldownStatus
from  app_config import AppConfig, AppSetting

# gpio  = False
gpio  = True

if gpio :
  import io_controll

ALARM_LED_PULSE_DURATION_MS = 5000

class MotionStatus(Enum) :
    NOT_DETECTED = 0
    STATIC = 1
    MOVING = 2   


fallDownSeconds = 5
fallDownRefOffset = -18

noTrackTimeoutSeconds = 50
vitalDeviationTresh = 0.003
gl_numVirtAnt = 3 * 4
AoA_spacing = 1

# mov_framelen_vsign = frameRate
mov_framelen_vsign = 16
# window_vsign = 16  # Window Size for Vital Sign Recognition
# inital_window_vsign = window_vsign // 2  # Phase Difference
# initalBatchSize = inital_window_vsign * mov_framelen_vsign  # Phase Difference
# batchSize = (window_vsign + 1) * mov_framelen_vsign  # Phase Difference
circularBufferSizeBreath = 256
circularBufferSizeHeart = 512
PHASE_FFT_SIZE = 1024

CONF_METRIC_BANDWIDTH_PEAK_HEART_HZ    = 0.1    # Bandwidth around the max peak to include in the signal power estimation
CONF_METRIC_BANDWIDTH_PEAK_BREATH_HZ   = 0.2   # Bandwidth around the max peak to include in the signal power estimation
CONVERT_HZ_BPM = 60            #Converts Hz to Beats per minute


# ------------------------------------------------------------------
# Send the data from the configuration file to our IWR6843AOP radar
def parseConfigFile(configFileName):
    configParameters = {}  # Initialize an empty dictionary to store the configuration parameters

    # Read the configuration file and send it to the board
    config = [line.rstrip('\r\n') for line in open(configFileName)]
    for i in config:
        # Split the line
        splitWords = i.split(" ")

        # Hard code the number of antennas, change if other configuration is used
        # numRxAnt = 4
        numTxAnt = 3

        # Get the information about the profile configuration
        if "profileCfg" in splitWords[0]:
            startFreq = int(float(splitWords[2]))
            idleTime = float(splitWords[3])
            rampEndTime = float(splitWords[5])
            freqSlopeConst = float(splitWords[8])
            numAdcSamples = int(splitWords[10])
            numAdcSamplesRoundTo2 = 1

            while numAdcSamples > numAdcSamplesRoundTo2:
                numAdcSamplesRoundTo2 = numAdcSamplesRoundTo2 * 2

            digOutSampleRate = float(splitWords[11])

        # Get the information about the frame configuration
        elif "frameCfg" in splitWords[0]:

            chirpStartIdx = int(splitWords[1])
            chirpEndIdx = int(splitWords[2])
            numLoops = int(splitWords[3])
            framePeriodicity = float(splitWords[5])

    # Combine the read data to obtain the configuration parameters
    # numChirpsPerFrame = (chirpEndIdx - chirpStartIdx + 1) * numLoops
    configParameters["numRangeBins"] = numAdcSamplesRoundTo2
    configParameters["rangeResolutionMeters"] = (3e8 * digOutSampleRate * 1e3) / (
            2 * freqSlopeConst * 1e12 * numAdcSamples)
    configParameters["rangeIdxToMeters"] = (3e8 * digOutSampleRate * 1e3) / (
            2 * freqSlopeConst * 1e12 * configParameters["numRangeBins"])
    configParameters["maxRange"] = (300 * 0.9 * digOutSampleRate) / (2 * freqSlopeConst * 1e3)
    configParameters["maxVelocity"] = 3e8 / (4 * startFreq * 1e9 * (idleTime + rampEndTime) * 1e-6 * numTxAnt)
    configParameters["framePeriodicity"] = framePeriodicity

    rangeArray = np.array(range(configParameters["numRangeBins"])) * configParameters["rangeIdxToMeters"]

    return configParameters, rangeArray


def unwrap( rangeBinPhase, phasePrevFrame, diffPhaseCorrectionCum):

  diffPhase = rangeBinPhase - phasePrevFrame
  if diffPhase > np.pi:
    modFactorF = 1
  elif diffPhase < - np.pi:
    modFactorF = -1
  else:
    modFactorF = 0
      
  diffPhaseMod = diffPhase - modFactorF*2*np.pi
  # // preserve variation sign for +pi vs. -pi
  if ((diffPhaseMod == -np.pi) and (diffPhase > 0)) :
    diffPhaseMod = np.pi

  # // incremental phase correction
  diffPhaseCorrection = diffPhaseMod - diffPhase

# // Ignore correction when incremental variation is smaller than cutoff
  if (((diffPhaseCorrection < np.pi) and (diffPhaseCorrection > 0)) or
      ((diffPhaseCorrection > -np.pi) and (diffPhaseCorrection<0))) :
    diffPhaseCorrection = 0
  
  diffPhaseCorrectionCum = diffPhaseCorrectionCum + diffPhaseCorrection

  phaseOut = rangeBinPhase + diffPhaseCorrectionCum

  return phaseOut, diffPhaseCorrectionCum


def removeImpulseNoise(dataPrev2, dataPrev1 , dataCurr, thresh) :
  backwardDiff = dataPrev1 - dataPrev2
  forwardDiff  = dataPrev1 - dataCurr
  y = dataPrev1 

  x1 = 0
  x2 = 2
  y1 = dataPrev2
  y2 = dataCurr
  x = 1

  if ((forwardDiff > thresh) and (backwardDiff > thresh)) or ((forwardDiff < -thresh) and (backwardDiff < -thresh)) :
    y = y1 + ( ((x-x1)*(y2 - y1))/(x2 - x1) )

  return y

def findPeaks(pDataIn, start_index, stop_index) :
  pPeakLocs = []
  pPeakValues = []
  for temp in range(start_index + 1, stop_index - 1) :
     if (pDataIn[temp] > pDataIn[temp-1] and pDataIn[temp] > pDataIn[temp+1] )  :
        pPeakLocs.append(temp)
        pPeakValues.append(pDataIn[temp])
  return pPeakLocs, pPeakValues

def  computeConfidenceMetric(pDataSpectrum,
                                spectrumIndexStart,
                                spectrumIndexEnd,
                                peakIndex,
                                numIndexAroundPeak):
  endInd   = peakIndex + numIndexAroundPeak
  startInd = peakIndex - numIndexAroundPeak

  if (startInd < 0) :
      startInd = 0
  if(endInd >= (spectrumIndexEnd)) :
      endInd = spectrumIndexEnd - 1

  # /* Energy of the complete Spectrum */
  sumSignal = 0
  for indexTemp in range(spectrumIndexStart, spectrumIndexEnd + 1) :
      sumSignal += pDataSpectrum[indexTemp]

  # /* Energy of the frequency Bins including (and around) the peak of interest */
  sumPeak   = 0
  for indexTemp in range(startInd, endInd + 1):
    sumPeak += pDataSpectrum[indexTemp]

  if abs(sumSignal - sumPeak) < 0.0001:     # This condition would arise if the input signal amplitude is very low
      confidenceMetric = 0
  else :
    confidenceMetric = sumPeak/(sumSignal - sumPeak)

  return confidenceMetric


def  filterPeaksWfm(pPeakLocsIn, winMin, winMax) :
	# // Filter out invalid peaks outside [winMin winMax]
  pPeakLocsOut = []
  pPeakLocsOut.append(pPeakLocsIn[0])
  numPeaksOutValid = 1   # The first peak is assumed to be valid

  for tempIndex in range(1, len(pPeakLocsIn)) :
    pkDiff = pPeakLocsIn[tempIndex] - pPeakLocsOut[numPeaksOutValid - 1 ]
    if ( pkDiff > winMin ) :
        pPeakLocsOut.append(pPeakLocsIn[tempIndex])
        numPeaksOutValid = numPeaksOutValid + 1

  return numPeaksOutValid

# Function to rotate a set of coordinates [x,y,z] about the various axis via the tilt angles
# Tilt angles are in degrees
def eulerRot(x, y, z, elevTilt, aziTilt):
    # Convert to radians
    elevTilt = np.deg2rad(elevTilt) 
    aziTilt = np.deg2rad(aziTilt)

    elevAziRotMatrix = np.matrix([  [  math.cos(aziTilt),  math.cos(elevTilt)*math.sin(aziTilt), math.sin(elevTilt)*math.sin(aziTilt)],
                                    [ -math.sin(aziTilt),  math.cos(elevTilt)*math.cos(aziTilt), math.sin(elevTilt)*math.cos(aziTilt)],
                                    [                  0,                   -math.sin(elevTilt),                   math.cos(elevTilt)],
                                ])
    
    # Old matrix for only Elevation tilt
    # elevRotMatrix = np.matrix([ [ 1,                   0,                  0 ],
    #                             [ 0,  math.cos(elevTilt), math.sin(elevTilt) ],
    #                             [ 0, -math.sin(elevTilt), math.cos(elevTilt) ]
    #                         ])

    target =  np.array([[x],[y],[z]])
    rotTarget = elevAziRotMatrix*target
    rotX = rotTarget[0,0]
    rotY = rotTarget[1,0]
    rotZ = rotTarget[2,0]
    return rotX, rotY, rotZ

class VitalMonitor():
  def __init__(self, config_dir = "configs"):
    # set window toolbar options, and title

    self.config_dir =  config_dir
    self.configParameters, self.rangeArray = parseConfigFile(os.path.join(config_dir, 'AOP_6m_default.cfg'))
    self.numRangeBins = self.configParameters["numRangeBins"]
    self.numAngleBins = 180 // AoA_spacing + 1
    self.samplingFreq_Hz = 1 / (self.configParameters["framePeriodicity"] * 0.001)  # Sampling Frequency
    self.freqIncrement_Hz = self.samplingFreq_Hz  / PHASE_FFT_SIZE


    self.breath_startFreq_Hz = 0.1    # Breathing-Rate peak search Start-Frequency
    self.breath_endFreq_Hz   = 0.6    # Breathing-Rate peak search End-Frequency

    self.heart_startFreq_Hz = 0.8    # Heart-Rate peak search Start-Frequency
    self.heart_endFreq_Hz   = 2.0    # Heart-Rate peak search End-Frequency
    
    self.breath_start_index = math.floor(self.breath_startFreq_Hz / self.freqIncrement_Hz)
    self.breath_stop_index = math.ceil(self.breath_endFreq_Hz / self.freqIncrement_Hz)

    self.heart_start_index = math.floor(self.heart_startFreq_Hz / self.freqIncrement_Hz)
    self.heart_stop_index = math.ceil(self.heart_endFreq_Hz / self.freqIncrement_Hz)

    self.fft_freqs = np.fft.fftfreq(PHASE_FFT_SIZE, 1 / self.samplingFreq_Hz)[:PHASE_FFT_SIZE // 2]

    self.peakDistanceBreath_Max = self.samplingFreq_Hz/(self.breath_startFreq_Hz)
    self.peakDistanceBreath_Min = self.samplingFreq_Hz/(self.breath_endFreq_Hz)
  
    self.peakDistanceBreath_Max = self.samplingFreq_Hz/(self.breath_startFreq_Hz)
    self.peakDistanceBreath_Min = self.samplingFreq_Hz/(self.breath_endFreq_Hz)

    self.breath_bp_b, self.breath_bp_a = sci_signal.butter(5, (self.breath_startFreq_Hz, self.breath_endFreq_Hz), btype='bandpass', fs=self.samplingFreq_Hz)
    self.breath_zi = sci_signal.lfilter_zi(self.breath_bp_b, self.breath_bp_a)
    # self.sos = sci_signal.butter(5, 0.5, btype='lowpass', fs=self.samplingFreq_Hz, output='sos')
    # self.sos_zi = sci_signal.sosfilt_zi(self.sos)

    self.heart_bp_b, self.heart_bp_a = sci_signal.butter(5, [self.heart_startFreq_Hz, self.heart_endFreq_Hz], btype='bandpass', fs=self.samplingFreq_Hz)
    self.heart_zi = sci_signal.lfilter_zi(self.heart_bp_b, self.heart_bp_a)


    self.info_sender = InfoSender()

    self.min_breath_confidence = 1.8
    self.min_heart_confidence = 0.2

    self.frameNum = 0
    self.lastFrameNum = -1
    self.lastTID = []

    self.profile = {'startFreq': 60.25, 'numLoops': 64, 'numTx': 3, 'sensorHeight':1.5, 'maxRange':10, 'az_tilt':0, 'elev_tilt':0}
    self.stopFlag = False
   

    self.falldown_status = []
    self.falldown_counter = []
    self.falldown_height = []

    self.vital_status = []
    self.breath_rate = []
    self.heart_rate = []
    self.position = []

    self.notrack_frames = 0 #number of frames without person.
    self.motionStatus = MotionStatus.NOT_DETECTED


    # Persistent point cloud
    # self.previousClouds = np.zeros((1,7))

    self.targetSize = [] #10 frames of data, 20 tracks, height, relHeight, length, width, age, average height, height delta
    self.targetAge = {} 

    self.VSignBuffer = np.empty((0, 64, gl_numVirtAnt), dtype=complex)
    self.CmplxforVitalSign = np.empty((0, 64, gl_numVirtAnt), dtype=complex)

    self.filt_breathing = []
    self.filt_heartbeat = []
    self.breath_iir_z = []
    self.heart_iir_z = []
    self.phasePrevFrame = []
    self.breath_buffer = []
    self.heart_buffer = []
    self.diffPhaseCorrectionCum = []
    self.phaseUsedComputationPrev = []
    self.dataPrev1 = []
    self.dataPrev2 = []

    self.numPersistentFrames = 25

    self.confMetric_numIndexAroundPeak_breath = math.floor(CONF_METRIC_BANDWIDTH_PEAK_BREATH_HZ/ self.freqIncrement_Hz)
    self.confMetric_numIndexAroundPeak_heart = math.floor(CONF_METRIC_BANDWIDTH_PEAK_HEART_HZ/ self.freqIncrement_Hz)

    self.pTempReal_Prev = np.zeros((self.numRangeBins,), dtype='complex64')

    self.DopplerWindowCoefs=  [ 0.0800, 0.0894, 0.1173, 0.1624,
                                                        0.2231, 0.2967, 0.3802, 0.4703,
                                                        0.5633, 0.6553, 0.7426, 0.8216,
                                                        0.8890, 0.9422, 0.9789, 0.9976 ]
    pass

  def start_run(self, cliCom, dataCom) :
    self.connectCom(cliCom, dataCom)
    self.selectCfg()
    self.sendCfg()
    self.run()

  def stop(self, signum, frame) :
    self.stopFlag = True
    time.sleep(0.03)
    sys.exit()

  def saveSensorPoseCfg(self, height, yaw, pitch):
    fname = self.selectFile()
    with open(fname, 'r') as cfg_file:
      self.cfg = cfg_file.readlines()

    for i, line in enumerate(self.cfg):
      args = line.split()
      if (len(args) > 0):
        # cfarCfg
        if (args[0] == 'sensorPosition'):    
          self.cfg[i] = "sensorPosition " + str(height) + " " + str(yaw) + " " + str(pitch) + "\n"

    with open(fname, 'w') as cfg_file:
      cfg_file.writelines(self.cfg)


  def setSensorPose(self, height, yaw, pitch):
    self.profile['sensorHeight'] = height
    self.profile['az_tilt'] = yaw
    self.profile['elev_tilt'] = pitch
    self.saveSensorPoseCfg(height, yaw, pitch)


  def getSensorPose(self):
    return self.profile['sensorHeight'] , self.profile['az_tilt'], self.profile['elev_tilt']
  
  def saveBoundaryBoxCfg(self, leftX, rightX, nearY, farY, bottomZ, topZ):
    fname = self.selectFile()
    with open(fname, 'r') as cfg_file:
        self.cfg = cfg_file.readlines()

    for i, line in enumerate(self.cfg):
      args = line.split()
      if (len(args) > 0):
          # cfarCfg
        if (args[0] == 'boundaryBox'):    
          self.cfg[i] = "boundaryBox " + str(leftX) + " " + str(rightX) + " " + str(nearY) + " " + str(farY) + " " + str(bottomZ) + " " + str(topZ) + "\n"

    with open(fname, 'w') as cfg_file:
      cfg_file.writelines(self.cfg)

  def setBoundaryBox(self, leftX, rightX, nearY, farY, bottomZ, topZ):
    self.saveBoundaryBoxCfg(leftX, rightX, nearY, farY, bottomZ, topZ)
    self.boundaryBox = Box(leftX, rightX, nearY, farY, bottomZ, topZ)

  def getBoundaryBox(self):
    return self.boundaryBox.left_x, self.boundaryBox.right_x, self.boundaryBox.near_y, self.boundaryBox.far_y, self.boundaryBox.bottom_z, self.boundaryBox.top_z
  
  def setServerAddress(self, server_addr, server_port):
    self.info_sender.setServerIp(server_addr, server_port)

  def getServerAddress(self):
    return self.info_sender.getServerIp()
    
  def setDeviceID(self, device_id):
    self.info_sender.setDeviceID(device_id)
        
  def setDetectionParameters(self, fall_thresh, distance_thresh, send_period, falldown_detection):
    self.fallThresh = fall_thresh
    self.distanceThresh = distance_thresh
    self.sendPeriod = send_period
    self.falldownDetection = falldown_detection

  def getDetectionParameters(self):
    return self.fallThresh, self.distanceThresh, self.sendPeriod
  
  def setVitalSignParameters(self, min_breath_confidence, min_heart_confidence):
    self.min_breath_confidence = min_breath_confidence
    self.min_heart_confidence = min_heart_confidence

  def getVitalSignParameters(self):
    return self.min_breath_confidence, self.min_heart_confidence


  def getBinIndex(self, Pos_x, Pos_y):
      # Position of Static Person
      ind_bin = int(np.argmin(np.abs(self.rangeArray - np.sqrt(Pos_x ** 2 + Pos_y ** 2))))
      return ind_bin
  
  def removeClutter(self,  cmplx_Foreground_all):
    numRangeBins = cmplx_Foreground_all.shape[0]
    alphaClutter = 0.01

    for rangeBinIndex in range(0, numRangeBins) :
      self.pTempReal_Prev[rangeBinIndex] = alphaClutter*cmplx_Foreground_all[rangeBinIndex] + (1-alphaClutter)*self.pTempReal_Prev[rangeBinIndex]


  def GetPhase(self, ind_bin, bin_range, cmplx_Foreground_all):
    numRangeBins = cmplx_Foreground_all.shape[0]
    self.removeClutter(cmplx_Foreground_all)
    cmplx_Foreground = 0
    max_norm = 0
    for i in range(max(0, ind_bin - bin_range), min(ind_bin + bin_range + 1, numRangeBins)) :
      norm = np.abs(cmplx_Foreground_all[i] - self.pTempReal_Prev[i])
      cmplx_Foreground =  cmplx_Foreground_all[i] if norm > max_norm else cmplx_Foreground
      max_norm = max(norm, max_norm)

    return np.angle(cmplx_Foreground)
  

  def DoppleWin(self, data_buffer):

    index_WinEnd =  len(data_buffer) - 1

    for index_win in range(len(self.DopplerWindowCoefs)) :
      tempFloat = self.DopplerWindowCoefs[index_win]
      data_buffer[index_win] = tempFloat * data_buffer[index_win] 
      data_buffer[index_WinEnd] = tempFloat * data_buffer[index_WinEnd] 

      index_WinEnd = index_WinEnd - 1

  
  def updateGraph(self, outputDict):
    tracks = None
    # trackIndexs = None
    numTracks = 0
    self.frameNum = 0
    error = 0
    # vitalsDict = None
    cmplx_rgAntAziEle = None
    heights = None

    # Tracks
    if ('trackData' in outputDict):
      tracks = outputDict['trackData']

    # Heights
    if ('heightData' in outputDict):
        heights = outputDict['heightData']

    # Track index
    # if ('trackIndexes' in outputDict):
    #   trackIndexs = outputDict['trackIndexes']
      
    # Number of Tracks
    if ('numDetectedTracks' in outputDict):
      numTracks = outputDict['numDetectedTracks']

    # Frame number
    if ('frameNum' in outputDict):
      self.frameNum = outputDict['frameNum'] 

    # Error
    if ('error' in outputDict):
      error = outputDict['error']
      

    if ('aziumElevHeatMap' in outputDict):
      cmplx_rgAntAziEle = outputDict['aziumElevHeatMap'].reshape((self.numRangeBins, gl_numVirtAnt)) 
    else :
      return
          

    if (error != 0):
      print ("Parsing Error on frame: %d" % (self.frameNum))
      print ("\tError Number: %d" % (error))


    if (numTracks > 0 ):
      # cmplx_Foreground_all = self.CmplxforVitalSign - np.mean(self.CmplxforVitalSign, axis=0)
      for track in tracks:
        tid = int(track[0])       
        ind_bin = self.getBinIndex(track[1], track[2])
        if tid not in self.lastTID:
          self.breath_buffer[tid] = np.empty((0, ))
          self.heart_buffer[tid] = np.empty((0, ))
          self.phasePrevFrame[tid] = 0
          self.diffPhaseCorrectionCum[tid] = 0
          self.phaseUsedComputationPrev[tid] = 0
          self.dataPrev1[tid] = 0
          self.dataPrev2[tid] = 0

          self.filt_breathing[tid] = []
          self.filt_breath_conf[tid] = []
          self.filt_heartbeat[tid] = []

          self.breath_rate[tid] = None
          self.heart_rate[tid] = None
          
          self.speed[tid] = 0

        self.speed[tid] = math.sqrt(track[4] * track[4]  + track[5] * track[5]  + track[6]  * track[6])
        # print ("track %d, speed: %f" % (tid, self.speed[tid]))

        rangeBinPhase = self.GetPhase(ind_bin, 3, cmplx_rgAntAziEle[:, -1])
        unwrapPhasePeak, self.diffPhaseCorrectionCum[tid] = unwrap(rangeBinPhase, self.phasePrevFrame[tid], self.diffPhaseCorrectionCum[tid])
        self.phasePrevFrame[tid] = rangeBinPhase

        phaseUsedComputation = unwrapPhasePeak - self.phaseUsedComputationPrev[tid]
        self.phaseUsedComputationPrev[tid] = unwrapPhasePeak


        framePhase = removeImpulseNoise(self.dataPrev2[tid] , self.dataPrev1[tid], phaseUsedComputation, 1.5)
        self.dataPrev2[tid] = self.dataPrev1[tid]
        self.dataPrev1[tid] = phaseUsedComputation

        phaseUsedComputation = np.expand_dims(framePhase, 0)
        
        if (len(self.breath_buffer[tid]) == 0) :
          self.breath_iir_z[tid] = self.breath_zi * phaseUsedComputation
          
        filt_phase_diff_brt, self.breath_iir_z[tid] = sci_signal.lfilter(self.breath_bp_b, self.breath_bp_a, phaseUsedComputation, zi = self.breath_iir_z[tid])
        self.breath_buffer[tid] =  np.hstack((self.breath_buffer[tid], filt_phase_diff_brt))
       
        if (len(self.heart_buffer[tid]) == 0) :
          self.heart_iir_z[tid] = self.heart_zi * phaseUsedComputation

        filt_phase_diff_brt, self.heart_iir_z[tid] = sci_signal.lfilter(self.heart_bp_b, self.heart_bp_a, phaseUsedComputation, zi = self.heart_iir_z[tid])
        if (self.speed[tid] < 0.6) :
          self.heart_buffer[tid] =  np.hstack((self.heart_buffer[tid], filt_phase_diff_brt))  # cmplx_rgAntAziEle 64 * 12

        # Breathing Frequency Estimation
        if ((len(self.breath_buffer[tid]) % mov_framelen_vsign) == 0) : 

          # peak_locs, peak_values = findPeaks(self.breath_buffer[tid], 0, circularBufferSizeBreath - 1)
          # numPeaksBreath = len(peak_values) 
          # if (numPeaksBreath > 0) :
          #   numPeaksBreath = filterPeaksWfm(peak_locs, self.peakDistanceBreath_Min, self.peakDistanceBreath_Max)
          # breathingRateEst_peakCount = round(CONVERT_HZ_BPM  * ((numPeaksBreath * self.samplingFreq_Hz) / circularBufferSizeBreath))
          # print("breath rate: {}".format(breathingRateEst_peakCount))
          # self.filt_breathing[tid].append(breathingRateEst_peakCount)

          # FFT
          breathing_yf = np.fft.fft(self.breath_buffer[tid], PHASE_FFT_SIZE)
          breathing_norm_yf = 2.0 / PHASE_FFT_SIZE * np.abs(breathing_yf[:PHASE_FFT_SIZE // 2])

          peak_locs, peak_values = findPeaks(breathing_norm_yf, self.breath_start_index, self.breath_stop_index)
          
          if (len(peak_values) > 0) :
            maxIndexBreathSpect = peak_locs[np.argmax(np.array(peak_values))]
          else :
            maxIndexBreathSpect = np.argmax(breathing_norm_yf[self.breath_start_index : self.breath_stop_index]) + self.breath_start_index
            
          breathingRateEst_fft = self.fft_freqs[maxIndexBreathSpect] * CONVERT_HZ_BPM
          confidenceMetricBreathOut = computeConfidenceMetric(breathing_norm_yf, self.breath_start_index, self.breath_stop_index, maxIndexBreathSpect, self.confMetric_numIndexAroundPeak_breath)

          # print("breath rate: {}, confidence: {}".format(breathingRateEst_fft, confidenceMetricBreathOut))
          if (confidenceMetricBreathOut >= self.min_breath_confidence ) :
            self.filt_breathing[tid].append(breathingRateEst_fft)
            # self.filt_breath_conf[tid].append(confidenceMetricBreathOut)
          
          # if (len(self.filt_breathing[tid]) >= NUM_FRAMES_FOR_MEDIAN):
          #   max_conf_index = np.argmax(self.filt_breath_conf[tid])
          #   # self.breath_rate[tid]  = statistics.median(self.filt_breathing[tid]) 
          #   self.breath_rate[tid]  = self.filt_breathing[tid][max_conf_index]
          #   self.filt_breathing[tid].pop(0)
          #   self.filt_breath_conf[tid].pop(0)
          
          if (len(self.filt_breathing[tid]) >= NUM_FRAMES_FOR_MEDIAN):
            self.breath_rate[tid]  = statistics.median(self.filt_breathing[tid]) 
            self.filt_breathing[tid].pop(0)

        # Heart Frequency Estimation
        if ((len(self.heart_buffer[tid]) % mov_framelen_vsign) == 0 ) : 
          if len(self.heart_buffer[tid]) >= circularBufferSizeHeart:
            self.DoppleWin(self.heart_buffer[tid])
          # FFT
          heartbeat_yf = np.fft.fft(self.heart_buffer[tid], PHASE_FFT_SIZE)
          heartbeat_norm_yf = 2.0 / PHASE_FFT_SIZE * np.abs(heartbeat_yf[:PHASE_FFT_SIZE // 2])
          peak_locs, peak_values = findPeaks(heartbeat_norm_yf, self.heart_start_index, self.heart_stop_index)

          if (len(peak_values) > 0) :
            maxIndexHeartBeatSpect = peak_locs[np.argmax(np.array(peak_values))]
          else :
            maxIndexHeartBeatSpect = np.argmax(heartbeat_norm_yf[self.heart_start_index : self.heart_stop_index]) + self.heart_start_index

          heartbeatRateEst_fft = self.fft_freqs[maxIndexHeartBeatSpect] * CONVERT_HZ_BPM
          confidenceMetricHeartbeatOut = computeConfidenceMetric(heartbeat_norm_yf, self.heart_start_index, self.heart_stop_index, maxIndexHeartBeatSpect, self.confMetric_numIndexAroundPeak_heart)

          # print("heart rate: {}, confidence: {}".format(heartbeatRateEst_fft, confidenceMetricHeartbeatOut))
          if (confidenceMetricHeartbeatOut >= self.min_heart_confidence ) :
            self.filt_heartbeat[tid].append(heartbeatRateEst_fft)

          if (len(self.filt_heartbeat[tid]) >= NUM_FRAMES_FOR_MEDIAN):
            self.heart_rate[tid] = statistics.median(self.filt_heartbeat[tid])
            self.filt_heartbeat[tid].pop(0)

        if len(self.breath_buffer[tid]) >= circularBufferSizeBreath:
          self.breath_buffer[tid]  = np.delete(self.breath_buffer[tid] , range(mov_framelen_vsign))

        if len(self.heart_buffer[tid]) >= circularBufferSizeHeart:
          self.heart_buffer[tid]  = np.delete(self.heart_buffer[tid] , range(mov_framelen_vsign))
          
    # Rotate point cloud and tracks to account for elevation and azimuth tilt
    if (self.profile['elev_tilt'] != 0 or self.profile['az_tilt'] != 0):
      if (tracks is not None):
        for i in range(numTracks):
          rotX, rotY, rotZ = eulerRot (tracks[i,1], tracks[i,2], tracks[i,3], self.profile['elev_tilt'], self.profile['az_tilt'])
          tracks[i,1] = rotX
          tracks[i,2] = rotY
          tracks[i,3] = rotZ

    # Shift points to account for sensor height
    if (self.profile['sensorHeight'] != 0):
      if (tracks is not None):
          tracks[:,3] = tracks[:,3] + self.profile['sensorHeight']
    
    targetSize = np.zeros((2,20))

    #target heights     
       
    if (self.falldownDetection and numTracks) :
      if (len(heights) != len(tracks)):
        print ("WARNING: number of heights does not match number of tracks")
      #populate heights for current tracks
      for height in heights:
        # Find track with correct TID
        tid = -1
        for track in tracks:
          # Found correct track
          if (track[0] == height[0]):
            tid = int(height[0])
            break
        if (tid == -1) :
          break
        # self.falldown_status[tid] = FalldownStatus.STANDING
        
        person_height= round(height[1], 3) #absolute height

        if tid in self.lastTID:
          #print("lastTID")

          targetSize[0, tid] = self.frameNum
          targetSize[1, tid] = person_height

          self.targetAge[tid] = self.targetAge[tid] + 1
          age = self.targetAge[tid] #age
          #need 2 seconds to get accurate height
          if(age>40):
            # for i in range(-1, -10, -1) :
            #   if self.frameNum - self.targetList[i][3, tid] >= 10 :
            #     break
            delta_height = person_height - self.targetSize[fallDownRefOffset][1,tid] #delta height after 10 frames
            # print("tid {}, frame {}, height {}, old_height {}, delta_height {}.".format(tid, self.frameNum, person_height, self.targetSize[fallDownRefOffset][1,tid], delta_height))

            if (delta_height < self.fallThresh):
              print("fall down detected, frame {}, id: {}, height {}, delta_height {}.".format(int(self.frameNum), tid, targetSize[1, tid], delta_height))
              self.falldown_status[tid] = FalldownStatus.FALLDOWN
              self.falldown_height[tid] = min(self.falldown_height[tid], person_height)
            else :
              if (self.falldown_status[tid] == FalldownStatus.FALLDOWN or self.falldown_status[tid] == FalldownStatus.LYING): 
                if (person_height >= self.falldown_height[tid] + 0.2) :
                  self.falldown_height[tid] = 100.
                  self.falldown_status[tid] = FalldownStatus.STANDING
                else :
                  self.falldown_height[tid] = min(self.falldown_height[tid], person_height)
                  self.falldown_status[tid] = FalldownStatus.LYING
                  
        else:
          self.targetAge[tid] = 1
          # targetSize[0, tid] = self.frameNum
          # targetSize[1, tid]= person_height
          self.falldown_status[tid] = FalldownStatus.STANDING
          self.falldown_counter[tid] = 0
          self.falldown_height[tid] = 100.
    
      self.targetSize.append(targetSize)
      # If we have more point clouds than we need, delete the oldest ones
      while (len(self.targetSize) > self.numPersistentFrames):
        self.targetSize.pop(0)

    # send result to server
    track_ids = []
    track_positions = []
    breath_rate = []
    heart_rate = []
    vital_status = []
    falldown_status = []

    if (numTracks) :
      falldown_detected = False
      for track in tracks:
        tid = int(track[0])
        track_falldown_status = FalldownStatus.STANDING

        if (self.falldownDetection) :
          if (self.falldown_status[tid] == FalldownStatus.FALLDOWN):
            self.falldown_counter[tid] = int(self.samplingFreq_Hz) * fallDownSeconds + 1
            falldown_detected = True

          track_falldown_status = FalldownStatus.FALLDOWN if self.falldown_counter[tid] > 0 or self.falldown_status[tid] == FalldownStatus.FALLDOWN else self.falldown_status[tid]


        self.falldown_counter[tid]  -= 1

        if (self.breath_rate[tid] != None and self.heart_rate[tid] != None) :
          track_ids.append(tid)
          falldown_status.append(track_falldown_status.value)
          track_positions.append(Position(track[1], track[2], track[3]))
          breath_rate.append(int(round(self.breath_rate[tid])))
          heart_rate.append(int(round(self.heart_rate[tid])))
          vital_status.append(VitalStatus.PRESENCE.value)    

      if gpio:
        io_controll.setGreenLed(True)
        if falldown_detected :
            io_controll.triggerFalldownAlarm()
            io_controll.triggerRedLed()

      if len(track_ids) != 0 and self.frameNum - self.lastFrameNum >= (self.sendPeriod * self.samplingFreq_Hz): 
        self.info_sender.sendVitalSigns(track_ids, \
                                    track_positions, \
                                    breath_rate, heart_rate, vital_status, falldown_status, 
                                  self.boundaryBox)

        
        self.lastFrameNum = self.frameNum

    else :
      # send no person packet.
      # self.lastFrameNum = min(self.lastFrameNum, self.frameNum)
      if self.notrack_frames > int(self.samplingFreq_Hz) * noTrackTimeoutSeconds :  

        self.info_sender.sendVitalSigns(track_ids, \
                                    track_positions, \
                                    breath_rate, heart_rate, vital_status, falldown_status, 
                                    self.boundaryBox)  
        self.lastFrameNum = self.frameNum                                                      
        self.notrack_frames = 0

        if gpio:
          io_controll.setGreenLed(False)

    if (tracks is None) :
      self.notrack_frames += 1
      # print("NoTracks, notrack frames: {}".format(self.notrack_frames))
    else:   
      self.notrack_frames = 0
      # self.motionStatus = MotionStatus.MOVING

    self.lastTID = []
    if numTracks > 0 :
      for track in tracks:
        # Found correct track
        self.lastTID.append(track[0])

      
    return


  def connectCom(self, cliCom, dataCom):
    #get parser
    self.parser = uartParser()
    # #init threads and timers
    # self.uart_thread = parseUartThread(self.parser)
    # self.uart_thread.fin.connect(self.updateGraph)
    # self.parseTimer = QTimer()
    # self.parseTimer.setSingleShot(False)
    # self.parseTimer.timeout.connect(self.parseData)        
    try:
      self.parser.connectComPorts(cliCom, dataCom)
      self.cliCom = cliCom
      self.dataCom = dataCom

#TODO: create the disconnect button action
    except Exception as e:
        print (e)
        print('Unable to Connect')

#TODO: create the disconnect button action
    except Exception as e:
        print (e)
        print('Unable to Connect')


  def disconnectCom(self):
    if self.parser:
      self.parser.disconnectComPorts()         
        
#
# Select and parse the configuration file
  def selectCfg(self):
    try:
      self.parseCfg(self.selectFile())
    except Exception as e:
      print(e)
      print('No cfg file selected!')

  def selectFile(self):
    configDirectory = self.config_dir
    # return os.path.join(configDirectory ,'vital_signs_AOP_6m.cfg')
    return os.path.join(configDirectory ,'AOP_6m_default.cfg')

  

  def parseCfg(self, fname):
    with open(fname, 'r') as cfg_file:
      self.cfg = cfg_file.readlines()
    counter = 0
    chirpCount = 0

    self.falldown_status = []
    self.falldown_counter = []
    self.falldown_height = []

    self.vital_status = []
    self.breath_rate = []
    self.heart_rate = []
    self.position = []   

    self.filt_breathing = []
    self.filt_breath_conf = []
    self.filt_heartbeat = []
    self.breath_iir_z = []
    self.heart_iir_z = []
    self.phasePrevFrame = []
    self.breath_buffer = []
    self.heart_buffer = []
    self.diffPhaseCorrectionCum = []
    self.phaseUsedComputationPrev = []
    self.dataPrev1 = []
    self.dataPrev2 = []
    self.speed = []
    
    for line in self.cfg:
      args = line.split()
      if (len(args) > 0):
        # cfarCfg
        if (args[0] == 'cfarCfg'):
          pass
          #self.cfarConfig = {args[10], args[11], '1'}
        # trackingCfg
        elif (args[0] == 'trackingCfg'):
          if (len(args) < 5):
            print ("Error: trackingCfg had fewer arguments than expected")
            continue
          self.profile['maxTracks'] = int(args[4])
          # If we only support 1 patient, hide the other patient window

          # Initialize Vitals output dictionaries for each potential patient
          for i in range (min(self.profile['maxTracks'], MAX_VITALS_PATIENTS)):
            # Initialize 
            patientDict = {}
            patientDict ['id'] = i
            patientDict ['rangeBin'] = 0
            patientDict ['breathDeviation'] = []
            patientDict ['heartRate'] = []
            patientDict ['breathRate'] = []
            patientDict ['position'] = []
            patientDict ['speed'] = []

            self.vital_status.append(VitalStatus.NOT_DETECTED)
            self.falldown_status.append(FalldownStatus.NOT_DETECTED)
            self.falldown_counter.append(0)
            self.falldown_height.append(100.)
            self.breath_rate.append(0.)
            self.heart_rate.append(0.)
            self.position.append(Position(0., 0., 0.))

            self.filt_breathing.append([])
            self.filt_breath_conf.append([])
            self.filt_heartbeat.append([])

            self.breath_iir_z.append(np.zeros(self.breath_zi.shape))
            self.heart_iir_z.append(np.zeros(self.heart_zi.shape))
            self.breath_buffer.append(np.empty((0, )))
            self.heart_buffer.append(np.empty((0, )))

            self.phasePrevFrame.append(0)
            self.diffPhaseCorrectionCum.append(0)
            self.phaseUsedComputationPrev.append(0)
            self.dataPrev1.append(0)
            self.dataPrev2.append(0)

            self.speed.append(0.)

        elif (args[0] == 'AllocationParam'):
            pass
            #self.allocConfig = tuple(args[1:6])
        elif (args[0] == 'GatingParam'):
            pass
            #self.gatingConfig = tuple(args[1:4])
        elif (args[0] == 'SceneryParam' or args[0] == 'boundaryBox'):
            if (len(args) < 7):
                print ("Error: SceneryParam/boundaryBox had fewer arguments than expected")
                continue
            self.boundaryLine = counter
            leftX = float(args[1])
            rightX = float(args[2])
            nearY = float(args[3])
            farY = float(args[4])
            bottomZ = float(args[5])
            topZ = float(args[6])
            self.boundaryBox = Box(leftX, rightX, nearY, farY, bottomZ, topZ)

        elif (args[0] == 'staticBoundaryBox'):
            self.staticLine = counter
        elif (args[0] == 'profileCfg'):
            if (len(args) < 12):
                print ("Error: profileCfg had fewer arguments than expected")
                continue
            self.profile['startFreq'] = float(args[2])
            self.profile['idle'] = float(args[3])
            self.profile['adcStart'] = float(args[4])
            self.profile['rampEnd'] = float(args[5])
            self.profile['slope'] = float(args[8])
            self.profile['samples'] = float(args[10])
            self.profile['sampleRate'] = float(args[11])
            print(self.profile)
        elif (args[0] == 'frameCfg'):
            if (len(args) < 4):
                print ("Error: frameCfg had fewer arguments than expected")
                continue
            self.profile['numLoops'] = float(args[3])
            self.profile['numTx'] = float(args[2])+1
        elif (args[0] == 'chirpCfg'):
            chirpCount += 1
        elif (args[0] == 'sensorPosition'):
          if (len(args) < 4):
              print ("Error: sensorPosition had fewer arguments than expected")
              continue
          self.profile['sensorHeight'] = float(args[1])
          print('Sensor Height from cfg = ',str(self.profile['sensorHeight']))
          self.profile['az_tilt'] = float(args[2])
          self.profile['elev_tilt'] = float(args[3])
        # Only used for Small Obstacle Detection
        elif (args[0] == 'occStateMach'):
          numZones = int(args[1])
          if (numZones > 2):
              print('ERROR: More zones specified by cfg than are supported in this GUI')
          
      counter += 1
    pass


  def sendCfg(self):
    try:
      self.parser.sendCfg(self.cfg)
      self.configSent = 1
      # self.parseTimer.start(self.frameTime) # need this line 
            
    except Exception as e:
      print(e)
      print ('No cfg file selected!')


  def run(self):
    self.stopFlag = False

    while not self.stopFlag :
      outputDict = self.parser.readAndParseUart()
      if len(outputDict) != 0 :
        self.updateGraph(outputDict)
      else :
        self.disconnectCom()
        self.connectCom(self.cliCom, self.dataCom)
        self.selectCfg()
        self.sendCfg()

  def buttonAlarmCB(self): 
    print("button alarm triggered!")
    io_controll.triggerRedLed(ALARM_LED_PULSE_DURATION_MS)
    self.info_sender.sendButtonAlarm()

  def audioAlarmCB(self): 
    print("audio alarm triggered!")
    io_controll.triggerRedLed(ALARM_LED_PULSE_DURATION_MS)
    self.info_sender.sendAudioAlarm()

  def start(self):
    
    signal.signal(signal.SIGINT, self.stop)
    signal.signal(signal.SIGTERM, self.stop)

    if gpio:
      io_controll.monitorButtonAlarm(self.buttonAlarmCB)
      io_controll.monitorAudioAlarm(self.audioAlarmCB)

      io_controll.setStartAudio(True)

    self.start_run('/dev/ttyUSB0', '/dev/ttyUSB1')

    vital_monitor.disconnectCom()



if __name__ == "__main__":
    current_dir = os.getcwd()
    config_dir = os.path.join(current_dir, 'configs')

    if gpio:
      io_controll.initialize()
    
    vital_monitor = VitalMonitor(config_dir)
    app_setting = AppSetting(AppConfig.load_config(os.path.join(config_dir, 'app_config.json'),))
    vital_monitor.setServerAddress(app_setting.server_addr, app_setting.server_port)
    vital_monitor.setDeviceID(app_setting.device_id)
    vital_monitor.setDetectionParameters(app_setting.fall_thresh, app_setting.distance_thresh, \
                                         app_setting.send_period, app_setting.falldown_detection)
    vital_monitor.start()
