# from passlib.hash import sha256_crypt
import os
import subprocess
from time import sleep
import re
from threading import Thread

from PiFiServer.commandExec import commandExec
# from commandExec import commandExec
class MyThread(Thread):
    def __init__(self, func, args=()):
        super(MyThread, self).__init__()
        self.func = func
        self.args = args
    def run(self):
        self.result = self.func(*self.args)
    def get_result(self):
        Thread.join(self)  # 等待线程执行完毕
        try:
            return self.result
        except Exception:
            return None
def resetDevice():
    sleep(1)
    commandExec('sudo reboot')

class WifiController():
    def __init__(self, interface_name, hotspot_name, online_addr):
        self.interface_name = interface_name
        self.hotspot_name = hotspot_name
        self.online_addr = online_addr
        
        self.ssid_list = []

    def checkOnline(self) :
        response = os.system("ping -c 2 " + self.online_addr + " > /dev/null 2>&1")
        print("configwifi ping result = ")
        print(response)
        # and then check the response...
        online = False
        if response == 0:
            online = True
        else:
            online = False

        return online

    def hotspotAdded(self):
        result = subprocess.check_output(['iwconfig', self.interface_name])
        matches = re.findall(r'ESSID:\"(.+?)\"', result.split(b'\n')[0].decode('utf-8'))
        if len(matches) > 0:
            print("got connected to " + matches[0])
            return True
        return False
    
    def connect_status(self):
        """
        获取当前网卡连接状态
        :return: 无线网卡状态，数组
        """
        wlan_status = os.popen("nmcli device ").readlines()  # 获取网卡连接状态
        #print(wlan_status)
        wlan_status = [re.split("\s{2,}", x) for x in wlan_status]  # 处理数据

        #print(wlan_status)
        for status in wlan_status:
            if status[1] == "wifi":
                return status  # 返回无线网卡状态，数组

        return "Wi-Fi disable"
   
    def disconnectRouter(self):
        """
        断开wifi连接
        :return:
        """
        cmd = "nmcli device disconnect " + self.connect_status()[0]
        os.system(cmd)  # 断开连接

    def connectRouter(self, ssid,  psk):
        self.disconnectRouter()
        sleep(5)
        print("connect_wifi")
        # sudo iwlist  wlx0013eff302dc scan
        # command = 'sudo iwlist ' + interface_name + ' scan | grep \'ESSID:\''
        # result = os.popen(command)
        
        nmcli_connection_show = os.popen("sudo nmcli connection show").readlines()  # 获取无线网卡已储存的wifi列表
        nmcli_connection_show = [re.split("\s{2,}", x) for x in nmcli_connection_show]
        print(nmcli_connection_show)     


        check = False
        for i in nmcli_connection_show:  # 连接wifi
            if i[0] == ssid:
                print("i[0] = ", i[0])
                result = os.popen("sudo nmcli connection up '" + ssid + "'")  # 已存在的ssid连接
                res = result.read()
                for line in res.splitlines():
                    if ("successfully" in line) and (self.connect_status()[2] == "connected"):
                        print("1connection_success")
                        check = True
                        return True
                    else :
                        return False
        wifi_command = "sudo nmcli dev wifi connect '{}' password {}".format(ssid, psk.strip())   # wifi密码连接         
        # wifi_command = 'sudo nmcli con add con-name {} type wifi ssid {} wifi-sec.key-mgmt wpa-psk wifi-sec.psk {} autoconnect yes '.format(ssid, ssid, psk.strip())
        print('wifi command : {}'.format(wifi_command))
        # sleep(2)
        ret = commandExec(wifi_command)
        if ret.find('successfully') != -1  or ret.find('成功') != -1:
            os.system("sudo nmcli con up {}".format(ssid))
            sleep(3)
            if self.hotspotAdded() :
                os.system("sudo nmcli con delete " + self.hotspot_name)
            return True
        
        # os.system("sudo nmcli con delete " + ssid)
        os.system("sudo nmcli con up " + self.hotspot_name)
        return False


    def asyncWifiRouteValid(self, ssid,  psk):
        self.wifi_check_thread = MyThread(self.connectRouter, args=(ssid, psk))
        self.wifi_check_thread.start()

    def getWifiValidResult(self):
        return self.wifi_check_thread.get_result()
    
    def startHotspot(self, ssid = "healthtrack", password = "12345678"):
    #     os.system("nmcli con add con-name my-hotspot type wifi 802-11-wireless.mode  ap ifname {} ssid healthtrack \
    #               wifi-sec.key-mgmt wpa-psk wifi-sec.psk 12345678 ipv4.method manual autoconnect no \
    #               ipv4.address 192.168.4.1/24 ipv4.gateway 192.168.4.1".format(interface_name))
        if not self.hotspotAdded() :
            os.system("nmcli con add type wifi ifname {} con-name {} autoconnect yes ssid {}".format(self.interface_name, self.hotspot_name, ssid))
            os.system("nmcli con modify {} 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared".format(self.hotspot_name))
            os.system("nmcli con modify {} wifi-sec.key-mgmt wpa-psk".format(self.hotspot_name))
            os.system('nmcli con modify {} wifi-sec.psk "{}"'.format(self.hotspot_name, password))
        os.system("nmcli con up {}".format(self.hotspot_name))
        # os.system("nmcli device wifi hotspot con-name {} ssid {} band bg password {}".format(self.hotspot_name, ssid, password))
        sleep(2)

    def getssid(self):
        # if len(self.ssid_list) > 0:
        #     return self.ssid_list
            
        ssid_list = []
        # os.system("sudo ifconfig " + self.interface_name + " down")
        # os.system("sudo iwconfig " + self.interface_name + " mode Managed")
        # os.system("sudo ifconfig " + self.interface_name + " up")

        sleep(2)
        data = "sudo iw dev " + self.interface_name + " scan ap-force"
        sleep(2)
        get_ssid_list = subprocess.check_output(('sudo', 'iw', 'dev', self.interface_name, 'scan', 'ap-force'))
        ssids = get_ssid_list.splitlines()
        # if len(ssids) == 0:

            # get_ssid_list = commandExec(data)
            # ssids = get_ssid_list.splitlines()

        for s in ssids:
            s = s.strip().decode('utf-8')
            if s.startswith("SSID"):
                a = s.split(": ")
                try:
                    if len(a[1]) < 32:
                        ssid_list.append(a[1])
                except:
                    pass
        print(ssid_list)
        self.ssid_list = sorted(list(set(ssid_list)))

        return self.ssid_list



