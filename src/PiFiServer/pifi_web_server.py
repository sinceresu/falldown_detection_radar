#!/usr/bin/env python3
from flask import Flask, render_template, flash, request, url_for, redirect, Response, jsonify, g         
from time import sleep     
from threading import Thread
import os
import sys
import subprocess
import time
from vital_monitor import VitalMonitor
from PiFiServer.wifi_controller import  WifiController
from PiFiServer.commandExec import commandExec
from  app_config import AppConfig, AppSetting


import os
def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))
script_path = get_script_path()
# os.chdir(script_path())


app = Flask(__name__, static_url_path=os.path.join(script_path, 'PiFiServer/static') )

wifi_check_thread = None

setting_cb = None
wifi_init = False
wifi_controller = None
vital_monitor = None

class SettingCallback :
    def __init__(self, position_cb, boundbox_cb, server_cb):
        self.position_cb = position_cb
        self.boundbox_cb = boundbox_cb
        self.server_cb = server_cb


def resetDevice():
    print("str(e)")

    sleep(2)
    commandExec('sudo reboot')


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    
    if (wifi_init):
        return redirect(url_for("configWifi"))
    else:
        return redirect(url_for("configRadar"))


@app.route('/configWifi', methods=["GET", "POST"])
def configWifi():
    access_point=""
    return render_template('configWifi.html', access_point=access_point)

@app.route('/check', methods=["GET"])
def check():
    if wifi_controller.getWifiValidResult():
        Thread(target=resetDevice).start()
        return render_template('connectionSucceed.html')
    else:
        return redirect(url_for("iError"))
    
@app.route('/getwifilist')
def reuus():
    return render_template('wifiList.html', wifi_list= wifi_controller.getssid())   

@app.route('/setwifi', methods=["GET", "POST"])
def setWifi():
    global wifi_check_thread
    try:
        if request.method == "POST":
            attempted_ssid = request.form['ssid']
            attempted_psk  = request.form['psk']
            print(attempted_ssid)
            print(attempted_psk)
            wifi_controller.asyncWifiRouteValid(attempted_ssid, attempted_psk)
            return render_template('restart.html', message = "Try to connect wifi, please wait...", alert_msg = "Wifi Connection Completed, Reset device...")

    except Exception as e:
        print(str(e))
        return render_template('404.html'), "Error"


@app.route('/changeap', methods=["GET", "POST"])
def changeSettings():
    try:
        if request.method == "POST":
            attempted_ssid         = request.form['ssid_ap']
            attempted_psk          = request.form['psk_ap']
            attempted_new_psk      = request.form['psk_new_ap']
            attempted_confirm_psk  = request.form['psk_confirm_ap']
            print(attempted_ssid)
            print(attempted_psk)
            print(attempted_new_psk)
            print(attempted_confirm_psk)
            return render_template('restart.html', message = "Changing Settings", alert_msg = "Settings Updated")
    except Exception as e:
        print(str(e))
        return render_template('404.html'), "Error"
        
    return "yolo"

# @app.route('/isucceed')
# def iSucceed():
#     Thread(target=resetDevice).start()
#     return render_template('connectionSucceed.html')

@app.route('/ierror')
def iError():
    flash("wifi连接错误,请重新输出wifi名及密码")
    access_point=""
    return render_template('configWifi.html', access_point=access_point)


@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('404.html'), 404

@app.route('/configradar')
def configRadar():
    app_setting = AppSetting(AppConfig.load_config(config_file))

    sensor_height, sensor_yaw, sensor_pitch = vital_monitor.getSensorPose()
    leftX, rightX, nearY, farY, bottomZ, topZ = vital_monitor.getBoundaryBox()

    app_setting = AppSetting(AppConfig.load_config(config_file))

    return render_template('configRadar.html', sensor_height=sensor_height, sensor_yaw=sensor_yaw, sensor_pitch=sensor_pitch, \
                           leftX=leftX, rightX=rightX, nearY=nearY, farY=farY, bottomZ=bottomZ, topZ=topZ, \
                           server_addr=app_setting.server_addr, server_port=app_setting.server_port)

# app.run(threaded=True, debug=True)                                          # Starting the Flask App and giving it permission to be accessable by all the ip addresses.

@app.route('/setposition', methods=["POST"])
def setPosition():
    global setting_cb
    sensor_height = request.form['sensorHeight']
    sensor_yaw = request.form['sensorYaw']
    sensor_pitch = request.form['sensorPitch']
    vital_monitor.setSensorPose(float(sensor_height), float(sensor_yaw), float(sensor_pitch))
    return redirect(url_for("configRadar"))
    # leftX, rightX, nearY, farY, bottomZ, topZ = vital_monitor.getBoundaryBox()
    # app_setting = AppSetting(AppConfig.load_config(config_file))

    # return render_template('configRadar.html', sensor_height=sensor_height, sensor_yaw=sensor_yaw, sensor_pitch=sensor_pitch, \
    #                        leftX=leftX, rightX=rightX, nearY=nearY, farY=farY, bottomZ=bottomZ, topZ=topZ, \
    #                        server_addr=app_setting.server_addr, server_port=app_setting.server_port)

@app.route('/setboundbox', methods=["POST"])
def setBoundBox():
    global setting_cb
    leftX = request.form['leftX']
    rightX = request.form['rightX']
    nearY = request.form['nearY']
    farY = request.form['farY']
    bottomZ = request.form['bottomZ']
    topZ = request.form['topZ']    

    vital_monitor.setBoundaryBox(float(leftX), float(rightX), float(nearY), float(farY), float(bottomZ), float(topZ))

    return redirect(url_for("configRadar"))

    # sensor_height, sensor_yaw, sensor_pitch = vital_monitor.getSensorPose()
    # app_setting = AppSetting(AppConfig.load_config(config_file))

    # # mainWin.setBoundaryBox(float(leftX), float(rightX), float(nearY), float(farY), float(bottomZ), float(topZ))
    # return render_template('configRadar.html', sensor_height=sensor_height, sensor_yaw=sensor_yaw, sensor_pitch=sensor_pitch, \
    #                        leftX=leftX, rightX=rightX, nearY=nearY, farY=farY, bottomZ=bottomZ, topZ=topZ, \
    #                        server_addr=app_setting.server_addr, server_port=app_setting.server_port)

@app.route('/setserver', methods=["POST"])
def setServer():
    global setting_cb
    server_addr = request.form['serverAddr']
    server_port = int(request.form['serverPort'])

    vital_monitor.setServerAddress(server_addr, server_port)

    app_setting = AppSetting(AppConfig.load_config(config_file))
    app_setting.server_addr = server_addr
    app_setting.server_port = server_port
    AppConfig.save_config(config_file, app_setting)

    return redirect(url_for("configRadar"))

    # sensor_height, sensor_yaw, sensor_pitch = vital_monitor.getSensorPose()
    # leftX, rightX, nearY, farY, bottomZ, topZ = vital_monitor.getBoundaryBox()
    
    # # mainWin.setServerAddress(serverAddr, int(serverPort))
    # return render_template('configRadar.html', sensor_height=sensor_height, sensor_yaw=sensor_yaw, sensor_pitch=sensor_pitch, \
    #                        leftX=leftX, rightX=rightX, nearY=nearY, farY=farY, bottomZ=bottomZ, topZ=topZ, \
    #                        server_addr=server_addr, server_port=server_port)

def startWeb(wifi_controller_, config_file_ = "", wifi_init_page = False, vital_monitor_ = None) :
    global config_file
    global wifi_init
    global wifi_controller
    global vital_monitor

    config_file = config_file_
    wifi_init = wifi_init_page
    wifi_controller = wifi_controller_

    vital_monitor = vital_monitor_

    # while True:
    #     sleep(1)

    # app.run()
    app.run(host='0.0.0.0',port = 80, threaded=True)                                          # Starting the Flask App and giving it permission to be accessable by all the ip addresses.
    pass

if __name__ == '__main__':


    # os.system("sudo nmcli con down my-hotspot")
    # os.system("sudo nmcli radio all off")
    # time.sleep(2)
    # os.system("sudo nmcli radio all on")
    # # print("nmcli radio wwan off")
    # time.sleep(1)
    wifi_controller_ = WifiController('wlan0', 'my-hotspot','www.xiaomianao.net.cn')

    startWeb(wifi_controller_=wifi_controller_, wifi_init_page = True)                                   # Starting the Flask App and giving it permission to be accessable by all the ip addresses.
