# -*- coding: utf-8 -*-
# Copyright (c) 2018 Richard Hull & Contributors
# See LICENSE.md for details.

"""
Alternative pin mappings for Orange PI 4
(https://drive.google.com/drive/folders/1jALhyhwjSVsxwSX1MwhjiOyQdx_fwlFg)

Usage:

.. code:: python
   import orangepi.4
   from OPi import GPIO

   GPIO.setmode(orangepi.4.BOARD) or GPIO.setmode(orangepi.4.BCM)
"""

# pin number = (position of letter in alphabet - 1) * 32 + pin number
# So, PD14 will be (4 - 1) * 32 + 14 = 110

# Orange Pi 4 physical board pin to GPIO pin
BOARD = {
	3:47,
	5:46,
	7:54,
	8:131,
	10:132,
	12:29,
	11:138,
	13:139,
	15:28,
	16:59,
	18:58,
	19:49,
	21:48,
	22:92,
	24:52,
	23:50,
	26:35
}

# No reason for BCM mapping, keeping it for compatibility
BCM = BOARD
