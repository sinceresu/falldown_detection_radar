import errno
import json
import os
import typing as t

class AppSetting(dict):
  def __init__(self, app_config: dict, defaults: t.Optional[dict] = None) -> None:
    super().__init__(defaults or {})

    self.hotspot_name = app_config.get('hotspotName', "my-hotspot")
    self.hotspot_ssid = app_config.get('hotspotSsid', "healthtrack")
    self.hotspot_password = app_config.get('hotspotPassword', "12345678")
    self.server_addr = app_config.get('serverAddr', "www.xiaomianao.net.cn")
    self.server_port = app_config.get('serverPort', 33333)
    self.device_id = app_config.get('deviceID', 1)
    self.fall_thresh = app_config.get('fallThresh', -0.4)
    self.distance_thresh = app_config.get('distanceThresh', 0.5)
    self.send_period = app_config.get('sendPeriod', 5)
    self.falldown_detection = app_config.get('fallDetecion', True)

    pass

  def to_json(self) :
    app_config =  {
      "hotspotName": self.hotspot_name,
      "hotspotSsid": self.hotspot_ssid,
      "hotspotPassword": self.hotspot_password,
      "serverAddr": self.server_addr,
      "serverPort": self.server_port,
      "deviceID": self.device_id,
      "fallThresh": self.fall_thresh,
      "distanceThresh": self.distance_thresh,
      "sendPeriod": self.send_period
    }
    return app_config

class AppConfig():


  def load_config(config_file_name) :
    if  not os.path.exists(config_file_name) :
      return {}

    with open(config_file_name, 'r') as json_file :
      json_data = json.loads(json_file.read())
      return json_data


  def save_config(config_file_name, app_setting) :
    with open(config_file_name, 'w') as json_file :
      app_config = app_setting.to_json()
      json.dump(app_config, json_file, ensure_ascii=False, indent=0)
      
