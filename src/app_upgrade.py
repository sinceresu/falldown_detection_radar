# -*- coding: UTF-8 -*-
import os
import requests
import urllib.request
import time

###############################################################
#        自动升级程序
#  1. 本地需要两个文件，一个是保存序列号的文件serialConfig.dat，一个是保存版本号的文件version.dat
#  2. 服务器保存版本号的文件在app/config/update.txt, 格式：类别,版本号,升级文件名（vitalradar,1.6.4,update.zip）
#  3. 升级时，需要将所有必需的文件压缩打包，上传到服务器的/home/healthftpuser/ftproot/updates/vitalradar
#  4. 然后修改update.txt文件里的版本号
#  5. 运行此程序就可以调用API获得服务器update.txt里的版本号，和本地的serialConfig.dat里的版本号比较，如果不同，则下载update.zip，并解压缩
##############################################################


#调用API，获取当前版本。 服务器上版本配置文件在app/config/update.txt
checkverAPI = "api/update/vitalradar"


#################初始化变量#####################################
deviceSerial = 0
version = "0"

#serverAddr = 'ec2-3-26-5-20.ap-southeast-2.compute.amazonaws.com'

#服务器地址
serverAddr = 'www.xiaomianao.net.cn'
#################初始化变量#####################################

class AppUpgrade() :
    def __init__(self, server_addr, server_port, serial_file, version_file) :
        self.server_addr = server_addr
        self.server_port = server_port
        self.serial_file = serial_file
        self.version_file = version_file

    def checkOnline(self) :
            #check if the device is online
        response = os.system("ping -c 2  " + self.server_addr + " > /dev/null 2>&1")
        return response == 0
  
    # read serial number and version from config files
    def readConfig(self) :
        deviceSerial = None
        version = None
        # read device serial
        if os.path.isfile(self.serial_file):
            # Using readlines()
            file = open(self.serial_file, 'r')
            Lines = file.readlines()

            # Strips the newline character
            for line in Lines:
                if "serial" in line.lower():
                    array = line.strip().split('=')
                    if len(array) == 2:
                        try:
                            deviceSerial = int(array[1])
                        except:
                            print(self.serial_file + '读取错误！')
            file.close()
            #print("devideSerial = ", deviceSerial)


            #read version
            if os.path.isfile(self.version_file):
                # Using readlines()
                file = open(self.version_file, 'r')
                Lines = file.readlines()

                # Strips the newline character
                for line in Lines:
                    if "version" in line.lower():

                        array = line.strip().split('=')
                        if len(array) == 2:
                            try:
                                version = array[1]
                            except:
                                print(self.version_file + '读取错误！')
            else:
                version = '0.0'
            file.close()
        return deviceSerial, version
            #print("version = ", version)

    #compare the version from the web service and the version from local file
    #if they are different, download update.zip and overwrite the program
    def needUpgrading(self):
        deviceSerial, version = self.readConfig()
        if deviceSerial == None or  version == None:
            print("Can not read device serial and version")
            return False
        try:
            #deviceSerialbytes = deviceSerial.to_bytes(4, 'little')
            deviceSerialbytes = int(str(deviceSerial), 16).to_bytes(4, 'little')
            checkVersionUrl = "{}:{}/{}/{}".format(self.server_addr, self.server_port, checkverAPI, deviceSerialbytes.hex())
            print("checkVersionUrl=", checkVersionUrl)
            response = requests.get(checkVersionUrl)
            if (response.status_code == 200):
                # print("response=", response)
                print("response.json()=", response.json())
                self.newVersion = response.json()["ver"]
                self.packageUrl = response.json()["url"]
                print(response.json()["url"])
                print("version is {}, new_version is ", version)
                if self.newVersion != version:
                    return True
                else:
                    return False
            elif response.status_code == 404:
                return False
        except:
            return False


    def upgrade(self, newVersion, upgrade_file_url):
        print("upgrading")

        url_request = urllib.request.Request(self.packageUrl)
        url_connect = urllib.request.urlopen(url_request)

        print("downloading update.zip....")
        with open("update.zip", 'wb') as f:
            while True:
                buffer_size = 100
                buffer = url_connect.read(buffer_size)
                if not buffer: break

                # an integer value of size of written data
                data_wrote = f.write(buffer)

        # you could probably use with-open-as manner
        url_connect.close()

        #unzip update.zip
        os.system("unzip -o update.zip")

        os.system("sudo rm " + self.version_file)
        with open(self.version_file, 'w') as f:
            f.write("version=" + self.newVersion)

        os.system("chmod a+rx *.sh")

        print("reboot device!!!")
        time.sleep(2)
        os.system("sudo reboot")

if __name__ == '__main__':
    app_upgrade = AppUpgrade('www.xiaomianao.net.cn', 33333, 'configs/serialConfig.dat', 'configs/version.dat')
    if app_upgrade.needUpgrade() :
        app_upgrade.upgrade()
    print("no need to upgrade!!!")
    