#!/usr/bin/python
#-- coding:utf8 --
import subprocess
import signal
import string
import random
import re
import json
import time
import os
import socket
import requests
import re
from multiprocessing import Process

from threading import Thread

import PiFiServer
from PiFiServer import pifi_web_server
from PiFiServer.commandExec import commandExec
from PiFiServer.wifi_controller import  WifiController
from  app_config import AppConfig, AppSetting
from  app_upgrade import AppUpgrade
import vital_monitor


# gpio  = False
gpio  = True

if gpio :
  import io_controll



currentdir = os.path.dirname(os.path.abspath(__file__))
os.chdir(currentdir)
            
def getConfigDir():
  configDirectory = ''
  try:
      # current_dir = os.path.dirname(sys.argv[0])
      current_dir = os.getcwd()
      configDirectory = os.path.join(current_dir, 'configs')

  except:
      configDirectory = ''
  return configDirectory




def getWiFiInterface():
    interface_name = ''
    wlan_status = os.popen("nmcli device ").readlines()  # 获取网卡连接状态
    # print(wlan_status)
    wlan_status = [re.split("\s{2,}", x) for x in wlan_status]  # 处理数据

    # print(wlan_status)
    for status in wlan_status:
        if status[1] == "wifi":
            if len(status) > 0:
                interface_name =  status[0]  # 返回无线网卡状态，数组
    count = 0
    while interface_name.strip() == "" and count < 10:
        print("getWiFiInterface()=", interface_name)
        # os.system("sudo systemctl restart NetworkManager")
        time.sleep(5)
        count = count + 1
        interface_name = getWiFiInterface()
                
    return interface_name


hotspot_name = 'my-hotspot'

    # os.system("nmcli con up my-hotspot")

if __name__ == "__main__":

  # check connection
  # os.system("sudo nmcli con delete {}".format(hotspot_name))

  # os.system("nmcli radio all off")
  # time.sleep(1)
  if gpio:
    io_controll.initialize()
    io_controll.testLED()
      
  os.system("nmcli radio all on")
  # print("nmcli radio wwan off")
  time.sleep(4)

  config_dir = getConfigDir()
  app_setting = AppSetting(AppConfig.load_config(os.path.join(config_dir, 'app_config.json'),))

  wifi_controller = WifiController(getWiFiInterface(), app_setting.hotspot_name, app_setting.server_addr)

  if gpio:
    input = io_controll.getResetWifiPin()
    if (not input) :
      io_controll.setBlueLed(True)
      wifi_controller.startHotspot(app_setting.hotspot_ssid, app_setting.hotspot_password)
      print("start webserver to configure wifi.")
      pifi_web_server.startWeb(wifi_controller, True)
  

  if wifi_controller.checkOnline():
    app_upgrade = AppUpgrade(app_setting.server_addr, app_setting.server_port, \
                             os.path.join(config_dir, 'serialConfig.dat'), os.path.join(config_dir, 'version.dat'))
    if not app_upgrade.checkOnline() :
        print("can not connect to server:{}, please check network configuration.".format(app_setting.server_addr))
    else :
      if app_upgrade.needUpgrading() :
        app_upgrade.upgrade()
        
    vital_monitor = vital_monitor.VitalMonitor(config_dir)

    vital_monitor.setServerAddress(app_setting.server_addr, app_setting.server_port)
    vital_monitor.setDeviceID(app_setting.device_id)
    vital_monitor.setDetectionParameters(app_setting.fall_thresh, app_setting.distance_thresh, \
                                         app_setting.send_period, app_setting.falldown_detection)       

    print("start webserver to configure radar.")
    Thread(target=pifi_web_server.startWeb, args=(wifi_controller, os.path.join(config_dir, 'app_config.json'), False, vital_monitor)).start()

    vital_monitor.start()
      
  else:
      io_controll.setBlueLed(True)
      
      wifi_controller.startHotspot(app_setting.hotspot_ssid, app_setting.hotspot_password)
      print("start webserver to configure wifi.")
      pifi_web_server.startWeb(wifi_controller, os.path.join(config_dir, 'app_config.json'), True)


