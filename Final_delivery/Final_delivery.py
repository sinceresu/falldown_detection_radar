import numpy as np
import serial
import time
from sklearn.metrics.pairwise import euclidean_distances
from scipy.spatial import distance
import random
from scipy.optimize import linear_sum_assignment
import scipy.signal as sci_signal
import pyqtgraph as pg
import warnings
from multiprocessing import Pool, Manager
from NMS_Cython import NMS_mmWave
import Numba_Module
from sklearn.cluster import DBSCAN
import mmwave.dsp as dsp
import copy
from scipy import signal as sg
from numpy import linalg as lg
import socket


# region MOT Tracking
# !!!!!!!!!!!!!!!!!!!!
radar_Height = 1.0  # Radar Height that should be modified according to the actual deployment!!!!!!!!!
framelen_track = 10
mov_framelen_track = 2  # Window Spacing
# endregion

# region Vital Signs
framelen_vsign = 10  # Window Size for MVDR
mov_framelen_vsign = 8
window_vsign = 40  # Window Size for Vital Sign Recognition
sensing_dist = 6.0  # Maximum Sensing Distance (m)
Len_cmplx = window_vsign * mov_framelen_vsign  # Phase Difference
# endregion

# region Radar Initialization
CLIport = {}  # Initialize an empty dictionary to store the configuration parameters
Dataport = {}
byteBuffer = np.zeros(2 ** 15, dtype='uint8')
byteBufferLength = 0
configFileName = 'IWR6843AOP_cfg_IS.cfg'  # Configuration file name
gl_numVirtAnt = 3 * 4
AoA_spacing = 1


# endregion

# ------------------------------------------------------------------
# region Connect and Configure mmWave Radar
# Configure the serial ports
def serialConfig(configFileName):
    # Open the serial ports for the configuration and the data ports
    # CLIport = serial.Serial('/dev/ttyACM0', 115200)
    # Dataport = serial.Serial('/dev/ttyACM1', 921600)

    CLIport = serial.Serial('/dev/ttyUSB0', 115200)
    Dataport = serial.Serial('/dev/ttyUSB1', 921600)

#     CLIport = serial.Serial('/dev/tty.SLAB_USBtoUART', 115200)
#     Dataport = serial.Serial('/dev/tty.SLAB_USBtoUART4', 921600)

    CLIport.write(('sensorStop\n').encode())
    time.sleep(0.1)
    CLIport.write(('flushCfg\n').encode())
    time.sleep(0.1)
    # Read the configuration file and send it to the board
    config = [line.rstrip('\r\n') for line in open(configFileName)]
    for i in config:
        CLIport.write((i + '\n').encode())
        print(i)
        time.sleep(0.02)

    return CLIport, Dataport


# ------------------------------------------------------------------
# Send the data from the configuration file to our IWR6843AOP radar
def parseConfigFile(configFileName):
    configParameters = {}  # Initialize an empty dictionary to store the configuration parameters

    # Read the configuration file and send it to the board
    config = [line.rstrip('\r\n') for line in open(configFileName)]
    for i in config:
        # Split the line
        splitWords = i.split(" ")

        # Hard code the number of antennas, change if other configuration is used
        numRxAnt = 4
        numTxAnt = 3

        # Get the information about the profile configuration
        if "profileCfg" in splitWords[0]:
            startFreq = int(float(splitWords[2]))
            idleTime = int(splitWords[3])
            rampEndTime = float(splitWords[5])
            freqSlopeConst = float(splitWords[8])
            numAdcSamples = int(splitWords[10])
            numAdcSamplesRoundTo2 = 1

            while numAdcSamples > numAdcSamplesRoundTo2:
                numAdcSamplesRoundTo2 = numAdcSamplesRoundTo2 * 2

            digOutSampleRate = int(splitWords[11])

        # Get the information about the frame configuration
        elif "frameCfg" in splitWords[0]:

            chirpStartIdx = int(splitWords[1])
            chirpEndIdx = int(splitWords[2])
            numLoops = int(splitWords[3])
            numFrames = int(splitWords[4])
            framePeriodicity = float(splitWords[5])

    # Combine the read data to obtain the configuration parameters
    numChirpsPerFrame = (chirpEndIdx - chirpStartIdx + 1) * numLoops
    configParameters["numDopplerBins"] = numChirpsPerFrame // numTxAnt
    configParameters["numRangeBins"] = numAdcSamplesRoundTo2
    configParameters["rangeResolutionMeters"] = (3e8 * digOutSampleRate * 1e3) / (
            2 * freqSlopeConst * 1e12 * numAdcSamples)
    configParameters["rangeIdxToMeters"] = (3e8 * digOutSampleRate * 1e3) / (
            2 * freqSlopeConst * 1e12 * configParameters["numRangeBins"])
    configParameters["dopplerResolutionMps"] = 3e8 / (
            2 * startFreq * 1e9 * (idleTime + rampEndTime) * 1e-6 * configParameters["numDopplerBins"] * numTxAnt)
    configParameters["maxRange"] = (300 * 0.9 * digOutSampleRate) / (2 * freqSlopeConst * 1e3)
    configParameters["maxVelocity"] = 3e8 / (4 * startFreq * 1e9 * (idleTime + rampEndTime) * 1e-6 * numTxAnt)
    configParameters["framePeriodicity"] = framePeriodicity

    rangeArray = np.array(range(configParameters["numRangeBins"])) * configParameters["rangeIdxToMeters"]
    thetaArray = np.array(range(-90, 91, AoA_spacing)).astype(np.float64)
    # np.rad2deg(np.arcsin(np.array(range(-gl_numAngleBins // 2 + 1, gl_numAngleBins // 2)) *
    #                                    (2 / gl_numAngleBins)))
    dopplerArray = np.multiply(np.arange(-configParameters["numDopplerBins"] / 2,
                                         configParameters["numDopplerBins"] / 2),
                               configParameters["dopplerResolutionMps"])

    return configParameters, rangeArray, thetaArray, dopplerArray


# Read the mmWave data from the radar
def readAndParseData6843AoP(Dataport, configParameters):
    global byteBuffer, byteBufferLength

    # Constants
    OBJ_STRUCT_SIZE_BYTES = 12
    BYTE_VEC_ACC_MAX_SIZE = 2 ** 15
    MMWDEMO_UART_MSG_DETECTED_POINTS = 1
    MMWDEMO_UART_MSG_RANGE_PROFILE = 2
    MMWDEMO_OUTPUT_MSG_NOISE_PROFILE = 3
    MMWDEMO_OUTPUT_MSG_RANGE_DOPPLER_HEAT_MAP = 5
    MMWDEMO_OUTPUT_MSG_STATS = 6
    MMWDEMO_OUTPUT_MSG_DETECTED_POINTS_SIDE_INFO = 7
    MMWDEMO_OUTPUT_MSG_AZIMUT_ELEVATION_STATIC_HEAT_MAP = 8
    MMWDEMO_OUTPUT_MSG_TEMPERATURE_STATS = 9

    maxBufferSize = 2 ** 15
    tlvHeaderLengthInBytes = 8
    pointLengthInBytes = 16
    magicWord = [2, 1, 4, 3, 6, 5, 8, 7]

    readBuffer = Dataport.read(Dataport.in_waiting)
    byteVec = np.frombuffer(readBuffer, dtype='uint8')
    byteCount = len(byteVec)

    # Initialize variables
    magicOK = 0  # Checks if magic number has been read
    dataOK = 0  # Checks if the data has been read correctly
    dataOK_cmplx = 0
    frameNumber = 0
    numDetectedObj = 0
    detected_points = {}
    side_info = {}
    cmplx_rgAntAziEle = []

    if byteCount == 0:
        return dataOK, dataOK_cmplx, frameNumber, numDetectedObj, detected_points, side_info, cmplx_rgAntAziEle

    # Check that the buffer is not full, and then add the data to the buffer
    if (byteBufferLength + byteCount) < maxBufferSize:
        byteBuffer[byteBufferLength:byteBufferLength + byteCount] = byteVec[:byteCount]
        byteBufferLength = byteBufferLength + byteCount

    # Check that the buffer has some data
    if byteBufferLength > 16:

        # Check for all possible locations of the magic word
        possibleLocs = np.where(byteBuffer == magicWord[0])[0]

        # Confirm that is the beginning of the magic word and store the index in startIdx
        startIdx = []
        for loc in possibleLocs:
            check = byteBuffer[loc:loc + 8]
            if np.all(check == magicWord):
                startIdx.append(loc)

        # Check that startIdx is not empty
        if startIdx:

            # Remove the data before the first start index
            if 0 < startIdx[0] < byteBufferLength:
                byteBuffer[:byteBufferLength - startIdx[0]] = byteBuffer[startIdx[0]:byteBufferLength]
                byteBuffer[byteBufferLength - startIdx[0]:] = np.zeros(len(byteBuffer[byteBufferLength - startIdx[0]:]),
                                                                       dtype='uint8')
                byteBufferLength = byteBufferLength - startIdx[0]

            # Check that there have no errors with the byte buffer length
            if byteBufferLength < 0:
                byteBufferLength = 0

            # word array to convert 4 bytes to a 32 bit number
            word = [1, 2 ** 8, 2 ** 16, 2 ** 24]

            # Read the total packet length
            totalPacketLen = np.matmul(byteBuffer[12:12 + 4], word)

            # Check that all the packet has been read
            if (byteBufferLength >= totalPacketLen) and (byteBufferLength != 0):
                magicOK = 1

    # If magicOK is equal to 1 then process the message
    if magicOK:
        # Initialize the pointer index
        idX = 0

        # Read the header
        magicNumber = byteBuffer[idX:idX + 8].view(dtype=np.uint16)[0]
        idX += 8
        version = byteBuffer[idX:idX + 4].view(dtype=np.uint32)[0]
        idX += 4
        totalPacketLen = byteBuffer[idX:idX + 4].view(dtype=np.uint32)[0]
        idX += 4
        platform = byteBuffer[idX:idX + 4].view(dtype=np.uint32)[0]
        idX += 4
        frameNumber = byteBuffer[idX:idX + 4].view(dtype=np.uint32)[0]
        idX += 4
        timeCpuCycles = byteBuffer[idX:idX + 4].view(dtype=np.uint32)[0]
        idX += 4
        numDetectedObj = byteBuffer[idX:idX + 4].view(dtype=np.uint32)[0]
        idX += 4
        numTLVs = byteBuffer[idX:idX + 4].view(dtype=np.uint32)[0]
        idX += 4
        subFrameNumber = byteBuffer[idX:idX + 4].view(dtype=np.uint32)[0]
        idX += 4
        frame_header = {"magicNumber": magicNumber,
                        "version": version,
                        "totalPacketLen": totalPacketLen,
                        "platform": platform,
                        "frameNumber": frameNumber,
                        "timeCpuCycles": timeCpuCycles,
                        "numDetectedObj": numDetectedObj,
                        "numTLVs": numTLVs,
                        "subFrameNumber": subFrameNumber}

        # Read the TLV messages
        for tlvIdx in range(numTLVs):
            # print(byteBuffer[idX:idX + 4])
            # if len(byteBuffer[idX:idX + 4]) == 0:
            #     print('tlv_type is null.0000000000000000000')
            #     continue

            # Check the header of the TLV message
            tlv_type = byteBuffer[idX:idX + 4].view(dtype=np.uint32)
            idX += 4
            tlv_length = byteBuffer[idX:idX + 4].view(dtype=np.uint32)
            idX += 4
            # # word array to convert 4 bytes to a 32 bit number
            # word = [1, 2 ** 8, 2 ** 16, 2 ** 24]
            #
            # # Check the header of the TLV message
            # tlv_type = np.matmul(byteBuffer[idX:idX + 4], word)
            # idX += 4
            # tlv_length = np.matmul(byteBuffer[idX:idX + 4], word)
            # idX += 4

            # Read the data depending on the TLV message
            warnings.filterwarnings(action='ignore', category=DeprecationWarning)
            try:
                if tlv_type == MMWDEMO_UART_MSG_DETECTED_POINTS:
                    # Initialize the arrays
                    x = np.zeros(numDetectedObj, dtype=np.float32)
                    y = np.zeros(numDetectedObj, dtype=np.float32)
                    z = np.zeros(numDetectedObj, dtype=np.float32)
                    velocity = np.zeros(numDetectedObj, dtype=np.float32)

                    for objectNum in range(numDetectedObj):
                        # Read the data for each object
                        x[objectNum] = byteBuffer[idX:idX + 4].view(dtype=np.float32)[0]
                        idX += 4
                        y[objectNum] = byteBuffer[idX:idX + 4].view(dtype=np.float32)[0]
                        idX += 4
                        z[objectNum] = byteBuffer[idX:idX + 4].view(dtype=np.float32)[0]
                        idX += 4
                        velocity[objectNum] = byteBuffer[idX:idX + 4].view(dtype=np.float32)[0]
                        idX += 4

                    # Store the data in the detObj dictionary
                    detected_points = {"x": x, "y": y, "z": z, "velocity": velocity}
                    dataOK += 1

                elif tlv_type == MMWDEMO_OUTPUT_MSG_DETECTED_POINTS_SIDE_INFO:
                    snr = np.zeros(numDetectedObj, dtype=np.int16)
                    noise = np.zeros(numDetectedObj, dtype=np.int16)
                    for objectNum in range(numDetectedObj):
                        snr[objectNum] = byteBuffer[idX:idX + 2].view(dtype=np.int16)[0]
                        idX += 2
                        noise[objectNum] = byteBuffer[idX:idX + 2].view(dtype=np.int16)[0]
                        idX += 2
                    side_info = {"snr": snr, "noise": noise}
                    dataOK += 1

                elif tlv_type == MMWDEMO_OUTPUT_MSG_AZIMUT_ELEVATION_STATIC_HEAT_MAP:
                    numBytes = configParameters["numRangeBins"] * gl_numVirtAnt * 4
                    range_AziEle = byteBuffer[idX:idX + numBytes].view(dtype=np.int16)

                    idX += numBytes
                    range_AziEle_img = range_AziEle[0::2].reshape((configParameters["numRangeBins"], gl_numVirtAnt))
                    range_AziEle_real = range_AziEle[1::2].reshape((configParameters["numRangeBins"], gl_numVirtAnt))
                    cmplx_rgAntAziEle = range_AziEle_real + 1j * range_AziEle_img  # configParameters["numRangeBins"] * gl_numVirtAnt

                    dataOK_cmplx += 1

                    # Some frames have strange values, skip those frames
                    # TO DO: Find why those strange frames happen
                    if np.max(np.abs(cmplx_rgAntAziEle[np.arange(1, configParameters["numRangeBins"] - 1), :])) > 10000:
                        #print('############-------############')
                        #print("An Outlier Frame in TI mmWave Radar.....")
                        #print('############-------############')
                        pass
                        # continue
                    else:
                        dataOK_cmplx += 1
            except:
                print('exception happens!')

        # Remove already processed data
        if 0 < idX < byteBufferLength:
            shiftSize = totalPacketLen

            byteBuffer[:byteBufferLength - shiftSize] = byteBuffer[shiftSize:byteBufferLength]
            byteBuffer[byteBufferLength - shiftSize:] = np.zeros(len(byteBuffer[byteBufferLength - shiftSize:]),
                                                                 dtype='uint8')
            byteBufferLength = byteBufferLength - shiftSize

            # Check that there are no errors with the buffer length
            if byteBufferLength < 0:
                byteBufferLength = 0

    return dataOK, dataOK_cmplx, frameNumber, numDetectedObj, detected_points, side_info, cmplx_rgAntAziEle


# endregion


# ------------------------------------------------------------------
# region Graph Class
class Graph(pg.GraphItem):
    def __init__(self):
        self.dragPoint = None
        self.dragOffset = None
        self.textItems = []
        pg.GraphItem.__init__(self)

    def setData(self, **kwds):
        self.text = kwds.pop('text', [])
        self.data = kwds
        if 'pos' in self.data:
            npts = self.data['pos'].shape[0]
            self.data['data'] = np.empty(npts, dtype=[('index', int)])
            self.data['data']['index'] = np.arange(npts)
        self.setTexts(self.text)
        self.updateGraph()

    def setTexts(self, text):
        for i in self.textItems:
            i.scene().removeItem(i)
        self.textItems = []
        for t in text:
            item = pg.TextItem(t)
            self.textItems.append(item)
            item.setParentItem(self)
            item.setColor('k')
            font = pg.Qt.QtGui.QFont('Arial', 13)
            item.setFont(font)

    def updateGraph(self):
        pg.GraphItem.setData(self, **self.data)
        for i, item in enumerate(self.textItems):
            item.setPos(*self.data['pos'][i])


# endregion


# ------------------------------------------------------------------
# region Customized Functions
def NMS_Clustering(pointObj, bb_Pos_X, bb_Pos_YZ, bb_SNR, box_width, IoU_thres):
    '''' Down-sampling Function
    Args:
        pointObj: N×5 2D array, X, Y, Z, Velocity, SNR
        bb_Pos_X: 1D array, X Positions
        bb_Pos_YZ: 1D array, Y or Z Positions
        bb_SNR: 1D array, SNR
        box_width: Value, Box Width
        IoU_thres: Value, Intersection Over Union (IoU)
    
    Returns:
        res_Ind: Index of Point Clouds of Interests
        res_pointObj: M×5 2D array, Point Clouds of Interests
    '''''
    # Assign a bounding box for each point
    bb_Pos_X_tl = bb_Pos_X - box_width / 2  # bounding box_Pos_X - Top_Left
    bb_Pos_Y_tl = bb_Pos_YZ + box_width / 2  # bounding box_Pos_Y - Top_Left
    bb_Pos_X_br = bb_Pos_X + box_width / 2  # bounding box_Pos_X - Bottom_Right
    bb_Pos_Y_br = bb_Pos_YZ - box_width / 2  # bounding box_Pos_T - Bottom_Right

    # Assign a score to each bounding box
    bb_Score = bb_SNR / np.max(bb_SNR)

    # Apply NMS to extract points of interest
    bb_Obj = np.vstack([bb_Pos_X_tl, bb_Pos_Y_tl, bb_Pos_X_br, bb_Pos_Y_br, bb_Score]).T  # N * 5

    # Implement Cython-based NMS to speed up
    res_Ind = NMS_mmWave.cpu_nms(bb_Obj.astype(np.float32), IoU_thres)
    res_pointObj = pointObj[res_Ind, :]
    return res_Ind, res_pointObj


def compute_autocovariance(x, M):
    # Create covariance matrix for psd estimation
    # length of the vector x
    N = x.shape[0]

    # Create column vector from row array
    x_vect = np.transpose(np.matrix(x))

    # init covariance matrix
    yn = x_vect[M - 1::-1]
    R = yn * yn.H
    for indice in range(1, N - M):
        # extract the column vector
        yn = x_vect[M - 1 + indice:indice - 1:-1]
        R = R + yn * yn.H

    R = R / N
    return R


def root_MUSIC(x, L, M, Fe=1):
    # length of the vector x
    N = x.shape[0]

    if M == None:
        M = N // 2

    # extract noise subspace
    R = compute_autocovariance(x, M)
    U, S, V = lg.svd(R)
    G = U[:, L:]

    # construct matrix P
    P = G * G.H

    # construct polynomial Q
    Q = 0j * np.zeros(2 * M - 1)
    # Extract the sum in each diagonal
    for (idx, val) in enumerate(range(M - 1, -M, -1)):
        diag = np.diag(P, val)
        Q[idx] = np.sum(diag)

    # Compute the roots
    roots = np.roots(Q)

    # Keep the roots with radii <1 and with non zero imaginary part
    roots = np.extract(np.abs(roots) < 1, roots)
    roots = np.extract(np.imag(roots) != 0, roots)

    # Find the L roots closest to the unit circle
    distance_from_circle = np.abs(np.abs(roots) - 1)
    index_sort = np.argsort(distance_from_circle)
    component_roots = roots[index_sort[:L]]

    # extract frequencies ((note that there a minus sign since Yn are defined as [y(n), y(n-1),y(n-2),..].T))
    angle = -np.angle(component_roots)

    # frequency normalisation
    f = Fe * angle / (2. * np.pi)

    return f


def error_handler(exception):
    print(f'{exception} occurred, terminating pool.')
    print("error: ", exception)
    pool.terminate()


# endregion


# ------------------------------------------------------------------
# region Capture Point Cloud and Azimuth-Elevation Heatmap
def Processing_GetData(mmWave_Buffer, syn_PoinyObj, syn_Heatmap, ):
    # Configurate the serial ports
    CLIport, Dataport = serialConfig(configFileName)
    configParameters, rangeArray, thetaArray, dopplerArray = parseConfigFile(configFileName)

    frameNumber_next = 0
    while True:
        time.sleep(0.01)
        # Read and parse the received data
        dataOK, dataOK_cmplx, frameNumber, numDetectedObj, detected_points, side_info, cmplx_rgAntAziEle = \
            readAndParseData6843AoP(Dataport, configParameters)

        if frameNumber >= 1 and dataOK_cmplx == 2 and frameNumber > frameNumber_next:
            frameNumber_next = frameNumber

            if numDetectedObj > 0 and dataOK == 2:
                pointObj_y = np.reshape(detected_points["y"], (-1, 1))
                idx = np.where(pointObj_y >= 0.2)[0]
                if len(idx) == 0:
                    numDetectedObj = 0

            # region Point Clouds
            if numDetectedObj > 0 and dataOK == 2:
                mmWave_Buffer['NULL_POINT_CLOUD'] = 0  # Presence of Point Clouds

                pointObj_FrameID = np.ones((numDetectedObj, 1), dtype=float) * frameNumber
                pointObj_PointNum = np.ones((numDetectedObj, 1), dtype=float) * numDetectedObj
                pointObj_x = np.reshape(detected_points["x"], (-1, 1))
                pointObj_y = np.reshape(detected_points["y"], (-1, 1))
                pointObj_z = np.reshape(detected_points["z"], (-1, 1)) + radar_Height
                pointObj_velocity = np.reshape(detected_points["velocity"], (-1, 1))
                pointObj_snr = np.reshape(20 * np.log10(side_info["snr"] + 1), (-1, 1))
                pointObj = np.concatenate((pointObj_FrameID, pointObj_PointNum,
                                           pointObj_x, pointObj_y, pointObj_z,
                                           pointObj_velocity, pointObj_snr), axis=1)
                # noise point clouds close to (0, 0)
                idx = np.where(pointObj[:, 3] >= 0.2)[0]
                #print('Size of Point Clouds:', pointObj.shape[0])

                if len(idx) > 0:
                    pointObj = pointObj[idx, :]
                    pointObj[:, 1] = len(idx)
                    #print('Size of Point Clouds (Filtered):', pointObj.shape[0])
                    mmWave_Buffer['GetData_PointCloud'] = np.vstack((mmWave_Buffer['GetData_PointCloud'], pointObj))
                    # print(np.shape(mmWave_Buffer['GetData_PointCloud']))
            else:
                mmWave_Buffer['NULL_POINT_CLOUD'] += 1  # Absence of Point Clouds
                if mmWave_Buffer['NULL_POINT_CLOUD'] >= 10:
                    mmWave_Buffer['NULL_POINT_CLOUD'] = 10
            syn_PoinyObj.release()
            # endregion

            # region Heatmap
            mmWave_Buffer['GetData_Cmplx'] = \
                np.vstack((mmWave_Buffer['GetData_Cmplx'], cmplx_rgAntAziEle[None]))  # cmplx_rgAntAziEle 64 * 12
            syn_Heatmap.release()
            # endregion


# endregion


# ------------------------------------------------------------------
# region Multiple Object Tracking
def Processing_MOT(syn_PoinyObj, mmWave_Buffer, Tracks, ):
    # region Get the configuration parameters of Radar
    configParameters, rangeArray, thetaArray, dopplerArray = parseConfigFile(configFileName)
    Fs = 1 / (configParameters["framePeriodicity"] * 0.001)  # Sampling Frequency
    Ts = 1 / Fs  # Sampling Time Interval
    # endregion

    # region MOT Parameters
    # NMS Parameters
    box_width = 0.4
    IoU_thres = 0.6
    # Gating Parameter which provides a boundary in association process
    dist_Gate_WA = 1.2
    # endregion

    # region Initialize tracks and trajectory
    tracks = {}
    tracks['tid'] = np.empty((0, 1), dtype=int)  # ID
    tracks['position'] = np.empty((0, 3), dtype=float)  # Optimized Position (XYZ)
    tracks['velocity'] = np.empty((0, 3), dtype=float)  # Optimized Velocity (XYZ)
    tracks['acceleration'] = np.empty((0, 3), dtype=float)  # Optimized acceleration (XYZ)
    tracks['position_w'] = np.empty((0, 3), dtype=float)  # Raw Positions (XYZ)
    tracks['doppler_velocity'] = np.empty((0, 1), dtype=float)  # Raw Doppler Velocity
    tracks['covariance_matrix'] = np.empty((0, 9, 9), dtype=float)  # Initialized covariance matrix in Kalman Filter
    tracks['age'] = np.empty((0, 1), dtype=int)  # Trajectory age
    tracks['totalVisibleCount'] = np.empty((0, 1), dtype=int)  # Total Visible Count of a trajectory
    tracks['consecutiveInvisibleCount'] = np.empty((0, 1), dtype=int)  # Consecutive Invisible Count of a trajectory
    tracks['tracklets'] = np.empty((0, framelen_track, 15), dtype=float)  # tracklets
    tracks['activity'] = np.empty((0, 1), dtype=np.string_)  # Activity
    tracks['trajectory_status'] = np.empty((0, 1), dtype=int)  # 0-Waiting, 1-Confirmed, 2-Sleeping
    # endregion

    flag_MOT = 0
    numofTrackLet = 0
    pre_trackLet = []
    curr_trackLet = []
    while True:
        syn_PoinyObj.acquire()

        # region 0. Update Tracks
        if mmWave_Buffer['NULL_POINT_CLOUD'] >= 10:
            Tracks['tid'] = np.empty((0, 1), dtype=int)
            Tracks['position'] = np.empty((0, 3), dtype=float)
            Tracks['velocity'] = np.empty((0, 3), dtype=float)
            Tracks['acceleration'] = np.empty((0, 3), dtype=float)
            Tracks['height_curve'] = np.empty((0, framelen_track), dtype=float)
            Tracks['activity'] = np.empty((0, 1), dtype=np.string_)

            tracks['tid'] = np.empty((0, 1), dtype=int)  # ID
            tracks['position'] = np.empty((0, 3), dtype=float)  # Optimized Position (XYZ)
            tracks['velocity'] = np.empty((0, 3), dtype=float)  # Optimized Velocity (XYZ)
            tracks['acceleration'] = np.empty((0, 3), dtype=float)  # Optimized acceleration (XYZ)
            tracks['position_w'] = np.empty((0, 3), dtype=float)  # Raw Positions (XYZ)
            tracks['doppler_velocity'] = np.empty((0, 1), dtype=float)  # Raw Doppler Velocity
            tracks['covariance_matrix'] = np.empty((0, 9, 9),
                                                   dtype=float)  # Initialized covariance matrix in Kalman Filter
            tracks['age'] = np.empty((0, 1), dtype=int)  # Trajectory age
            tracks['totalVisibleCount'] = np.empty((0, 1), dtype=int)  # Total Visible Count of a trajectory
            tracks['consecutiveInvisibleCount'] = np.empty((0, 1),
                                                           dtype=int)  # Consecutive Invisible Count of a trajectory
            tracks['tracklets'] = np.empty((0, framelen_track, 15), dtype=float)  # tracklets
            tracks['activity'] = np.empty((0, 1), dtype=np.string_)  # Activity
            tracks['trajectory_status'] = np.empty((0, 1), dtype=int)  # 0-Waiting, 1-Confirmed, 2-Sleeping

            if len(tracks['tid']) == 0:
                numofTrackLet = 0
        # endregion

        # ----------------------------------------------------------
        # ------------Multiple Object Tracking----------------------
        framebuff_ID = np.array(list(set(mmWave_Buffer['GetData_PointCloud'][:, 0])))  # find all frame_ID
        framebuff_Len = len(framebuff_ID)
        if framebuff_Len < framelen_track:
            if flag_MOT == 0:
                #print('---------- (1) Multiple Object Tracking ----------')
                #print('Number of frames:', framebuff_Len, '#')
                pass
        else:
            flag_MOT = 1
            #print('----------(1) Multiple Object Tracking ----------')
            #print("FrameID:", int(framebuff_ID[0]), "#")
            start_MOT = time.time()

            # region 1. Extract the points of interest using NMS Algorithm
            pcloud_key = np.where(mmWave_Buffer['GetData_PointCloud'][:, 0] == framebuff_ID[0])[0]
            pointObj = mmWave_Buffer['GetData_PointCloud'][pcloud_key, :]
            pointObj = pointObj[:, np.arange(2, 7)]  # X, Y, Z, Velocity, SNR
            res_Ind, res_pointObj = \
                NMS_Clustering(pointObj, pointObj[:, 0], pointObj[:, 1], pointObj[:, 4], box_width, IoU_thres)
            # endregion

            # region 2. Search a trackLet for each extracted point
            count, copy_trackLet = Numba_Module.Search_TrackLet(mmWave_Buffer['GetData_PointCloud'],
                                                                framebuff_ID, res_pointObj, Ts, framelen_track)
            # Initialize trackLet
            # 0. track_Id,
            # 1. Pos_X, 2. Pos_Y, 3. Pos_Z,                 # Position
            # 4. Vx, 5. Vy, 6. Vz,                          # Velocity
            # 7. ax, 8. ay, 9. az,                          # Acceleration
            # 10. Pos_X_w, 11. Pos_Y_w, 12. Pos_Z_w,        # Position without optimization
            # 13. Doppler_Velocity, 14. SNR                 # Doppler Velocity, SNR
            trackLet = np.empty((0, framelen_track, 15), dtype=float)
            for k_count in range(count):
                trackLet = np.vstack((trackLet,
                                      copy_trackLet[np.arange(k_count * framelen_track,
                                                              (k_count + 1) * framelen_track), :][None]))
            # print(trackLet[:, 0, range(11)])
            # endregion

            # region 3. Clear tracking buffer
            for i_del in range(mov_framelen_track):
                mmWave_Buffer['GetData_PointCloud'] = \
                    np.delete(mmWave_Buffer['GetData_PointCloud'],
                              range(int(mmWave_Buffer['GetData_PointCloud'][0, 1])), axis=0)
            # print('############-------############')
            print('                   (1) Length of GetData_PointCloud:',
                  len(np.array(list(set(mmWave_Buffer['GetData_PointCloud'][:, 0])))))
            # print('############-------############')
            # endregion

            if len(trackLet) > 0:
                # region 4. Filter some overlapped point clouds
                KalmanFilt_pointObj = np.zeros((trackLet.shape[0], 3), dtype=float)
                for k in range(trackLet.shape[0]):
                    for j in range(2):
                        KalmanFilt_pointObj[k, j] = np.average(trackLet[k, :, 10 + j], weights=trackLet[k, :, 14])
                    KalmanFilt_pointObj[k, 2] = np.average(trackLet[k, :, 14])
                res_Ind, _ = \
                    NMS_Clustering(KalmanFilt_pointObj,
                                   KalmanFilt_pointObj[:, 0], KalmanFilt_pointObj[:, 1],
                                   KalmanFilt_pointObj[:, 2], box_width, IoU_thres)
                trackLet = trackLet[res_Ind, :, :]
                # endregion

                # region 5. Save trackLets of two consecutive frames for association
                if numofTrackLet == 0:
                    pre_trackLet = trackLet
                    numofTrackLet += 1

                    # Create tracks
                    for k in range(np.shape(pre_trackLet)[0]):
                        res_IDs = sorted(list(set(np.arange(0, 200)) - set(tracks['tid'][:, 0])))
                        # ID
                        tracks['tid'] = np.vstack((tracks['tid'], [[res_IDs[0]]]))

                        # Position
                        tracks_pos = pre_trackLet[k, :, np.arange(1, 4)]
                        tracks_pos = np.median(tracks_pos, axis=1).reshape(1, -1)
                        # tracks_pos = pre_trackLet[k, 0, np.arange(1, 4)].reshape(1, -1)
                        tracks['position'] = np.vstack((tracks['position'], tracks_pos))
                        # Velocity
                        tracks_vel = pre_trackLet[k, :, np.arange(4, 7)]
                        tracks_vel = np.median(tracks_vel, axis=1).reshape(1, -1)
                        # tracks_vel = pre_trackLet[k, 0, np.arange(4, 7)].reshape(1, -1)
                        tracks['velocity'] = np.vstack((tracks['velocity'], tracks_vel))
                        # Acceleration
                        tracks_acc = pre_trackLet[k, :, np.arange(7, 10)]
                        tracks_acc = np.median(tracks_acc, axis=1).reshape(1, -1)
                        # tracks_acc = pre_trackLet[k, 0, np.arange(7, 10)].reshape(1, -1)
                        tracks['acceleration'] = np.vstack((tracks['acceleration'], tracks_acc))

                        # Position_W.o.
                        tracks_pos_w = pre_trackLet[k, :, np.arange(10, 13)]
                        tracks_pos_w = np.median(tracks_pos_w, axis=1).reshape(1, -1)
                        # tracks_pos_w = pre_trackLet[k, 0, np.arange(10, 13)].reshape(1, -1)
                        tracks['position_w'] = np.vstack((tracks['position_w'], tracks_pos_w))
                        # Doppler
                        tracks_doppler = np.median(pre_trackLet[k, :, 13])
                        # tracks_doppler = pre_trackLet[k, 0, 13]
                        tracks['doppler_velocity'] = np.vstack((tracks['doppler_velocity'],
                                                                [[tracks_doppler]]))

                        # region tracks['covariance_matrix']
                        tracks['covariance_matrix'] = np.vstack((tracks['covariance_matrix'],
                                                                 (np.array([[0.5, 0., 0., 0., 0., 0., 0., 0., 0.],
                                                                            [0, 0.5, 0., 0., 0., 0., 0., 0., 0.],
                                                                            [0, 0., 0.5, 0., 0., 0., 0., 0., 0.],
                                                                            [0, 0., 0., 2.5, 0., 0., 0., 0., 0.],
                                                                            [0, 0, 0., 0., 2.5, 0., 0., 0., 0.],
                                                                            [0, 0., 0, 0., 0., 2.5, 0., 0., 0.],
                                                                            [0, 0., 0, 0., 0., 0, 5, 0., 0.],
                                                                            [0, 0., 0, 0., 0., 0, 0., 5, 0.],
                                                                            [0, 0., 0, 0., 0., 0, 0., 0, 5]]) ** 2)[
                                                                     None]))
                        # endregion
                        tracks['age'] = np.vstack((tracks['age'], [[1]]))
                        tracks['totalVisibleCount'] = np.vstack((tracks['totalVisibleCount'], [[1]]))
                        tracks['consecutiveInvisibleCount'] = np.vstack((tracks['consecutiveInvisibleCount'], [[0]]))
                        tracks['tracklets'] = np.vstack((tracks['tracklets'], pre_trackLet[k, :, :][None]))
                        tracks['activity'] = np.vstack((tracks['activity'], [['Waiting    ']]))
                        tracks['trajectory_status'] = np.vstack((tracks['trajectory_status'], [[0]]))
                elif numofTrackLet == 1:
                    curr_trackLet = trackLet
                    numofTrackLet += 1
                else:
                    pre_trackLet = tracks['tracklets']
                    curr_trackLet = trackLet
                # endregion

                if numofTrackLet == 2:
                    # region 6. Predict Tracks
                    for k in range(len(pre_trackLet)):
                        if tracks['trajectory_status'][k, 0] != 2:
                            EKF_S_apr, EKF_P_apr = Numba_Module.EKF_Predict_MOT(k,
                                                                                tracks['position'],
                                                                                tracks['velocity'],
                                                                                tracks['acceleration'],
                                                                                tracks['covariance_matrix'],
                                                                                Ts, mov_framelen_track)
                            tracks['position'][k, :] = 0.8 * EKF_S_apr[np.arange(0, 3), 0] + \
                                                       0.2 * tracks['position'][k, :]
                            tracks['velocity'][k, :] = 0.8 * EKF_S_apr[np.arange(3, 6), 0] + \
                                                       0.2 * tracks['velocity'][k, :]
                            tracks['acceleration'][k, :] = 0.8 * EKF_S_apr[np.arange(6, 9), 0] + \
                                                           0.2 * tracks['acceleration'][k, :]
                            tracks['covariance_matrix'][k, :, :] = 0.8 * EKF_P_apr + \
                                                                   0.2 * tracks['covariance_matrix'][k, :, :]

                            # region Update tracklets
                            vx = tracks['tracklets'][k, :, 4]
                            vy = tracks['tracklets'][k, :, 5]
                            vz = tracks['tracklets'][k, :, 6]
                            tracks['tracklets'][k, :, 1] = \
                                0.3 * tracks['tracklets'][k, :, 1] + \
                                0.7 * (tracks['tracklets'][k, :, 1] + vx * Ts * mov_framelen_track)
                            tracks['tracklets'][k, :, 2] = \
                                0.3 * tracks['tracklets'][k, :, 2] + \
                                0.7 * (tracks['tracklets'][k, :, 2] + vy * Ts * mov_framelen_track)
                            tracks['tracklets'][k, :, 3] = \
                                0.3 * tracks['tracklets'][k, :, 3] + \
                                0.7 * (tracks['tracklets'][k, :, 3] + vz * Ts * mov_framelen_track)
                            # endregion

                        tracks['age'][k, 0] += 1
                        tracks['consecutiveInvisibleCount'][k, 0] += 1
                    pre_trackLet = tracks['tracklets']
                    # endregion

                    # region 7. Compute Cost of Assigning every Detection to each Track
                    cost = Numba_Module.Compute_Cost(pre_trackLet, curr_trackLet, mov_framelen_track, Ts)
                    # endregion

                    # region 8. Assign Detections to Tracks (Priority Assignment Algorithm)
                    tracks_totalVisibleCount = np.array(tracks['totalVisibleCount'][:, 0])
                    assignments = []
                    while True:
                        if np.sum(tracks_totalVisibleCount) == 0:
                            break
                        # find the index of the maximum of tracks_totalVisibleCount
                        ind_maxtotalVisibleCount = \
                            np.where(tracks_totalVisibleCount == np.max(tracks_totalVisibleCount))[0]
                        tracks_totalVisibleCount[ind_maxtotalVisibleCount] = 0

                        if len(ind_maxtotalVisibleCount) == 1:
                            if np.min(cost[ind_maxtotalVisibleCount, :]) < dist_Gate_WA:
                                ind_minCost = np.argmin(cost[ind_maxtotalVisibleCount, :])
                                assignments.append((ind_maxtotalVisibleCount[0], ind_minCost))
                                cost[:, ind_minCost] = 100 + random.random()
                        else:
                            # find the cost corresponding to ind_maxAge
                            totalVisibleCount_cost = cost[ind_maxtotalVisibleCount, :]
                            Age_assigned_tracks, Age_assigned_detections = \
                                linear_sum_assignment(totalVisibleCount_cost)
                            # filter out matched assignments using group_thres
                            for t, d in zip(Age_assigned_tracks, Age_assigned_detections):
                                if cost[t, d] < dist_Gate_WA:
                                    assignments.append((ind_maxtotalVisibleCount[t], d))
                                    cost[:, d] = 100 + random.random()

                    assignments = np.array(assignments)
                    if len(assignments) == 0:
                        assigned_tracks = []
                        assigned_detections = []
                    else:
                        assigned_tracks = assignments[:, 0]
                        assigned_detections = assignments[:, 1]

                    # Finding unmatchedTracks
                    unassigned_tracks, unassigned_detections = [], []
                    for k in range(cost.shape[0]):
                        if k not in assigned_tracks:
                            unassigned_tracks.append(k)

                    # Finding unmatched_detections
                    for k in range(cost.shape[1]):
                        if k not in assigned_detections:
                            unassigned_detections.append(k)
                    # print(assignments)
                    # endregion

                    # region 9. Update Assigned Tracks with Detections
                    for i in range(assignments.shape[0]):
                        t, d = assignments[i, :]  # tracks, 0; detections, 1
                        EKF_S, EKF_P = Numba_Module.EKF_Update_MOT(d,
                                                                   tracks['position'][t, :],
                                                                   tracks['velocity'][t, :],
                                                                   tracks['acceleration'][t, :],
                                                                   tracks['covariance_matrix'][t, :, :],
                                                                   curr_trackLet,
                                                                   framelen_track)

                        tracks['position'][t, :] = 0.9 * EKF_S[np.arange(0, 3), 0] + 0.1 * tracks['position'][t, :]
                        tracks['velocity'][t, :] = 0.9 * EKF_S[np.arange(3, 6), 0] + 0.1 * tracks['velocity'][t, :]
                        tracks['acceleration'][t, :] = \
                            0.9 * EKF_S[np.arange(6, 9), 0] + 0.1 * tracks['acceleration'][t, :]
                        tracks['covariance_matrix'][t, :, :] = 0.9 * EKF_P + 0.1 * tracks['covariance_matrix'][t, :, :]

                        tracks['position_w'][t, :] = np.median(curr_trackLet[d, :, np.arange(10, 13)], axis=1)
                        tracks['doppler_velocity'][t, 0] = np.median(curr_trackLet[d, :, 13])

                        tracks['tracklets'][t, :, :] = curr_trackLet[d, :, :]
                        tracks['totalVisibleCount'][t, 0] = tracks['totalVisibleCount'][t, 0] + 1
                        tracks['consecutiveInvisibleCount'][t, 0] = 0
                    # endregion

                    # region 10. Create New Tracks
                    # unassigned_detections, create new tracks
                    unassigned_detections = np.array(unassigned_detections)
                    if len(unassigned_detections) > 0:
                        # copy_trackLet_NMS = np.empty((0, 15), dtype=float)
                        # for k in unassigned_detections:
                        #     copy_trackLet_NMS = np.vstack((copy_trackLet_NMS, np.median(curr_trackLet[k, :, :], axis=0)))
                        # res_Ind, _ = \
                        #     NMS_Clustering(copy_trackLet_NMS, copy_trackLet_NMS[:, 1], copy_trackLet_NMS[:, 2],
                        #                    copy_trackLet_NMS[:, 14], 0.8, IoU_thres)
                        # for k in unassigned_detections[res_Ind]:
                        for k in unassigned_detections:
                            res_IDs = sorted(list(set(np.arange(0, 200)) - set(tracks['tid'][:, 0])))
                            # ID
                            tracks['tid'] = np.vstack((tracks['tid'], [[res_IDs[0]]]))
                            # Position
                            tracks_pos = curr_trackLet[k, :, np.arange(1, 4)]
                            tracks_pos = np.median(tracks_pos, axis=1).reshape(1, -1)
                            # tracks_pos = curr_trackLet[k, 0, np.arange(1, 4)].reshape(1, -1)
                            tracks['position'] = np.vstack((tracks['position'], tracks_pos))
                            # Velocity
                            tracks_vel = curr_trackLet[k, :, np.arange(4, 7)]
                            tracks_vel = np.median(tracks_vel, axis=1).reshape(1, -1)
                            # tracks_vel = curr_trackLet[k, 0, np.arange(4, 7)].reshape(1, -1)
                            tracks['velocity'] = np.vstack((tracks['velocity'], tracks_vel))
                            # Acceleration
                            tracks_acc = curr_trackLet[k, :, np.arange(7, 10)]
                            tracks_acc = np.median(tracks_acc, axis=1).reshape(1, -1)
                            # tracks_acc = curr_trackLet[k, 0, np.arange(7, 10)].reshape(1, -1)
                            tracks['acceleration'] = np.vstack((tracks['acceleration'], tracks_acc))

                            # Position_W.o.
                            tracks_pos_w = curr_trackLet[k, :, np.arange(10, 13)]
                            tracks_pos_w = np.median(tracks_pos_w, axis=1).reshape(1, -1)
                            # tracks_pos_w = curr_trackLet[k, 0, np.arange(10, 13)].reshape(1, -1)
                            tracks['position_w'] = np.vstack((tracks['position_w'], tracks_pos_w))
                            # Doppler
                            tracks_doppler = np.median(curr_trackLet[k, :, 13])
                            # tracks_doppler = curr_trackLet[k, 0, 13]
                            tracks['doppler_velocity'] = np.vstack((tracks['doppler_velocity'],
                                                                    [[tracks_doppler]]))

                            # region tracks['covariance_matrix']
                            tracks['covariance_matrix'] = np.vstack((tracks['covariance_matrix'],
                                                                     (np.array([[0.5, 0., 0., 0., 0., 0., 0., 0., 0.],
                                                                                [0, 0.5, 0., 0., 0., 0., 0., 0., 0.],
                                                                                [0, 0., 0.5, 0., 0., 0., 0., 0., 0.],
                                                                                [0, 0., 0., 2.5, 0., 0., 0., 0., 0.],
                                                                                [0, 0, 0., 0., 2.5, 0., 0., 0., 0.],
                                                                                [0, 0., 0, 0., 0., 2.5, 0., 0., 0.],
                                                                                [0, 0., 0, 0., 0., 0, 5, 0., 0.],
                                                                                [0, 0., 0, 0., 0., 0, 0., 5, 0.],
                                                                                [0, 0., 0, 0., 0., 0, 0., 0, 5]]) ** 2)[
                                                                         None]))
                            # endregion
                            tracks['age'] = np.vstack((tracks['age'], [[1]]))
                            tracks['totalVisibleCount'] = np.vstack((tracks['totalVisibleCount'], [[1]]))
                            tracks['consecutiveInvisibleCount'] = np.vstack(
                                (tracks['consecutiveInvisibleCount'], [[0]]))
                            tracks['tracklets'] = np.vstack((tracks['tracklets'], curr_trackLet[k, :, :][None]))
                            tracks['activity'] = np.vstack((tracks['activity'], [['Waiting    ']]))
                            tracks['trajectory_status'] = np.vstack((tracks['trajectory_status'], [[0]]))
                    # endregion
            else:
                # region 11. Update Track Attributes
                if len(tracks['tid']) > 0:
                    tracks['age'] = tracks['age'] + 1
                    tracks['consecutiveInvisibleCount'] = tracks['consecutiveInvisibleCount'] + 1
                # endregion
                print("trackLet: No objects of interest exists ....")

            # region 12. Update Tracks
            arr_res = np.array([], dtype=int)
            k = 0
            while True:
                if k == len(tracks['tid']):
                    break

                if tracks['age'][k, 0] >= framelen_track * 2:
                    if (tracks['consecutiveInvisibleCount'][k, 0] <= 5) and \
                            ((tracks['totalVisibleCount'][k, 0] / tracks['age'][k, 0]) > 0.7):

                        arr_res = np.append(arr_res, k)
                        tracks['trajectory_status'][k, 0] = 1  # Confirmed

                        # region Activity
                        med_traj_height = np.median(tracks['tracklets'][k, :, 3])
                        if med_traj_height <= 0.35:  # empirical value
                            print('Confirmed - Activity: Fall Down! -', round(med_traj_height, 3), "m")
                            activity = 'Fall Down'
                        else:
                            print('Confirmed - Activity: Walk! -', round(med_traj_height, 3), "m")
                            activity = 'Walk'
                        tracks['activity'][k, 0] = activity
                        # endregion

                        # region Show Results
                        print("Confirmed - track ID: " + str(tracks['tid'][k, 0]))
                        print("Confirmed - XYZ Position: " + str(
                            np.around(tracks['position'][k, :], 3)) + ' m')
                        print("Confirmed - XYZ Velocity: " + str(
                            np.around(tracks['velocity'][k, :], 3)) + ' m/s')
                        print("Confirmed - XYZ Acceleration: " + str(
                            np.around(tracks['acceleration'][k, :], 3)) + ' m/s2')
                        print("Confirmed - totalVisibleCount: " + str(tracks['totalVisibleCount'][k, 0]))
                        print("Confirmed - consecutiveInvisibleCount: " +
                              str(tracks['consecutiveInvisibleCount'][k, 0]))
                        print("Confirmed - age: " + str(tracks['age'][k, 0]))
                        # endregion

                        k += 1
                    elif 5 < tracks['consecutiveInvisibleCount'][k, 0] <= 20:
                        # arr_res = np.append(arr_res, k)
                        tracks['totalVisibleCount'][k, 0] += 1
                        # tracks['age'][k, 0] += 1
                        # tracks['motion_status'][k, 0] = 0
                        tracks['trajectory_status'][k, 0] = 2  # Sleeping
                        k += 1
                    else:
                        print("Deleted - track ID: " + str(tracks['tid'][k, 0]))
                        for keys in tracks.keys():
                            tracks[keys] = np.delete(tracks[keys], k, axis=0)
                else:
                    if tracks['consecutiveInvisibleCount'][k, 0] >= 5:
                        print("Deleted - track ID: " + str(tracks['tid'][k, 0]))
                        for keys in tracks.keys():
                            tracks[keys] = np.delete(tracks[keys], k, axis=0)
                    else:
                        tracks['trajectory_status'][k, 0] = 0  # Waiting
                        print("Waiting - track ID: " + str(tracks['tid'][k, 0]))
                        k += 1
            # endregion

            # region 13. Update numofTrackLet
            if len(tracks['tid']) == 0:
                numofTrackLet = 0
            # endregion

            # region 14. Output Tracks
            if len(arr_res) > 0:
                # Filter some overlapped point clouds
                copy_tracks_tracklets = tracks['tracklets'][arr_res, :, :]
                KalmanFilt_pointObj = np.zeros((copy_tracks_tracklets.shape[0], copy_tracks_tracklets.shape[2]),
                                               dtype=float)
                for k in range(copy_tracks_tracklets.shape[0]):
                    KalmanFilt_pointObj[k, :] = np.median(copy_tracks_tracklets[k, :, :], axis=0)
                res_Ind, res_KalmanFilt_pointObj = \
                    NMS_Clustering(KalmanFilt_pointObj,
                                   KalmanFilt_pointObj[:, 1], KalmanFilt_pointObj[:, 2],
                                   KalmanFilt_pointObj[:, 14], 0.8, IoU_thres)
                arr_res = arr_res[res_Ind]

                Tracks['tid'] = tracks['tid'][arr_res, :]
                Tracks['position'] = tracks['position'][arr_res, :]
                Tracks['velocity'] = tracks['velocity'][arr_res, :]
                Tracks['acceleration'] = tracks['acceleration'][arr_res, :]
                Tracks['height_curve'] = tracks['tracklets'][arr_res, :, 3]
                Tracks['activity'] = tracks['activity'][arr_res, :]
            else:
                Tracks['tid'] = np.empty((0, 1), dtype=int)
                Tracks['position'] = np.empty((0, 3), dtype=float)
                Tracks['velocity'] = np.empty((0, 3), dtype=float)
                Tracks['acceleration'] = np.empty((0, 3), dtype=float)
                Tracks['height_curve'] = np.empty((0, framelen_track), dtype=float)
                Tracks['activity'] = np.empty((0, 1), dtype=np.string_)
            # endregion

            end_MOT = time.time()
            print('MOT Execution time:', round((end_MOT - start_MOT), 3), 's')
# endregion


# ------------------------------------------------------------------
# region Multiple Object Vital Sign Recognition
def Processing_VSR(syn_Heatmap, mmWave_Buffer, EnvirSensing, PCounting, Tracks, VitalSigns, VS_height_curve, ):
    # region Get the configuration parameters of Radar
    configParameters, rangeArray, thetaArray, dopplerArray = parseConfigFile(configFileName)
    gl_numRangeBins = configParameters["numRangeBins"]
    gl_numAngleBins = 180 // AoA_spacing + 1
    Fs = 1 / (configParameters["framePeriodicity"] * 0.001)  # Sampling Frequency
    Ts = 1 / Fs  # Sampling Time Interval
    # endregion

    # region Generate a steering vector for AOA estimation
    ang_est_range = 90
    ang_est_resolution = AoA_spacing
    num_ant = 4
    num_vec = (2 * ang_est_range / ang_est_resolution + 1)
    num_vec = int(round(num_vec))
    steering_vec = np.zeros((num_vec, num_ant), dtype=np.complex128)
    for kk in range(num_vec):
        for jj in range(num_ant):
            steering_vec[kk, jj] = \
                np.exp(1j * -1 * np.pi * jj * np.sin((-ang_est_range + kk * ang_est_resolution) * np.pi / 180))
    # endregion

    # region Initialize Vital Sign Parameters
    box_width = 0.8
    IoU_thres = 0.6
    dist_Gate_vsign = 0.5  # This small threshold is to remove the case that the person moves at a fixed position.
    # endregion

    # region Initialize VS tracks
    tracks_vsign = {}
    tracks_vsign['tid'] = np.empty((0, 1), dtype=int)
    tracks_vsign['position'] = np.empty((0, 2), dtype=float)
    tracks_vsign['height'] = np.empty((0, 1), dtype=float)
    tracks_vsign['velocity'] = np.empty((0, 2), dtype=float)
    tracks_vsign['acceleration'] = np.empty((0, 2), dtype=float)
    tracks_vsign['covariance_matrix'] = np.empty((0, 6, 6), dtype=float)
    tracks_vsign['age'] = np.empty((0, 1), dtype=int)
    tracks_vsign['totalVisibleCount'] = np.empty((0, 1), dtype=int)
    tracks_vsign['consecutiveInvisibleCount'] = np.empty((0, 1), dtype=int)
    tracks_vsign['tracklets'] = np.empty((0, 8), dtype=float)
    # endregion

    # region Initialize Buffers for Filtering
    filt_breathing = {}
    filt_heartbeat = {}
    filt_height_curve = {}
    # endregion

    flag_vsign = 0
    numofTrackLet_vsign = 0
    age_EnvirSen = 0
    CmplxforVitalSign = np.empty((0, gl_numRangeBins, gl_numVirtAnt), dtype=complex)
    while True:
        syn_Heatmap.acquire()

        # region Update Tracks_vsign and VitalSigns
        copy_tracks_position = copy.deepcopy(Tracks['position'])
        copy_tracks_position = copy_tracks_position[:, range(2)]
        k = 0
        while True:
            if k == len(tracks_vsign['position']):
                break
            cost = distance.cdist(tracks_vsign['position'][k, :].reshape(1, -1), copy_tracks_position)
            if (cost > box_width).all():
                k += 1
            else:
                for keys in tracks_vsign.keys():
                    tracks_vsign[keys] = np.delete(tracks_vsign[keys], k, axis=0)
        if len(tracks_vsign['tid']) == 0:
            numofTrackLet_vsign == 0

        key_VS = VitalSigns.keys()
        k = 0
        while True:
            if k == len(VitalSigns['position']):
                break
            cost = distance.cdist(VitalSigns['position'][k, :].reshape(1, -1), copy_tracks_position)
            if (cost > box_width).all():
                k += 1
            else:
                for keys in np.arange(0, len(key_VS) - 3):
                    VitalSigns[key_VS[keys]] = np.delete(VitalSigns[key_VS[keys]], k, axis=0)

        k = 0
        while True:
            if k == len(VitalSigns['position_waiting']):
                break
            cost = distance.cdist(VitalSigns['position_waiting'][k, :].reshape(1, -1), copy_tracks_position)
            if (cost > box_width).all():
                k += 1
            else:
                for keys in np.arange(len(key_VS) - 3, len(key_VS)):
                    VitalSigns[key_VS[keys]] = np.delete(VitalSigns[key_VS[keys]], k, axis=0)
        # endregion

        # region People Counting
        PCounting.value = int(len(Tracks['tid'])) + \
                          int(len(VitalSigns['tid'])) + \
                          int(len(VitalSigns['tid_waiting']))
        # endregion

        # region Environment Sensing
        if len(VitalSigns['tid_waiting']) > 0 or len(VitalSigns['tid']) > 0:
            # region 1. Environment Sensing 1,0 and 1,1
            if len(Tracks['tid']) == 0:  # mmWave_Buffer['NULL_POINT_CLOUD'] == 10:
                # Stationary persons - 1, Moving persons - 0
                EnvirSensing['Environment'] = 2
            else:
                # Stationary persons - 1, Moving persons - 1
                EnvirSensing['Environment'] = 3
            # endregion
        else:
            # region 2. Environment Sensing 0,0 and 0,1
            if len(Tracks['tid']) == 0:  # mmWave_Buffer['NULL_POINT_CLOUD'] == 10:
                # Stationary persons - 0, Moving persons - 0
                EnvirSensing['Environment'] = 0  # Empty Room
            else:
                # Stationary persons - 0, Moving persons - 1
                EnvirSensing['Environment'] = 1
            # endregion
        # endregion

        # ----------------------------------------------------------
        # ---------Multiple Object Vital Sign Recognition-----------
        if mmWave_Buffer['GetData_Cmplx'].shape[0] < framelen_vsign:
            if flag_vsign == 0:
                #print('---------- (2) Vital Sign Recognition ----------')
                #print('Number of frames (Vital Sign):', mmWave_Buffer['GetData_Cmplx'].shape[0], '#')
                pass
        else:
            start_vsign = time.time()
            flag_vsign = 1
            age_EnvirSen += 1
            #print('---------- (2) Vital Sign Recognition ----------')
            #print('age_EnvirSen (Vital Sign):', int(age_EnvirSen), '#')

            # region 1. Extract Point Clouds based on MVDR and CFAR
            Foreground_vsign = mmWave_Buffer['GetData_Cmplx'][range(framelen_vsign - 5), :, :] - \
                               np.mean(mmWave_Buffer['GetData_Cmplx'][range(framelen_vsign - 5), :, :], axis=0)

            # region MVDR Algorithm
            # rgAziheatmap: (gl_numAngleBins, gl_numRangeBins)
            rgAziheatmap, weights = \
                Numba_Module.aoa_capon(Foreground_vsign, steering_vec, gl_numAngleBins, gl_numRangeBins)
            # endregion

            # region CFAR for Object Detection
            guard_len = 4
            train_len = 8
            rate_fa = 0.05
            azimuth_Ind, range_Ind, snrs = Numba_Module.CFAR(rgAziheatmap, guard_len, train_len, rate_fa)

            # # region Perform cfar in Angle direction    # (gl_numRangeBins, numAngleBins)
            # first_pass, _ = np.apply_along_axis(func1d=dsp.ca_,
            #                                     axis=0,
            #                                     arr=rgAziheatmap,
            #                                     l_bound=2,
            #                                     guard_len=4,
            #                                     noise_len=8)
            # # endregion
            #
            # # region Perform cfar in range direction    # (gl_numRangeBins, numAngleBins)
            # second_pass, noise_floor = np.apply_along_axis(func1d=dsp.ca_,
            #                                                axis=0,
            #                                                arr=rgAziheatmap.T,
            #                                                l_bound=2.5,
            #                                                guard_len=4,
            #                                                noise_len=8)
            # # endregion
            #
            # # Estimate peaks based on cfar     # (numAngleBins, gl_numRangeBins)
            # first_pass = (rgAziheatmap > first_pass)  # True or False in the corresponding position
            # second_pass = (rgAziheatmap > second_pass.T)  # True or False in the corresponding position
            # peaks = (first_pass & second_pass)
            # pairs = np.argwhere(peaks)
            # azimuth_Ind, range_Ind = pairs.T
            # endregion
            # endregion

            if len(range_Ind) > 0:
                # region 2. Convert bins to units
                mm_range = rangeArray[range_Ind]
                mm_azimuth = thetaArray[azimuth_Ind] * (np.pi / 180)
                mm_snr = 20 * np.log10(snrs + 1)
                mm_Pos_x = mm_range * np.sin(mm_azimuth)
                mm_Pos_y = mm_range * np.cos(mm_azimuth)
                mm_pointObj = np.vstack((mm_Pos_x, mm_Pos_y, mm_snr)).T  # N * 3

                # noise_floor = noise_floor.T
                # snr = rgAziheatmap[azimuth_Ind, range_Ind] - noise_floor[azimuth_Ind, range_Ind]
                # # Convert bins to units
                # mm_range = rangeArray[range_Ind]
                # mm_azimuth = thetaArray[azimuth_Ind] * (np.pi / 180)
                # mm_snr = snr
                # mm_Pos_x = mm_range * np.sin(mm_azimuth)
                # mm_Pos_y = mm_range * np.cos(mm_azimuth)
                # mm_pointObj = np.vstack((mm_Pos_x, mm_Pos_y, mm_snr)).T  # N * 3
                # endregion

                # region 3. Extract the points of interest using NMS Algorithm
                # copy_mm_pointObj = mm_pointObj
                # clustering = DBSCAN(eps=box_width, min_samples=5).fit(copy_mm_pointObj[:, range(2)])
                # copy_mm_pointObj = copy_mm_pointObj[np.where(clustering.labels_ != -1)[0], :]
                # if len(copy_mm_pointObj) != 0:
                #     mm_pointObj = copy_mm_pointObj
                # print(mm_pointObj)
                # print(mm_pointObj.shape)
                res_mm_Ind, res_mm_pointObj = NMS_Clustering(mm_pointObj, mm_pointObj[:, 0],
                                                             mm_pointObj[:, 1], mm_pointObj[:, 2], box_width, IoU_thres)
                # endregion

                # region 4. Weighted Averaging
                ave_res_pointObj = np.empty((0, 3), dtype=float)
                for k in range(res_mm_pointObj.shape[0]):
                    if np.abs(res_mm_pointObj[k, 0]) >= sensing_dist or res_mm_pointObj[k, 1] >= sensing_dist:
                        continue
                    dist_Diff = euclidean_distances(np.reshape(res_mm_pointObj[k, range(2)], (1, -1)),
                                                    mm_pointObj[:, range(2)])
                    pcloud_Ind = np.where(dist_Diff[0, :] <= box_width / 2)[0]
                    pcloud_snr = mm_pointObj[pcloud_Ind, 2]
                    copy_pointObj = \
                        np.array([round(np.sum(pcloud_snr * mm_pointObj[pcloud_Ind, 0]) / np.sum(pcloud_snr), 3),
                                  round(np.sum(pcloud_snr * mm_pointObj[pcloud_Ind, 1]) / np.sum(pcloud_snr), 3),
                                  np.mean(pcloud_snr)])
                    ave_res_pointObj = np.vstack((ave_res_pointObj, copy_pointObj))
                # endregion
                # print(ave_res_pointObj)

                if len(ave_res_pointObj) > 0:
                    # region 5. Save trackLets of two consecutive frames for association
                    # Initialize trackLet
                    # 0. track_Id,
                    # 1. Pos_X, 2. Pos_Y,   # Position
                    # 3. Vx, 4. Vy,         # Velocity
                    # 5. ax, 6. ay          # Acceleration
                    # 7. SNR
                    trackLet_vsign = np.empty((0, 8), dtype=float)
                    for k_trackLet_vsign in range(np.shape(ave_res_pointObj)[0]):
                        trackLet_Point_vsign = np.array([[k_trackLet_vsign,  # track_Id
                                                          ave_res_pointObj[k_trackLet_vsign, 0],  # Position_x
                                                          ave_res_pointObj[k_trackLet_vsign, 1],  # Position_y
                                                          0.01, 0.01,  # Velocity
                                                          0.01, 0.01,  # Acceleration
                                                          ave_res_pointObj[k_trackLet_vsign, 2]]])  # SNR
                        trackLet_vsign = np.vstack((trackLet_vsign, trackLet_Point_vsign))
                    if numofTrackLet_vsign == 0:
                        # Create tracks
                        pre_trackLet_vsign = trackLet_vsign
                        numofTrackLet_vsign += 1

                        for k in range(np.shape(pre_trackLet_vsign)[0]):
                            res_IDs = sorted(list(set(np.arange(0, 200)) - set(tracks_vsign['tid'][:, 0])))
                            tracks_vsign['tid'] = np.vstack((tracks_vsign['tid'], [[res_IDs[0]]]))
                            tracks_vsign['position'] = np.vstack((tracks_vsign['position'],
                                                                  [pre_trackLet_vsign[k, [1, 2]]]))
                            tracks_vsign['height'] = np.vstack((tracks_vsign['height'], [[radar_Height]]))
                            tracks_vsign['velocity'] = np.vstack((tracks_vsign['velocity'],
                                                                  [pre_trackLet_vsign[k, [3, 4]]]))
                            tracks_vsign['acceleration'] = np.vstack((tracks_vsign['acceleration'],
                                                                      [pre_trackLet_vsign[k, [5, 6]]]))
                            tracks_vsign['covariance_matrix'] = np.vstack(
                                (tracks_vsign['covariance_matrix'], (np.array([[0.5, 0., 0., 0., 0., 0.],
                                                                               [0, 0.5, 0., 0., 0., 0.],
                                                                               [0, 0., 0.5, 0., 0., 0.],
                                                                               [0, 0, 0., 0.5, 0., 0.],
                                                                               [0, 0., 0., 0., 0.5, 0.],
                                                                               [0, 0., 0., 0., 0., 0.5]]) ** 2)[None]))
                            tracks_vsign['age'] = np.append(tracks_vsign['age'], [[1]], axis=0)
                            tracks_vsign['totalVisibleCount'] = \
                                np.vstack((tracks_vsign['totalVisibleCount'], [[1]]))
                            tracks_vsign['consecutiveInvisibleCount'] = \
                                np.vstack((tracks_vsign['consecutiveInvisibleCount'], [[0]]))
                            tracks_vsign['tracklets'] = np.vstack((tracks_vsign['tracklets'], pre_trackLet_vsign[k, :]))
                    elif numofTrackLet_vsign == 1:
                        curr_trackLet_vsign = trackLet_vsign
                        numofTrackLet_vsign += 1
                    else:
                        pre_trackLet_vsign = tracks_vsign['tracklets']
                        curr_trackLet_vsign = trackLet_vsign
                    # endregion

                    if numofTrackLet_vsign == 2:
                        # region 6. Update Unassigned Tracks
                        for t in range(len(tracks_vsign['tid'])):
                            EKF_S_apr, EKF_P_apr = \
                                Numba_Module.EKF_Predict_VS(tracks_vsign['position'][t, :],
                                                            tracks_vsign['velocity'][t, :],
                                                            tracks_vsign['acceleration'][t, :],
                                                            tracks_vsign['covariance_matrix'][t, :, :], Ts)
                            tracks_vsign['position'][t, :] = \
                                0.7 * tracks_vsign['position'][t, :] + 0.3 * EKF_S_apr[np.arange(0, 2), 0]
                            tracks_vsign['velocity'][t, :] = \
                                0.7 * tracks_vsign['velocity'][t, :] + 0.3 * EKF_S_apr[np.arange(2, 4), 0]
                            tracks_vsign['acceleration'][t, :] = \
                                0.7 * tracks_vsign['acceleration'][t, :] + 0.3 * EKF_S_apr[np.arange(4, 6), 0]
                            tracks_vsign['covariance_matrix'][t, :, :] = \
                                0.7 * tracks_vsign['covariance_matrix'][t, :, :] + 0.3 * EKF_P_apr

                            tracks_vsign['tracklets'][t, 1] = tracks_vsign['position'][t, 0]
                            tracks_vsign['tracklets'][t, 2] = tracks_vsign['position'][t, 1]
                            tracks_vsign['tracklets'][t, 3] = tracks_vsign['velocity'][t, 0]
                            tracks_vsign['tracklets'][t, 4] = tracks_vsign['velocity'][t, 1]
                            tracks_vsign['tracklets'][t, 5] = tracks_vsign['acceleration'][t, 0]
                            tracks_vsign['tracklets'][t, 6] = tracks_vsign['acceleration'][t, 1]

                            tracks_vsign['age'][t, 0] = tracks_vsign['age'][t, 0] + 1
                            tracks_vsign['consecutiveInvisibleCount'][t, 0] = \
                                tracks_vsign['consecutiveInvisibleCount'][t, 0] + 1
                        pre_trackLet_vsign = tracks_vsign['tracklets']
                        # endregion

                        # region 7. Compute Cost of Assigning every Detection to each Track
                        cost = Numba_Module.Compute_Cost_VS(pre_trackLet_vsign, curr_trackLet_vsign)
                        # endregion

                        # region 8. Assign Detections to Tracks (Priority Assignment Algorithm)
                        # 0. assigned_tracks, unassigned_tracks
                        # 1. assigned_detections, unassigned_detections
                        tracks_vsign_totalVisibleCount = np.array(tracks_vsign['totalVisibleCount'][:, 0])
                        assignments = []
                        while True:
                            if np.sum(tracks_vsign_totalVisibleCount) == 0:
                                break
                            ind_maxtotalVisibleCount = \
                                np.where(tracks_vsign_totalVisibleCount == np.max(tracks_vsign_totalVisibleCount))[0]
                            # print('tracks_vsign_totalVisibleCount', tracks_vsign_totalVisibleCount)
                            tracks_vsign_totalVisibleCount[ind_maxtotalVisibleCount] = 0

                            if len(ind_maxtotalVisibleCount) == 1:
                                if np.min(cost[ind_maxtotalVisibleCount, :]) < dist_Gate_vsign:
                                    ind_minCost = np.argmin(cost[ind_maxtotalVisibleCount, :])
                                    assignments.append((ind_maxtotalVisibleCount[0], ind_minCost))
                                    cost[:, ind_minCost] = 100 + random.random()
                            else:
                                # find the cost corresponding to ind_maxAge
                                totalVisibleCount_cost = cost[ind_maxtotalVisibleCount, :]
                                Age_assigned_tracks, Age_assigned_detections = \
                                    linear_sum_assignment(totalVisibleCount_cost)
                                # filter out matched assignments using group_thres
                                for t, d in zip(Age_assigned_tracks, Age_assigned_detections):
                                    tracks_vsign_totalVisibleCount[ind_maxtotalVisibleCount[t]] = 0
                                    if cost[t, d] < dist_Gate_vsign:
                                        assignments.append((ind_maxtotalVisibleCount[t], d))
                                        cost[:, d] = 100 + random.random()

                        assignments = np.array(assignments)
                        if len(assignments) == 0:
                            assigned_tracks = []
                            assigned_detections = []
                        else:
                            assigned_tracks = assignments[:, 0]
                            assigned_detections = assignments[:, 1]

                        # Finding unmatchedTracks
                        unassigned_tracks, unassigned_detections = [], []
                        for k in range(cost.shape[0]):
                            if k not in assigned_tracks:
                                unassigned_tracks.append(k)

                        # Finding unmatched_detections
                        for k in range(cost.shape[1]):
                            if k not in assigned_detections:
                                unassigned_detections.append(k)
                        # endregion

                        # region 9. Update Assigned Tracks with Detections
                        for k in range(assignments.shape[0]):
                            t, d = assignments[k, :]

                            EKF_S, EKF_P = Numba_Module.EKF_Update_VS(tracks_vsign['position'][t, :],
                                                                      tracks_vsign['velocity'][t, :],
                                                                      tracks_vsign['acceleration'][t, :],
                                                                      tracks_vsign['covariance_matrix'][t, :, :],
                                                                      curr_trackLet_vsign[d, :])
                            # print(EKF_S.T)

                            # Update tracks_vsign
                            tracks_vsign['position'][t, :] = \
                                0.5 * EKF_S[np.arange(0, 2), 0] + 0.5 * tracks_vsign['position'][t, :]
                            tracks_vsign['velocity'][t, :] = \
                                0.5 * EKF_S[np.arange(2, 4), 0] + 0.5 * tracks_vsign['velocity'][t, :]
                            tracks_vsign['acceleration'][t, :] = \
                                0.5 * EKF_S[np.arange(4, 6), 0] + 0.5 * tracks_vsign['acceleration'][t, :]
                            tracks_vsign['covariance_matrix'][t, :, :] = \
                                0.5 * EKF_P + 0.5 * tracks_vsign['covariance_matrix'][t, :, :]

                            tracks_vsign['totalVisibleCount'][t, 0] = tracks_vsign['totalVisibleCount'][t, 0] + 1
                            tracks_vsign['consecutiveInvisibleCount'][t, 0] = 0

                            tracks_vsign['tracklets'][t, :] = curr_trackLet_vsign[d, :]
                        # endregion

                        # region 10. Create New Tracks
                        # unassigned_detections, create new tracks
                        if len(unassigned_detections) > 0:
                            unassigned_detections = np.array(unassigned_detections)
                            for d in unassigned_detections:
                                res_IDs = sorted(list(set(np.arange(0, 200)) - set(tracks_vsign['tid'][:, 0])))
                                tracks_vsign['tid'] = np.vstack((tracks_vsign['tid'], [[res_IDs[0]]]))
                                tracks_vsign['position'] = np.vstack((tracks_vsign['position'],
                                                                      [curr_trackLet_vsign[d, np.arange(1, 3)]]))
                                tracks_vsign['height'] = np.vstack((tracks_vsign['height'], [[radar_Height]]))
                                tracks_vsign['velocity'] = np.vstack((tracks_vsign['velocity'],
                                                                      [curr_trackLet_vsign[d, np.arange(3, 5)]]))
                                tracks_vsign['acceleration'] = np.vstack((tracks_vsign['acceleration'],
                                                                          [curr_trackLet_vsign[d, np.arange(5, 7)]]))
                                tracks_vsign['covariance_matrix'] = np.vstack(
                                    (tracks_vsign['covariance_matrix'], (np.array([[0.5, 0., 0., 0., 0., 0.],
                                                                                   [0, 0.5, 0., 0., 0., 0.],
                                                                                   [0, 0., 0.5, 0., 0., 0.],
                                                                                   [0, 0, 0., 0.5, 0., 0.],
                                                                                   [0, 0., 0., 0., 1, 0.],
                                                                                   [0, 0., 0., 0., 0., 1]]) ** 2)[
                                        None]))
                                tracks_vsign['age'] = np.vstack((tracks_vsign['age'], [[1]]))
                                tracks_vsign['totalVisibleCount'] = np.vstack(
                                    (tracks_vsign['totalVisibleCount'], [[1]]))
                                tracks_vsign['consecutiveInvisibleCount'] = \
                                    np.vstack((tracks_vsign['consecutiveInvisibleCount'], [[0]]))
                                tracks_vsign['tracklets'] = \
                                    np.vstack((tracks_vsign['tracklets'], curr_trackLet_vsign[d, :]))
                        # endregion
                else:
                    # region 11. Update tracks_vsign['age'] and tracks_vsign['consecutiveInvisibleCount']
                    if len(tracks_vsign['tid']) > 0:
                        tracks_vsign['age'] = tracks_vsign['age'] + 1
                        tracks_vsign['consecutiveInvisibleCount'] = tracks_vsign['consecutiveInvisibleCount'] + 1
                    # endregion
            else:
                # region 12. Update tracks_vsign['age'] and tracks_vsign['consecutiveInvisibleCount']
                if len(tracks_vsign['tid']) > 0:
                    tracks_vsign['age'] = tracks_vsign['age'] + 1
                    tracks_vsign['consecutiveInvisibleCount'] = tracks_vsign['consecutiveInvisibleCount'] + 1
                # endregion

            # region 13. Update tracks_vsign
            # The deleteLostTracks function deletes tracks that have been invisible for too many consecutive frames.
            # It also deletes recently created tracks that have been invisible for too many frames overall.
            k = 0
            idx_waiting = np.array([], dtype=int)
            idx_confirmed = np.array([], dtype=int)
            while True:
                if k == len(tracks_vsign['tid']):
                    break

                if tracks_vsign['age'][k, 0] < window_vsign / 2:
                    if (tracks_vsign['consecutiveInvisibleCount'][k, 0] > 3) or \
                            ((tracks_vsign['totalVisibleCount'][k, 0] / tracks_vsign['age'][k, 0]) < 0.8):
                        #print("1-Deleted - track_vs ID: " + str(tracks_vsign['tid'][k, 0]))
                        for keys in tracks_vsign.keys():
                            tracks_vsign[keys] = np.delete(tracks_vsign[keys], k, axis=0)
                    else:
                        #print("1-Waiting - track_vs ID: " + str(tracks_vsign['tid'][k, 0]))
                        k += 1
                elif window_vsign / 2 <= tracks_vsign['age'][k, 0] <= window_vsign:
                    if tracks_vsign['consecutiveInvisibleCount'][k, 0] > 3 or \
                            tracks_vsign['totalVisibleCount'][k, 0] / tracks_vsign['age'][k, 0] < 0.8:
                        #print("2-Deleted - track_vs ID: " + str(tracks_vsign['tid'][k, 0]))
                        for keys in tracks_vsign.keys():
                            tracks_vsign[keys] = np.delete(tracks_vsign[keys], k, axis=0)
                    else:
                        idx_waiting = np.append(idx_waiting, k)
                        #print("2-Waiting - track_vs ID: " + str(tracks_vsign['tid'][k, 0]))
                        k += 1
                else:
                    if tracks_vsign['consecutiveInvisibleCount'][k, 0] > 3 or \
                            tracks_vsign['totalVisibleCount'][k, 0] / tracks_vsign['age'][k, 0] < 0.8:
                        #print("3-Deleted - track_vs ID: " + str(tracks_vsign['tid'][k, 0]))
                        for keys in tracks_vsign.keys():
                            tracks_vsign[keys] = np.delete(tracks_vsign[keys], k, axis=0)
                    else:
                        idx_confirmed = np.append(idx_confirmed, k)
                        #print("3-Confirmed - track_vs ID: " + str(tracks_vsign['tid'][k, 0]))
                        #print("3-Confirmed - track_vs Position: " +
                        #      str(np.around(tracks_vsign['position'][k, :], 3)) + ' m')
                        k += 1
            # endregion

            # region 14. Update numofTrackLet_vsign
            if len(tracks_vsign['tid']) == 0:
                numofTrackLet_vsign == 0
            # endregion

            # region 15. Update CmplxforVitalSign
            CmplxforVitalSign = \
                np.vstack((CmplxforVitalSign, mmWave_Buffer['GetData_Cmplx'][range(mov_framelen_vsign), :, :]))
            # endregion

            # region 16. Update GetData_Cmplx
            if len(mmWave_Buffer['GetData_Cmplx']) >= mov_framelen_vsign:
                mmWave_Buffer['GetData_Cmplx'] = \
                    np.delete(mmWave_Buffer['GetData_Cmplx'], range(mov_framelen_vsign), axis=0)
            #print('                          (2) Length of GetData_Cmplx:', np.shape(mmWave_Buffer['GetData_Cmplx'])[0])
            # endregion

            # region 17. Update VitalSigns_Waiting
            if len(idx_waiting) > 0:
                wait_tid = np.empty((0, 1), dtype=int)
                wait_position = np.empty((0, 2), dtype=float)
                wait_height = np.empty((0, 1), dtype=float)
                for k in idx_waiting:
                    # region Height and Coordinate Transformation
                    _, Height, Pos_x, Pos_y = Numba_Module.Height_VS(k,
                                                                     tracks_vsign['position'],
                                                                     Foreground_vsign,
                                                                     steering_vec,
                                                                     rangeArray, thetaArray, radar_Height)
                    if Height > 2:
                        continue

                    wait_tid = np.vstack((wait_tid, tracks_vsign['tid'][k, :]))
                    wait_height = np.vstack((wait_height, [[Height]]))
                    wait_position = np.vstack((wait_position, [[Pos_x, Pos_y]]))
                    # endregion

                VitalSigns['tid_waiting'] = wait_tid
                VitalSigns['position_waiting'] = wait_position
                VitalSigns['height_waiting'] = wait_height
            else:
                VitalSigns['tid_waiting'] = np.empty((0, 1), dtype=int)
                VitalSigns['position_waiting'] = np.empty((0, 2), dtype=float)
                VitalSigns['height_waiting'] = np.empty((0, 1), dtype=float)
            # endregion

            # region 18. Update VitalSigns_Confirmed and Vital Signs
            if len(idx_confirmed) > 0:
                # region 18.1. Add and Delete data in the buffers filt_breathing and filt_heartbeat
                for k in np.array(list(set(filt_breathing.keys()) - set(tracks_vsign['tid'][idx_confirmed, 0]))):
                    del filt_breathing[k]
                    del filt_heartbeat[k]
                    del filt_height_curve[k]

                for k in np.array(list(set(tracks_vsign['tid'][idx_confirmed, 0]) - set(filt_breathing.keys()))):
                    filt_breathing[k] = np.array([])
                    filt_heartbeat[k] = np.array([])
                    filt_height_curve[k] = np.array([])
                # endregion

                # region 18.2 Initialize
                copy_vitalsigns = {}
                copy_vitalsigns['tid'] = np.empty((0, 1), dtype=int)
                copy_vitalsigns['position'] = np.empty((0, 2), dtype=float)
                copy_vitalsigns['height'] = np.empty((0, 1), dtype=float)
                copy_vitalsigns['breathing_times'] = np.empty((0, 1), dtype=float)
                copy_vitalsigns['heartbeat_times'] = np.empty((0, 1), dtype=float)
                copy_vitalsigns['breathing_wave'] = np.empty((0, Len_cmplx - 1), dtype=float)
                copy_vitalsigns['heartbeat_wave'] = np.empty((0, Len_cmplx - 1), dtype=float)
                copy_vitalsigns['event'] = np.empty((0, 1), dtype=np.string_)
                # endregion
                for k in idx_confirmed:
                    # region 18.3 Tid
                    k_tid = tracks_vsign['tid'][k, 0]
                    # copy_vitalsigns['tid'] = np.vstack((copy_vitalsigns['tid'], [[k_tid]]))
                    # print('--------------------')
                    # print('Confirmed Static Person - ID:', k_tid)
                    # endregion

                    # region 18.4. Height and Position of Static Person
                    ind_bin, Height, Pos_x, Pos_y = Numba_Module.Height_VS(k,
                                                                           tracks_vsign['position'],
                                                                           Foreground_vsign,
                                                                           steering_vec,
                                                                           rangeArray, thetaArray, radar_Height)

                    if Height > 2:
                        continue

                    # Height
                    filt_height_curve[k_tid] = np.append(filt_height_curve[k_tid], Height)
                    if len(filt_height_curve[k_tid]) >= 13:
                        filt_height_curve[k_tid, -1] = np.median(
                            sci_signal.medfilt(filt_height_curve[k_tid], kernel_size=3))
                        filt_height_curve[k_tid] = np.delete(filt_height_curve[k_tid], 0)

                    Pos_x = tracks_vsign['position'][k, 0]
                    Pos_y = tracks_vsign['position'][k, 1]
                    copy_vitalsigns['position'] = np.vstack((copy_vitalsigns['position'], [[Pos_x, Pos_y]]))
                    copy_vitalsigns['height'] = np.vstack((copy_vitalsigns['height'], [[Height]]))
                    print('Confirmed Static Person - Position:', (round(Pos_x, 3), round(Pos_y, 3)), 'm')
                    print('Confirmed Static Person - Height:', round(Height, 3), 'm')
                    # endregion

                    # region 18.5. Fall Event Detection
                    if Height <= 0.35:  # empirical value
                        event = 'Fall Down'
                    else:
                        event = 'Normal'
                    copy_vitalsigns['event'] = np.vstack((copy_vitalsigns['event'], [[event]]))
                    print('Confirmed Static Person - Event:', event)
                    # endregion

                    # region 18.6. Phase Difference of Static Object
                    cmplx_Foreground_all = CmplxforVitalSign[range(Len_cmplx), :, :] - \
                                           np.mean(CmplxforVitalSign[range(Len_cmplx), :, :], axis=0)
                    phase_diff = Numba_Module.PhaseDiff_VS(ind_bin,
                                                           cmplx_Foreground_all,
                                                           gl_numVirtAnt,
                                                           gl_numRangeBins,
                                                           Len_cmplx)
                    # endregion

                    # region 18.7. Breathing Recognition
                    # region Lowpass Filter
                    b, a = sci_signal.butter(5, 0.5, btype='lowpass', fs=Fs)
                    filt_phase_diff_brt = sci_signal.filtfilt(b, a, phase_diff, axis=1)
                    # endregion

                    # region Breathing Frequency Estimation
                    filt_phase_diff_brt = np.sum(filt_phase_diff_brt, axis=0) / gl_numVirtAnt
                    filt_phase_diff_brt = filt_phase_diff_brt - np.mean(filt_phase_diff_brt)
                    # Signal Subspace

                    
                    # breathing_freq = np.abs(root_MUSIC(filt_phase_diff_brt, 1, None, Fs))[0]
                    # FFT
                    LL = 1024
                    breathing_yf = np.fft.fft(filt_phase_diff_brt, LL)
                    breathing_xf = np.fft.fftfreq(LL, 1 / Fs)[:LL // 2]
                    breathing_norm_yf = 2.0 / LL * np.abs(breathing_yf[0:LL // 2])
                    breathing_freq = breathing_xf[np.argmax(breathing_norm_yf)]
                    # endregion

                    # region Output for Plotting
                    filt_phase_diff_brt_plt = sci_signal.medfilt(filt_phase_diff_brt, kernel_size=7)
                    filt_phase_diff_brt_plt = sci_signal.savgol_filter(filt_phase_diff_brt_plt,
                                                                       window_length=15, polyorder=2, axis=0)
                    filt_phase_diff_brt_plt = filt_phase_diff_brt_plt.reshape(1, -1)
                    copy_vitalsigns['breathing_wave'] = np.vstack((copy_vitalsigns['breathing_wave'],
                                                                   filt_phase_diff_brt_plt * 2))  # 12-20
                    # endregion

                    # region Build a buffer to filter breathing
                    filt_breathing[k_tid] = np.append(filt_breathing[k_tid], breathing_freq)
                    if len(filt_breathing[k_tid]) < 9:
                        copy_vitalsigns['breathing_times'] = \
                            np.vstack((copy_vitalsigns['breathing_times'], [[breathing_freq * 60]]))
                        print('Confirmed Static Person - Breathing Rate:', round(breathing_freq * 60), 'times')
                    else:
                        copy_vitalsigns['breathing_times'] = \
                            np.vstack((copy_vitalsigns['breathing_times'], [[np.median(filt_breathing[k_tid]) * 60]]))
                        filt_breathing[k_tid] = np.delete(filt_breathing[k_tid], 0)
                        print('Confirmed Static Person - Breathing Rate:',
                              round(np.median(filt_breathing[k_tid]) * 60), 'times')
                    # endregion
                    # endregion

                    # region 18.8. Heartbeat Recognition
                    # region Minimize the impact of noise
                    # phase_diff_smoothed = Numba_Module.Noise_Suppression(phase_diff, gl_numVirtAnt, Len_cmplx, Ts)
                    phase_diff_smoothed = phase_diff
                    # endregion

                    # region Bandpass Filter
                    # b, a = sci_signal.butter(5, 3.5, btype='lowpass', fs=Fs)
                    # filt_phase_diff_hrt = sci_signal.filtfilt(b, a, phase_diff_smoothed, axis=1)
                    b, a = sci_signal.butter(5, [0.8, 2], btype='bandpass', fs=Fs)
                    filt_phase_diff_hrt = sci_signal.filtfilt(b, a, phase_diff_smoothed, axis=1)
                    # endregion

                    # region Heartbeat Frequency Estimation
                    filt_phase_diff_hrt = np.sum(filt_phase_diff_hrt, axis=0) / gl_numVirtAnt
                    filt_phase_diff_hrt = filt_phase_diff_hrt - np.mean(filt_phase_diff_hrt)

                    # Signal Subspace
                    # heartbeat_freq = np.abs(root_MUSIC(filt_phase_diff_hrt, 1, None, Fs))[0]

                    # FFT
                    copy_heartbeat_freq = []
                    for ind_kk, kk in enumerate(np.arange(0, Len_cmplx - (window_vsign - 10) * mov_framelen_vsign + 10, 10)):
                        copy_filt_phase_diff_hrt = filt_phase_diff_hrt[kk:kk + (window_vsign - 10) * mov_framelen_vsign]
                        LL = 1024
                        heartbeat_yf = np.fft.fft(copy_filt_phase_diff_hrt, LL)
                        heartbeat_xf = np.fft.fftfreq(LL, 1 / Fs)[:LL // 2]
                        heartbeat_norm_yf = 2.0 / LL * np.abs(heartbeat_yf[0:LL // 2])
                        copy_heartbeat_freq.append(heartbeat_xf[np.argmax(heartbeat_norm_yf)])
                    copy_heartbeat_freq = np.array(copy_heartbeat_freq)
                    heartbeat_freq = np.median(copy_heartbeat_freq)
                    # print('Test', copy_heartbeat_freq * 60)
                    # endregion

                    # region Output for Plotting
                    filt_phase_diff_hrt_plt = filt_phase_diff_hrt.reshape(1, -1)
                    copy_vitalsigns['heartbeat_wave'] = \
                        np.vstack((copy_vitalsigns['heartbeat_wave'], filt_phase_diff_hrt_plt))
                    # endregion

                    # region Build a buffer to filter heartbeat
                    filt_heartbeat[k_tid] = np.append(filt_heartbeat[k_tid], heartbeat_freq)
                    if len(filt_heartbeat[k_tid]) < 9:
                        copy_vitalsigns['heartbeat_times'] = \
                            np.vstack((copy_vitalsigns['heartbeat_times'], [[heartbeat_freq * 60]]))
                        print('Confirmed Static Person - heartbeat Rate:', round(heartbeat_freq * 60), 'times')
                    else:
                        copy_vitalsigns['heartbeat_times'] = np.vstack((copy_vitalsigns['heartbeat_times'],
                                                                        [[np.median(filt_heartbeat[k_tid]) * 60]]))
                        filt_heartbeat[k_tid] = np.delete(filt_heartbeat[k_tid], 0)
                        print('Confirmed Static Person - heartbeat Rate:',
                              round(np.median(filt_heartbeat[k_tid]) * 60), 'times')
                    # endregion

                    # endregion

                    # region 18.9. Update tid
                    copy_Tracks = copy.deepcopy(Tracks)
                    if len(copy_Tracks['position']) > 0:
                        dist_diff = \
                            euclidean_distances(np.array([[Pos_x, Pos_y]]),
                                                copy_Tracks['position'][:, range(2)])
                        copy_vitalsigns['tid'] = np.vstack((copy_vitalsigns['tid'],
                                                            [[int(copy_Tracks['tid'][np.argmin(dist_diff), 0])]]))
                        print('Confirmed Static Person - tid:', tracks_vsign['tid'][k, 0])
                        VS_height_curve[int(copy_Tracks['tid'][np.argmin(dist_diff), 0])] = filt_height_curve[k_tid]
                    else:
                        copy_vitalsigns['tid'] = np.vstack((copy_vitalsigns['tid'], [[tracks_vsign['tid'][k, 0]]]))
                        print('Confirmed Static Person - tid:', tracks_vsign['tid'][k, 0])
                        VS_height_curve[k_tid] = filt_height_curve[k_tid]
                    # endregion

                # region 18.10. Update VitalSigns
                VitalSigns['tid'] = copy_vitalsigns['tid']
                VitalSigns['position'] = copy_vitalsigns['position']
                VitalSigns['height'] = copy_vitalsigns['height']
                VitalSigns['breathing_times'] = copy_vitalsigns['breathing_times']
                VitalSigns['heartbeat_times'] = copy_vitalsigns['heartbeat_times']
                VitalSigns['breathing_wave'] = copy_vitalsigns['breathing_wave']
                VitalSigns['heartbeat_wave'] = copy_vitalsigns['heartbeat_wave']
                VitalSigns['event'] = copy_vitalsigns['event']
                # endregion
            else:
                # region 18.11. Update VitalSigns
                VitalSigns['tid'] = np.empty((0, 1), dtype=int)
                VitalSigns['position'] = np.empty((0, 2), dtype=float)
                VitalSigns['height'] = np.empty((0, 1), dtype=float)
                VitalSigns['breathing_times'] = np.empty((0, 1), dtype=float)
                VitalSigns['heartbeat_times'] = np.empty((0, 1), dtype=float)
                VitalSigns['breathing_wave'] = np.empty((0, Len_cmplx - 1), dtype=float)
                VitalSigns['heartbeat_wave'] = np.empty((0, Len_cmplx - 1 ), dtype=float)
                VitalSigns['event'] = np.empty((0, 1), dtype=np.string_)
                VS_height_curve.clear()
                # endregion
            # endregion

            # region 19. Update GetData_CmplxforVitalSign
            if age_EnvirSen > window_vsign:
                age_EnvirSen = age_EnvirSen - 1
                CmplxforVitalSign = np.delete(CmplxforVitalSign, range(mov_framelen_vsign), axis=0)
            # endregion

            end_vsign = time.time()
            #print('VSR Execution Time:', round(end_vsign - start_vsign, 3), 's')


# endregion


# ------------------------------------------------------------------
# region Plot Results
def Processing_Plot(EnvirSensing, PCounting, Tracks, VitalSigns, VS_height_curve, ):
    # region Initialize parameters for real-time plotting
    win = pg.GraphicsLayoutWidget(show=True)
    win.setBackground("w")
    win.resize(1400, 700)
    # Enable antialiasing for prettier plots
    pg.setConfigOptions(antialias=True)
    labelStyle = {'font-size': '15pt'}
    font = pg.Qt.QtGui.QFont()
    font.setPixelSize(15)

    # plt_pos
    plt_pos = win.addPlot(title="<h3>Object Tracking and Localization</h3>", row=1, col=1, rowspan=2, colspan=2)  #
    plt_pos.setMinimumWidth(400)
    plt_pos.setMinimumWidth(400)
    plt_pos.setRange(yRange=(-6, 0.5), xRange=(-4, 4))
    ay = plt_pos.getAxis('left')
    ticks = np.arange(-6, 1)
    ay.setTicks([[(v, str(-1 * v)) for v in ticks]])

    plt_pos.setLabel('bottom', "X-coordinate", units='m', **labelStyle)
    plt_pos.setLabel('left', "Y-coordinate", units='m', **labelStyle)
    plt_pos.getAxis("bottom").setStyle(tickFont=font)
    plt_pos.getAxis("left").setStyle(tickFont=font)
    plt_pos.getAxis('left').setTextPen('k')
    plt_pos.getAxis('bottom').setTextPen('k')
    plt_pos.showGrid(x=True, y=True)
    # People Counting
    plt_pos_text = pg.TextItem(color='k')
    plt_pos_text.setPos(-1, 0.5)
    plt_pos.addItem(plt_pos_text)
    # Positions
    g = Graph()
    plt_pos.addItem(g)
    win.nextColumn()

    # plt_MOT_Height
    plt_height_1 = win.addPlot(title='<h3>Fall Detection (Moving Objects)</h3>', row=3, col=1)
    plt_height_1.setXRange(0, framelen_track)
    plt_height_1.setYRange(0, 2)
    plt_height_1.setLabel('bottom', "Frame", units='#', **labelStyle)
    plt_height_1.setLabel('left', "Height", units='m', **labelStyle)
    plt_height_1.getAxis("bottom").setStyle(tickFont=font)
    plt_height_1.getAxis("left").setStyle(tickFont=font)
    plt_height_1.getAxis('left').setTextPen('k')
    plt_height_1.getAxis('bottom').setTextPen('k')
    plt_height_1.showGrid(y=True)
    plt_height_1_text = pg.TextItem(color='k')
    plt_height_1_text.setPos(framelen_track / 3, 2)
    plt_height_1_text.setFont(font)
    plt_height_1.addItem(plt_height_1_text)
    plt_height_1_curve = plt_height_1.plot(pen=pg.mkPen('k', width=3))

    plt_height_2 = win.addPlot(title='<h3>Fall Detection (Moving Objects)</h3>', row=3, col=2)
    plt_height_2.setXRange(0, framelen_track)
    plt_height_2.setYRange(0, 2)
    plt_height_2.setLabel('bottom', "Frame", units='#', **labelStyle)
    plt_height_2.setLabel('left', "Height", units='m', **labelStyle)
    plt_height_2.getAxis("bottom").setStyle(tickFont=font)
    plt_height_2.getAxis("left").setStyle(tickFont=font)
    plt_height_2.getAxis('left').setTextPen('k')
    plt_height_2.getAxis('bottom').setTextPen('k')
    plt_height_2.showGrid(y=True)
    plt_height_2_text = pg.TextItem(color='k')
    plt_height_2_text.setPos(framelen_track / 3, 2)
    plt_height_2_text.setFont(font)
    plt_height_2.addItem(plt_height_2_text)
    plt_height_2_curve = plt_height_2.plot(pen=pg.mkPen('k', width=3))

    # plt_vs
    plt_brt_1 = win.addPlot(title='<h3>Breathing Waveform</h3>', row=1, col=3)
    plt_brt_1.setXRange(0, Len_cmplx - 1)
    plt_brt_1.setYRange(-2, 2)
    plt_brt_1.setLabel('bottom', "Frame", units='#', **labelStyle)
    plt_brt_1.setLabel('left', "Phase Difference", units='radians', **labelStyle)
    plt_brt_1.getAxis("bottom").setStyle(tickFont=font)
    plt_brt_1.getAxis("left").setStyle(tickFont=font)
    plt_brt_1.getAxis('left').setTextPen('k')
    plt_brt_1.getAxis('bottom').setTextPen('k')
    plt_brt_1.showGrid(y=True)
    plt_brt_1_text = pg.TextItem(color='k')
    plt_brt_1_text.setPos(Len_cmplx / 4, 2)
    plt_brt_1.addItem(plt_brt_1_text)
    plt_brt_1_curve = plt_brt_1.plot(pen=pg.mkPen('k', width=3))

    plt_hrt_1 = win.addPlot(title='<h3>Heartbeat Waveform</h3>', row=2, col=3)
    plt_hrt_1.setXRange(0, Len_cmplx - 1 )
    plt_hrt_1.setYRange(-1, 1)
    plt_hrt_1.setLabel('bottom', "Frame", units='#', **labelStyle)
    plt_hrt_1.setLabel('left', "Denoised Phase", units='radians', **labelStyle)
    plt_hrt_1.getAxis("bottom").setStyle(tickFont=font)
    plt_hrt_1.getAxis("left").setStyle(tickFont=font)
    plt_hrt_1.getAxis('left').setTextPen('k')
    plt_hrt_1.getAxis('bottom').setTextPen('k')
    plt_hrt_1.showGrid(y=True)
    plt_hrt_1_text = pg.TextItem(color='k')
    plt_hrt_1_text.setPos(Len_cmplx / 4, 1)
    plt_hrt_1.addItem(plt_hrt_1_text)
    plt_hrt_1_curve = plt_hrt_1.plot(pen=pg.mkPen('k', width=3))

    plt_height_1_vs = win.addPlot(title='<h3>Fall Detection (Stationary Objects)</h3>', row=3, col=3)
    plt_height_1_vs.setXRange(0, 11)
    plt_height_1_vs.setYRange(0, 2)
    plt_height_1_vs.setLabel('bottom', "Frame", units='#', **labelStyle)
    plt_height_1_vs.setLabel('left', "Height", units='m', **labelStyle)
    plt_height_1_vs.getAxis("bottom").setStyle(tickFont=font)
    plt_height_1_vs.getAxis("left").setStyle(tickFont=font)
    plt_height_1_vs.getAxis('left').setTextPen('k')
    plt_height_1_vs.getAxis('bottom').setTextPen('k')
    plt_height_1_vs.showGrid(y=True)
    plt_height_1_vs_text = pg.TextItem(color='k')
    plt_height_1_vs_text.setPos(framelen_track / 3, 2)
    plt_height_1_vs_text.setFont(font)
    plt_height_1_vs.addItem(plt_height_1_vs_text)
    plt_height_1_vs_curve = plt_height_1_vs.plot(pen=pg.mkPen('k', width=3))

    plt_brt_2 = win.addPlot(title='<h3>Breathing Waveform</h3>', row=1, col=4)
    plt_brt_2.setXRange(0, Len_cmplx - 1)
    plt_brt_2.setYRange(-2, 2)
    plt_brt_2.setLabel('bottom', "Frame", units='#', **labelStyle)
    plt_brt_2.setLabel('left', "Phase Difference", units='radians', **labelStyle)
    plt_brt_2.getAxis("bottom").setStyle(tickFont=font)
    plt_brt_2.getAxis("left").setStyle(tickFont=font)
    plt_brt_2.getAxis('left').setTextPen('k')
    plt_brt_2.getAxis('bottom').setTextPen('k')
    plt_brt_2.showGrid(y=True)
    plt_brt_2_text = pg.TextItem(color='k')
    plt_brt_2_text.setPos(Len_cmplx / 4, 2)
    plt_brt_2.addItem(plt_brt_2_text)
    plt_brt_2_curve = plt_brt_2.plot(pen=pg.mkPen('k', width=3))

    plt_hrt_2 = win.addPlot(title='<h3>Heartbeat Waveform</h3>', row=2, col=4)
    plt_hrt_2.setXRange(0, Len_cmplx - 1 )
    plt_hrt_2.setYRange(-1, 1)
    plt_hrt_2.setLabel('bottom', "Frame", units='#', **labelStyle)
    plt_hrt_2.setLabel('left', "Denoised Phase", units='radians', **labelStyle)
    plt_hrt_2.getAxis("bottom").setStyle(tickFont=font)
    plt_hrt_2.getAxis("left").setStyle(tickFont=font)
    plt_hrt_2.getAxis('left').setTextPen('k')
    plt_hrt_2.getAxis('bottom').setTextPen('k')
    plt_hrt_2.showGrid(y=True)
    plt_hrt_2_text = pg.TextItem(color='k')
    plt_hrt_2_text.setPos(Len_cmplx / 4, 1)
    plt_hrt_2.addItem(plt_hrt_2_text)
    plt_hrt_2_curve = plt_hrt_2.plot(pen=pg.mkPen('k', width=3))

    plt_height_2_vs = win.addPlot(title='<h3>Fall Detection (Stationary Objects)</h3>', row=3, col=4)
    plt_height_2_vs.setXRange(0, 11)
    plt_height_2_vs.setYRange(0, 2)
    plt_height_2_vs.setLabel('bottom', "Frame", units='#', **labelStyle)
    plt_height_2_vs.setLabel('left', "Height", units='m', **labelStyle)
    plt_height_2_vs.getAxis("bottom").setStyle(tickFont=font)
    plt_height_2_vs.getAxis("left").setStyle(tickFont=font)
    plt_height_2_vs.getAxis('left').setTextPen('k')
    plt_height_2_vs.getAxis('bottom').setTextPen('k')
    plt_height_2_vs.showGrid(y=True)
    plt_height_2_vs_text = pg.TextItem(color='k')
    plt_height_2_vs_text.setPos(framelen_track / 3, 2)
    plt_height_2_vs_text.setFont(font)
    plt_height_2_vs.addItem(plt_height_2_vs_text)
    plt_height_2_vs_curve = plt_height_2_vs.plot(pen=pg.mkPen('k', width=3))

    # endregion

    def initialize():
        # Position
        g.setData(pos=np.empty((0, 2)), pen=None, symbol='o', symbolBrush=(255, 0, 0), pxMode=False, size=0.15)
        # plt_pos_text.setText("")

        # Height
        plt_height_1_curve.setData([])
        plt_height_2_curve.setData([])
        plt_height_1_text.setText("")
        plt_height_2_text.setText("")
        plt_height_1_vs_curve.setData([])
        plt_height_2_vs_curve.setData([])
        plt_height_1_vs_text.setText("")
        plt_height_2_vs_text.setText("")

        # Breathing
        plt_brt_1_curve.setData([])
        plt_brt_2_curve.setData([])
        plt_brt_1_text.setText("")
        plt_brt_2_text.setText("")

        # Heartbeat
        plt_hrt_1_curve.setData([])
        plt_hrt_2_curve.setData([])
        plt_hrt_1_text.setText("")
        plt_hrt_2_text.setText("")

    # region Update Plotting
    def update():
        initialize()

        try:
            copy_Tracks = copy.deepcopy(Tracks)
            copy_VitalSigns = copy.deepcopy(VitalSigns)
            copy_VS_height_curve = copy.deepcopy(VS_height_curve)

            brush_color = "rbcmykwrgrbcmykwrgrbcmykwrgrbcmykwrg"
            brush_list = [pg.mkColor(c) for c in brush_color]

            texts = []
            position_plt = np.empty((0, 2), dtype=float)

            # People Counting
            if PCounting.value == -1:
                plt_pos_text.setText("People Counting: Waiting!")
            else:
                plt_pos_text.setText("Environment Status: %d \n People Counting: %d" % (
                    EnvirSensing['Environment'], PCounting.value))

            # Moving Objects
            if len(copy_Tracks['tid']) > 0:
                # Tracking
                position_plt = np.vstack((position_plt, -1 * copy_Tracks['position'][:, range(2)]))
                for k in range(len(copy_Tracks['tid'])):
                    # Position
                    txt_Tracking = "ID: %d" % copy_Tracks['tid'][k, 0] + "\n" + \
                                   "Pos: (%.2f, %.2f, %.2f)" % (-1 * copy_Tracks['position'][k, 0],
                                                                copy_Tracks['position'][k, 1],
                                                                copy_Tracks['position'][k, 2]) + " m" + "\n" + \
                                   "Vel: (%.2f, %.2f, %.2f)" % (copy_Tracks['velocity'][k, 0],
                                                                copy_Tracks['velocity'][k, 1],
                                                                copy_Tracks['velocity'][k, 2]) + " m/s" + "\n" + \
                                   "Acc: (%.2f, %.2f, %.2f)" % (copy_Tracks['acceleration'][k, 0],
                                                                copy_Tracks['acceleration'][k, 1],
                                                                copy_Tracks['acceleration'][k, 2]) + " m/s2" + "\n" + \
                                   "Activity: " + copy_Tracks['activity'][k, 0] + "\n" + \
                                   "Status: " + "Moving"
                    texts.append(txt_Tracking)

                # Height
                if len(copy_Tracks['tid']) == 1:
                    # 1
                    plt_height_1_curve.setData(copy_Tracks['height_curve'][0, :])
                    plt_height_1_text.setText("Tid:")
                    plt_height_1_text.setText(
                        "ID: " + str(copy_Tracks['tid'][0, 0]) + ", Activity: " + copy_Tracks['activity'][0, 0])
                    if copy_Tracks['activity'][0, 0] == 'Walk':
                        plt_height_1_text.setColor(color='k')
                    else:
                        plt_height_1_text.setColor(color='r')
                else:
                    # 1
                    plt_height_1_curve.setData(copy_Tracks['height_curve'][0, :])
                    plt_height_1_text.setText(
                        "ID: " + str(copy_Tracks['tid'][0, 0]) + ", Activity: " + copy_Tracks['activity'][0, 0])
                    if copy_Tracks['activity'][0, 0] == 'Walk':
                        plt_height_1_text.setColor(color='k')
                    else:
                        plt_height_1_text.setColor(color='r')

                    # 2
                    plt_height_2_curve.setData(copy_Tracks['height_curve'][1, :])
                    plt_height_2_text.setText(
                        "ID: " + str(copy_Tracks['tid'][1, 0]) + ", Activity: " + copy_Tracks['activity'][1, 0])
                    if copy_Tracks['activity'][1, 0] == 'Walk':
                        plt_height_2_text.setColor(color='k')
                    else:
                        plt_height_2_text.setColor(color='r')

            # Stationary Objects
            if len(copy_VitalSigns['tid']) > 0:
                # Vital Signs
                position_plt = np.vstack((position_plt, -1 * copy_VitalSigns['position'][:, range(2)]))
                for k in range(len(copy_VitalSigns['tid'])):
                    txt_VitalSign = "ID: %d" % copy_VitalSigns['tid'][k, 0] + "\n" + \
                                    "Pos: (%.2f, %.2f, %.2f)" % (-1 * copy_VitalSigns['position'][k, 0],
                                                                 copy_VitalSigns['position'][k, 1],
                                                                 copy_VitalSigns['height'][k, 0]) + " m" + "\n" + \
                                    "Breathing Times: %d" % copy_VitalSigns['breathing_times'][k, 0] + "\n" + \
                                    "Heartbeat Times: %d" % copy_VitalSigns['heartbeat_times'][k, 0] + "\n" + \
                                    "Event:" + copy_VitalSigns['event'][k, 0] + "\n" + \
                                    "Status:" + "Stationary"
                    texts.append(txt_VitalSign)

                # Breating & Heartbeat Wave
                if len(copy_VitalSigns['tid']) == 1:
                    peak_1 = np.max(np.abs(copy_VitalSigns['heartbeat_wave'][0, :]))
                    if peak_1 > 1:
                        plt_hrt_1.setYRange(-np.ceil(peak_1 + 1), np.ceil(peak_1 + 1))
                        plt_hrt_1_text.setPos(Len_cmplx / 4, np.ceil(peak_1 + 1))
                    else:
                        plt_hrt_1.setYRange(-1, 1)
                        plt_hrt_1_text.setPos(Len_cmplx / 4, 1)
                    plt_brt_1_curve.setData(copy_VitalSigns['breathing_wave'][0, :],
                                            pen=brush_color[len(copy_Tracks['tid'])])
                    plt_hrt_1_curve.setData(copy_VitalSigns['heartbeat_wave'][0, :],
                                            pen=brush_color[len(copy_Tracks['tid'])])
                    plt_brt_1_text.setText("ID: " + str(copy_VitalSigns['tid'][0, 0]) +
                                           ", Breathing Times: %d" % copy_VitalSigns['breathing_times'][0, 0])
                    plt_hrt_1_text.setText("ID: " + str(copy_VitalSigns['tid'][0, 0]) +
                                           ", Heartbeat Times: %d" % copy_VitalSigns['heartbeat_times'][0, 0])

                    # Height
                    # 1
                    plt_height_1_vs_curve.setData(copy_VS_height_curve[copy_VitalSigns['tid'][0, 0]])
                    # print(copy_VS_height_curve[copy_VitalSigns['tid'][0, 0]])
                    plt_height_1_vs_text.setText(
                        "ID:" + str(copy_VitalSigns['tid'][0, 0]) + ", Event:" + copy_VitalSigns['event'][0, 0])
                    if copy_VitalSigns['event'][0, 0] == 'Normal':
                        plt_height_1_vs_text.setColor(color='k')
                    else:
                        plt_height_1_vs_text.setColor(color='r')
                else:
                    # 1
                    peak_1 = np.max(np.abs(copy_VitalSigns['heartbeat_wave'][0, :]))
                    if peak_1 > 1:
                        plt_hrt_1.setYRange(-np.ceil(peak_1 + 1), np.ceil(peak_1 + 1))
                        plt_hrt_1_text.setPos(Len_cmplx / 4, np.ceil(peak_1 + 1))
                    else:
                        plt_hrt_1.setYRange(-1, 1)
                        plt_hrt_1_text.setPos(Len_cmplx / 4, 1)
                    plt_brt_1_curve.setData(copy_VitalSigns['breathing_wave'][0, :],
                                            pen=brush_color[len(copy_Tracks['tid'])])
                    plt_hrt_1_curve.setData(copy_VitalSigns['heartbeat_wave'][0, :],
                                            pen=brush_color[len(copy_Tracks['tid'])])
                    plt_brt_1_text.setText("ID: " + str(copy_VitalSigns['tid'][0, 0]) +
                                           ", Breathing Times: %d" % copy_VitalSigns['breathing_times'][0, 0])
                    plt_hrt_1_text.setText("ID: " + str(copy_VitalSigns['tid'][0, 0]) +
                                           ", Heartbeat Times: %d" % copy_VitalSigns['heartbeat_times'][0, 0])

                    # 2
                    peak_2 = np.max(np.abs(copy_VitalSigns['heartbeat_wave'][1, :]))
                    if peak_2 > 1:
                        plt_hrt_2.setYRange(-np.ceil(peak_2 + 1), np.ceil(peak_2 + 1))
                        plt_hrt_2_text.setPos(Len_cmplx / 4, np.ceil(peak_2 + 1))
                    else:
                        plt_hrt_2.setYRange(-1, 1)
                        plt_hrt_2_text.setPos(Len_cmplx / 4, 1)
                    plt_brt_2_curve.setData(copy_VitalSigns['breathing_wave'][1, :],
                                            pen=brush_color[len(copy_Tracks['tid']) + 1])
                    plt_hrt_2_curve.setData(copy_VitalSigns['heartbeat_wave'][1, :],
                                            pen=brush_color[len(copy_Tracks['tid']) + 1])

                    plt_brt_2_text.setText("ID: " + str(copy_VitalSigns['tid'][1, 0]) +
                                           ", Breathing Times: %d" % copy_VitalSigns['breathing_times'][1, 0])
                    plt_hrt_2_text.setText("ID: " + str(copy_VitalSigns['tid'][1, 0]) +
                                           ", Heartbeat Times: %d" % copy_VitalSigns['heartbeat_times'][1, 0])

                    # Height
                    if np.shape(copy_VitalSigns['tid'])[0] == 1:
                        # 1
                        plt_height_1_vs_curve.setData(copy_VS_height_curve[copy_VitalSigns['tid'][0, 0]])
                        # print(copy_VS_height_curve[copy_VitalSigns['tid'][0, 0]])
                        plt_height_1_vs_text.setText(
                            "ID: " + str(copy_VitalSigns['tid'][0, 0]) + ", Event:" + copy_VitalSigns['event'][0, 0])
                        if copy_VitalSigns['event'][0, 0] == 'Normal':
                            plt_height_1_vs_text.setColor(color='k')
                        else:
                            plt_height_1_vs_text.setColor(color='r')
                    else:
                        # 1
                        plt_height_1_vs_curve.setData(copy_VS_height_curve[copy_VitalSigns['tid'][0, 0]])
                        plt_height_1_vs_text.setText(
                            "ID: " + str(copy_VitalSigns['tid'][0, 0]) + ", Event:" + copy_VitalSigns['event'][0, 0])
                        if copy_VitalSigns['event'][0, 0] == 'Normal':
                            plt_height_1_vs_text.setColor(color='k')
                        else:
                            plt_height_1_vs_text.setColor(color='r')

                        # 2
                        plt_height_2_vs_curve.setData(copy_VS_height_curve[copy_VitalSigns['tid'][1, 0]])
                        plt_height_2_vs_text.setText(
                            "ID: " + str(copy_VitalSigns['tid'][1, 0]) + ", Event:" + copy_VitalSigns['event'][1, 0])
                        if copy_VitalSigns['event'][1, 0] == 'Normal':
                            plt_height_2_vs_text.setColor(color='k')
                        else:
                            plt_height_2_vs_text.setColor(color='r')

            if len(copy_VitalSigns['tid_waiting']) > 0:
                # Vital Signs
                position_plt = np.vstack((position_plt, -1 * copy_VitalSigns['position_waiting'][:, range(2)]))
                for k in range(np.shape(copy_VitalSigns['tid_waiting'])[0]):
                    txt_VitalSign = "ID: %d" % copy_VitalSigns['tid_waiting'][k, 0] + "\n" + \
                                    "Pos: (%.2f, %.2f)" % (-1 * copy_VitalSigns['position_waiting'][k, 0],
                                                           copy_VitalSigns['position_waiting'][k, 1]) + " m" + "\n" + \
                                    "Status:" + "Stationary"
                    texts.append(txt_VitalSign)

            g.setData(pos=position_plt[:, range(2)], text=texts, pen=None,
                      symbol='o', symbolBrush=brush_list[:(len(copy_Tracks['tid']) +
                                                           len(copy_VitalSigns['tid']) +
                                                           len(copy_VitalSigns['tid_waiting']))], pxMode=False,
                      size=0.15)
        except Exception as e:
            pass

    update.flag_PLT = 0
    timer = pg.QtCore.QTimer()
    timer.timeout.connect(lambda: update())
    timer.start(100)
    pg.exec()
    # endregion


# endregion


if __name__ == '__main__':
    manager = Manager()
    lock_MOT = manager.Lock()
    lock_VS = manager.Lock()

    # region Initialize GetData for mmWave Data Collection
    # region GetData
    syn_Heatmap = manager.Semaphore(0)
    syn_PoinyObj = manager.Semaphore(0)
    mmWave_Buffer = manager.dict()
    mmWave_Buffer['GetData_PointCloud'] = np.empty((0, 7), dtype=float)
    mmWave_Buffer['GetData_Cmplx'] = np.empty((0, 64, gl_numVirtAnt), dtype=complex)
    mmWave_Buffer['NULL_POINT_CLOUD'] = 0
    # endregion
    # endregion

    # region Parameters of Interest for Upper Applications
    # region Environment Sensing
    '''
    (1) Stationary persons - False, Moving persons - False
    EnvirSensing['Environment'] = 0   # Empty Room
    (2) Stationary persons - False, Moving persons - True
    EnvirSensing['Environment'] = 1   # Dynamic Sence
    (3) Stationary persons - True, Moving persons - False
    EnvirSensing['Environment'] = 2   # Static Sence
    (4) Stationary persons - True, Moving persons - True
    EnvirSensing['Environment'] = 3   # Dynamic & Static Sence
    (5) Waiting for determining
    EnvirSensing['Environment'] = -1
    '''
    EnvirSensing = manager.dict()
    EnvirSensing['Environment'] = -1  # -1, 0, 1, 2, 3
    # endregion

    # region People Counting
    '''
    Number of People (including moving and stationary persons)
    '''
    PCounting = manager.Value(int, -1)
    # endregion

    # region Multiple Object Tracking
    '''
    Trajectory and Activity of Moving Persons
    Tracks['tid']: ID
    Tracks['position']: XYZ Position
    Tracks['velocity']: XYZ Velocity
    Tracks['acceleration']: XYZ Acceleration
    Tracks['activity']: 1: Fall Down, 2: Crouch Down, 3: Walk
    '''
    Tracks = manager.dict()
    Tracks['tid'] = np.empty((0, 1), dtype=int)
    Tracks['position'] = np.empty((0, 3), dtype=float)
    Tracks['velocity'] = np.empty((0, 3), dtype=float)
    Tracks['acceleration'] = np.empty((0, 3), dtype=float)
    Tracks['height_curve'] = np.empty((0, framelen_track), dtype=float)
    Tracks['activity'] = np.empty((0, 1), dtype=np.string_)
    # endregion

    # region Vital Signs Recognition
    '''
    Vital Signs and Abnormal Event of Stationary Persons
    VitalSigns['tid']: ID
    VitalSigns['position']: XY Position
    VitalSigns['height']: Z Position
    VitalSigns['breathing_times']: Breathing Times
    VitalSigns['heartbeat_times']: Heartbeat Times
    VitalSigns['breathing_wave']: Breathing Wave
    VitalSigns['heartbeat_wave']: Noise-tolerant Heartbeat Wave
    VitalSigns['event']: 1: Fall Down, 2: Normal
    '''
    VitalSigns = manager.dict()
    VitalSigns['tid'] = np.empty((0, 1), dtype=int)
    VitalSigns['position'] = np.empty((0, 2), dtype=float)
    VitalSigns['height'] = np.empty((0, 1), dtype=float)
    VitalSigns['breathing_times'] = np.empty((0, 1), dtype=float)
    VitalSigns['heartbeat_times'] = np.empty((0, 1), dtype=float)
    VitalSigns['breathing_wave'] = np.empty((0, Len_cmplx - 1), dtype=float)
    VitalSigns['heartbeat_wave'] = np.empty((0, Len_cmplx - 1 ), dtype=float)
    VitalSigns['event'] = np.empty((0, 1), dtype=np.string_)

    VitalSigns['tid_waiting'] = np.empty((0, 1), dtype=int)
    VitalSigns['position_waiting'] = np.empty((0, 2), dtype=float)
    VitalSigns['height_waiting'] = np.empty((0, 1), dtype=float)
    # endregion

    # region Height Curve
    VS_height_curve = manager.dict()
    # endregion
    # endregion

    pool = Pool(processes=4)
    pool.apply_async(Processing_GetData, args=(mmWave_Buffer, syn_PoinyObj, syn_Heatmap,),
                     error_callback=error_handler)
    pool.apply_async(Processing_MOT, args=(syn_PoinyObj, mmWave_Buffer, Tracks,),
                     error_callback=error_handler)
    pool.apply_async(Processing_VSR,
                     args=(syn_Heatmap, mmWave_Buffer, EnvirSensing, PCounting, Tracks, VitalSigns, VS_height_curve,),
                     error_callback=error_handler)
    pool.apply_async(Processing_Plot,
                     args=(EnvirSensing, PCounting, Tracks, VitalSigns, VS_height_curve,),
                     error_callback=error_handler)

    pool.close()
    pool.join()
