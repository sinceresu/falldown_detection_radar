from numba.pycc import CC
import numpy as np
from numba import complex128, float64, int64, boolean
from numba import njit

cc = CC('Numba_Module')

# region MOT
@cc.export('Search_TrackLet', 'Tuple((i8, f8[:, :]))(f8[:, :], f8[:], f8[:, :], f8, i8)')
def Search_TrackLet(mmWave_Buffer_GetData_PointCloud, framebuff_ID, unq_ave_res_pointObj, Ts, framelen_track):
    '''' Extended Kalman Filter based Method to Search Tracklets of Interest
    numba package is used to speed up.
    Args:
        mmWave_Buffer_GetData_PointCloud: mmWave_Buffer['GetData_PointCloud']
        framebuff_ID: Frame ID of mmWave Radar Data
        unq_ave_res_pointObj:  N×5 2D Array after Downsampling and Weighted Averaging, X, Y, Z, Velocity, SNR
        Ts: Sampling Time Interval
    Returns:
        trackLet: Tracklets of Interests Stored in Dict
    '''''

    # Initialize trackLet
    # 0. track_Id,
    # 1. Pos_X, 2. Pos_Y, 3. Pos_Z,                 # Position
    # 4. Vx, 5. Vy, 6. Vz,                          # Velocity
    # 7. ax, 8. ay, 9. az,                          # Acceleration
    # 10. Pos_X_w, 11. Pos_Y_w, 12. Pos_Z_w,        # Position without optimization
    # 13. Doppler_Velocity, 14. SNR                 # Doppler Velocity, SNR
    trackLet = np.empty((0, 15), dtype=np.float64)
    # In the following, we apply forward and backward Kalman filters to obtain each tracklet.
    # region 1. Initialize Extended Kalman Filter
    # State Transition Matrix F is to specify the dynamics of the motion model
    EKF_F = np.array([[1., 0., 0., Ts, 0., 0., 0.5 * (Ts ** 2), 0., 0.],
                      [0., 1., 0., 0., Ts, 0., 0., 0.5 * (Ts ** 2), 0.],
                      [0., 0., 1., 0., 0., Ts, 0., 0., 0.5 * (Ts ** 2)],
                      [0., 0., 0., 1., 0., 0., Ts, 0., 0.],
                      [0., 0., 0., 0., 1., 0., 0., Ts, 0.],
                      [0., 0., 0., 0., 0., 1., 0., 0., Ts],
                      [0., 0., 0., 0., 0., 0., 1., 0., 0.],
                      [0., 0., 0., 0., 0., 0., 0., 1., 0.],
                      [0., 0., 0., 0., 0., 0., 0., 0., 1.]])  # 9x9

    # Process Noise Covariance Matrix based on Piecewise White Noise Model
    EKF_Q = np.array([[np.power(Ts, 4) / 4, 0., 0., np.power(Ts, 3) / 2, 0., 0., np.power(Ts, 2) / 2, 0., 0.],
                      [0., np.power(Ts, 4) / 4, 0., 0., np.power(Ts, 4) / 2, 0., 0., np.power(Ts, 2) / 2, 0.],
                      [0., 0., np.power(Ts, 4) / 4, 0., 0., np.power(Ts, 3) / 2, 0., 0., np.power(Ts, 2) / 2],
                      [np.power(Ts, 3) / 2, 0., 0., np.power(Ts, 2), 0., 0., Ts, 0., 0.],
                      [0., np.power(Ts, 3) / 2, 0., 0., np.power(Ts, 2), 0., 0., Ts, 0.],
                      [0., 0., np.power(Ts, 3) / 2, 0., 0., np.power(Ts, 2), 0., 0., Ts],
                      [np.power(Ts, 2) / 2, 0., 0., Ts, 0., 0., 1.0, 0., 0.],
                      [0., np.power(Ts, 2) / 2, 0., 0., Ts, 0., 0., 1.0, 0.],
                      [0., 0., np.power(Ts, 2) / 2, 0., 0., Ts, 0., 0., 1.0]]) * (2 ** 2)
    # endregion
    count = 0
    for k_TL in np.arange(np.shape(unq_ave_res_pointObj)[0]):
        # Tracklet initialization, the length of a tracklet is #framelen_track#.
        trackLet_Point_forward = np.zeros((framelen_track, 15), dtype=np.float64)
        trackLet_Point_backward = np.zeros((framelen_track, 15), dtype=np.float64)

        # age: the number of frames since the track was first detected
        # totalVisibleCount: the total number of frames in which the track was detected (visible)
        # consecutiveInvisibleCount: the number of consecutive frames for which the track was not detected (invisible).
        age_Point = 1
        totalVisibleCount_Point = 1
        consecutiveInvisibleCount_Point = 0

        # region 2. Forward Extended Kalman Filtering
        # region Initialize Extended Kalman Filter
        ave_Pos_X = unq_ave_res_pointObj[k_TL, 0]
        ave_Pos_Y = unq_ave_res_pointObj[k_TL, 1]
        ave_Pos_Z = unq_ave_res_pointObj[k_TL, 2]
        ave_velocity = unq_ave_res_pointObj[k_TL, 3]
        ave_snr = unq_ave_res_pointObj[k_TL, 4]
        EKF_Vx = 1 / 2 * ave_velocity  # m/s
        EKF_Vy = 1 / 2 * ave_velocity  # m/s
        EKF_Vz = 0.5  # m/s
        EKF_ax = 1.0  # m/s^2
        EKF_ay = 1.0  # m/s^2
        EKF_az = 0.5  # m/s^2
        # endregion

        # region Initialize forward trackLet
        trackLet_Point_forward[0, :] = np.array([[k_TL,  # track_Id
                                                  ave_Pos_X, ave_Pos_Y, ave_Pos_Z,  # Position
                                                  EKF_Vx, EKF_Vy, EKF_Vz,  # Velocity
                                                  EKF_ax, EKF_ay, EKF_az,  # Acceleration
                                                  ave_Pos_X, ave_Pos_Y, ave_Pos_Z,  # Position_w
                                                  ave_velocity, ave_snr]])  # Doppler Velocity, SNR
        # endregion

        # region State vector of tracking object
        EKF_S = np.array([[ave_Pos_X],
                          [ave_Pos_Y],
                          [ave_Pos_Z],
                          [EKF_Vx],
                          [EKF_Vy],
                          [EKF_Vz],
                          [EKF_ax],
                          [EKF_ay],
                          [EKF_az]])  # 9*1
        # endregion

        # region State vector estimation error covariance matrix
        EKF_P = np.array([[1, 0., 0., 0., 0., 0., 0., 0., 0.],
                          [0, 1, 0., 0., 0., 0., 0., 0., 0.],
                          [0, 0., 1, 0., 0., 0., 0., 0., 0.],
                          [0, 0., 0., 2.5, 0., 0., 0., 0., 0.],
                          [0, 0, 0., 0., 2.5, 0., 0., 0., 0.],
                          [0, 0., 0, 0., 0., 2.5, 0., 0., 0.],
                          [0, 0., 0, 0., 0., 0, 5, 0., 0.],
                          [0, 0., 0, 0., 0., 0, 0., 5, 0.],
                          [0, 0., 0, 0., 0., 0, 0., 0, 5]]) ** 2
        # endregion

        # region Measurement Covariance Matrix (diagonal Matrix)
        EKF_R = np.array([[0.5, 0., 0., 0.],
                          [0., 0.3, 0., 0.],
                          [0., 0., 0.3, 0.],
                          [0., 0., 0., 1]]) ** 2
        # endregion

        # region Save parameters for backward smoothing
        Back_EKF_u = {}
        Back_EKF_P = {}
        Back_EKF_P_apr = {}
        Back_EKF_u[0] = EKF_S
        Back_EKF_P[0] = EKF_P
        # endregion

        Back_flag = False
        for k_Next in np.arange(1, framelen_track + 1):
            age_Point += 1

            # Prediction Step
            # Compute the a-priori state estimate
            EKF_S_apr = np.dot(EKF_F, EKF_S)
            EKF_P_apr = np.dot(np.dot(EKF_F, EKF_P), EKF_F.T) + EKF_Q
            Back_EKF_P_apr[k_Next - 1] = EKF_P_apr
            if k_Next == framelen_track:
                continue

            # Test whether there exists other neighboring points near the predicted point
            key_Next = np.where(mmWave_Buffer_GetData_PointCloud[:, 0] == framebuff_ID[k_Next])[0]
            pointObj_Next = mmWave_Buffer_GetData_PointCloud[key_Next, :]
            pointObj_Next = pointObj_Next[:, np.arange(2, 7)]  # X, Y, Z, Velocity, SNR
            # Calculate Distances
            dist_Diff = np.sqrt(np.sum((pointObj_Next[:, np.arange(3)] - EKF_S_apr[np.arange(3), :].T) ** 2, axis=1))
            pcloud_Ind_Next = np.where(dist_Diff <= 1)[0]  # distance threshold: 1 m

            # Calculate coordinate points for comparison
            if len(pcloud_Ind_Next) == 0:  # No associated points, we regard that the target is still
                EKF_S = EKF_S_apr
                EKF_P = EKF_P_apr
                consecutiveInvisibleCount_Point += 1
            else:
                totalVisibleCount_Point += 1
                consecutiveInvisibleCount_Point = 0

                # Association Step
                # Weighted Averaging
                ave_Pos_X = np.sum(pointObj_Next[pcloud_Ind_Next, 0] * pointObj_Next[pcloud_Ind_Next, 4]) / \
                            np.sum(pointObj_Next[pcloud_Ind_Next, 4])
                ave_Pos_Y = np.sum(pointObj_Next[pcloud_Ind_Next, 1] * pointObj_Next[pcloud_Ind_Next, 4]) / \
                            np.sum(pointObj_Next[pcloud_Ind_Next, 4])
                ave_Pos_Z = np.sum(pointObj_Next[pcloud_Ind_Next, 2] * pointObj_Next[pcloud_Ind_Next, 4]) / \
                            np.sum(pointObj_Next[pcloud_Ind_Next, 4])
                ave_velocity = np.sum(pointObj_Next[pcloud_Ind_Next, 3] * pointObj_Next[pcloud_Ind_Next, 4]) / \
                               np.sum(pointObj_Next[pcloud_Ind_Next, 4])
                ave_snr = np.mean(pointObj_Next[pcloud_Ind_Next, 4])

                # a. Compute measurement residual
                # Measurement from prediction
                EKF_H_s_apr = \
                    np.array([[np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2 + EKF_S_apr[2, 0] ** 2),
                               np.arctan2(EKF_S_apr[0, 0], EKF_S_apr[1, 0]),
                               np.arctan2(EKF_S_apr[2, 0], np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2)),
                               (EKF_S_apr[0, 0] * EKF_S_apr[3, 0] + EKF_S_apr[1, 0] * EKF_S_apr[4, 0] +
                                EKF_S_apr[2, 0] * EKF_S_apr[5, 0]) / \
                               np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2 + EKF_S_apr[2, 0] ** 2)]]).T

                # Actual measurement
                EKF_u = np.array([[np.sqrt(ave_Pos_X ** 2 + ave_Pos_Y ** 2 + ave_Pos_Z ** 2),
                                   np.arctan2(ave_Pos_X, ave_Pos_Y),
                                   np.arctan2(ave_Pos_Z, np.sqrt(ave_Pos_X ** 2 + ave_Pos_Y ** 2)),
                                   ave_velocity]]).T
                EKF_y = EKF_u - EKF_H_s_apr

                # Compute innovation covariance
                # region Jacobian Matrix
                r_J = np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2 + EKF_S_apr[2, 0] ** 2)
                EKF_J_H = np.zeros((4, 9), dtype=np.float64)

                EKF_J_H[0, 0] = EKF_S_apr[0, 0] / r_J
                EKF_J_H[0, 1] = EKF_S_apr[1, 0] / r_J
                EKF_J_H[0, 2] = EKF_S_apr[2, 0] / r_J

                EKF_J_H[1, 0] = EKF_S_apr[1, 0] / (EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2)
                EKF_J_H[1, 1] = - EKF_S_apr[0, 0] / (EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2)

                EKF_J_H[2, 0] = - EKF_S_apr[0, 0] / (r_J ** 2) * \
                                EKF_S_apr[2, 0] / np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2)
                EKF_J_H[2, 1] = - EKF_S_apr[1, 0] / (r_J ** 2) * \
                                EKF_S_apr[2, 0] / np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2)
                EKF_J_H[2, 2] = np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2) / (r_J ** 2)

                EKF_J_H[3, 0] = (EKF_S_apr[1, 0] * (
                        EKF_S_apr[3, 0] * EKF_S_apr[1, 0] - EKF_S_apr[4, 0] * EKF_S_apr[0, 0]) +
                                 EKF_S_apr[2, 0] * (
                                         EKF_S_apr[3, 0] * EKF_S_apr[2, 0] - EKF_S_apr[5, 0] * EKF_S_apr[
                                     0, 0])) / np.power(r_J, 3)
                EKF_J_H[3, 1] = (EKF_S_apr[0, 0] * (
                        EKF_S_apr[4, 0] * EKF_S_apr[0, 0] - EKF_S_apr[3, 0] * EKF_S_apr[1, 0]) +
                                 EKF_S_apr[2, 0] * (
                                         EKF_S_apr[4, 0] * EKF_S_apr[2, 0] - EKF_S_apr[5, 0] * EKF_S_apr[
                                     1, 0])) / np.power(r_J, 3)
                EKF_J_H[3, 2] = (EKF_S_apr[0, 0] * (
                        EKF_S_apr[5, 0] * EKF_S_apr[0, 0] - EKF_S_apr[3, 0] * EKF_S_apr[2, 0]) +
                                 EKF_S_apr[1, 0] * (
                                         EKF_S_apr[5, 0] * EKF_S_apr[1, 0] - EKF_S_apr[4, 0] * EKF_S_apr[
                                     2, 0])) / np.power(r_J, 3)
                EKF_J_H[3, 3] = EKF_S_apr[0, 0] / r_J
                EKF_J_H[3, 4] = EKF_S_apr[1, 0] / r_J
                EKF_J_H[3, 5] = EKF_S_apr[2, 0] / r_J
                # endregion

                # b. Group residual Covariance Matrix
                EKF_C = np.dot(np.dot(EKF_J_H, EKF_P_apr), EKF_J_H.T) + EKF_R

                # c. Compute Kalman gain
                EKF_K = np.dot(np.dot(EKF_P_apr, EKF_J_H.T), np.linalg.inv(EKF_C))

                # d. Compute a-posteriori state vector
                EKF_S = EKF_S_apr + np.dot(EKF_K, EKF_y)
                # region Remove Outliers
                # Y-axis
                if EKF_S[1, 0] < 0:
                    EKF_S[1, 0] = 0.1
                # vx
                if EKF_S[3, 0] < -2.5:  # 2 m/s
                    EKF_S[3, 0] = -2.5
                if EKF_S[3, 0] > 2.5:  # 2 m/s
                    EKF_S[3, 0] = 2.5
                # vy
                if EKF_S[4, 0] < -2.5:  # 2 m/s
                    EKF_S[4, 0] = -2.5
                if EKF_S[4, 0] > 2.5:  # 2 m/s
                    EKF_S[4, 0] = 2.5
                # vz
                if EKF_S[5, 0] < -1.5:  # 1 m/s
                    EKF_S[5, 0] = -1.5
                if EKF_S[5, 0] > 1.5:  # 1 m/s
                    EKF_S[5, 0] = 1.5
                # ax
                if EKF_S[6, 0] < -1:  # 1.5 / (5*0.1) = 3 m/s2
                    EKF_S[6, 0] = -1
                if EKF_S[6, 0] > 1:  # 1.5 / (5*0.1) = 3 m/s2
                    EKF_S[6, 0] = 1
                # ay
                if EKF_S[7, 0] < -1:  # 1.5 / (5*0.1) = 3 m/s2
                    EKF_S[7, 0] = -1
                if EKF_S[7, 0] > 1:  # 1.5 / (5*0.1) = 3 m/s2
                    EKF_S[7, 0] = 1
                # az
                if EKF_S[8, 0] < -1:  # 1 / (5*0.1) = 2 m/s2
                    EKF_S[8, 0] = -1
                if EKF_S[8, 0] > 1:  # 1 / (5*0.1) = 2 m/s2
                    EKF_S[8, 0] = 1
                # endregion

                # e. Compute a-posteriori error covariance
                EKF_P = EKF_P_apr - np.dot(np.dot(EKF_K, EKF_J_H), EKF_P_apr)
            Back_EKF_u[k_Next] = EKF_S
            Back_EKF_P[k_Next] = EKF_P

            # Update trackLet_Point_forward
            trackLet_Point_forward[k_Next, :] = np.array([[k_Next,  # track_Id
                                                           EKF_S[0, 0], EKF_S[1, 0], EKF_S[2, 0],  # Position
                                                           EKF_S[3, 0], EKF_S[4, 0], EKF_S[5, 0],  # Velocity
                                                           EKF_S[6, 0], EKF_S[7, 0], EKF_S[8, 0],  # Acceleration
                                                           ave_Pos_X, ave_Pos_Y, ave_Pos_Z,  # Position_w
                                                           ave_velocity,  # Doppler Velocity
                                                           ave_snr]])  # SNR
            if totalVisibleCount_Point / age_Point < 0.6 or consecutiveInvisibleCount_Point >= 3:
                Back_flag = False
                break
            else:
                Back_flag = True
        # endregion

        # region 3. Backward Kalman Filter
        # Determine if the estimated trajectory in trackLet_Point_forward is confident enough
        if Back_flag == True:
            count += 1
            # region Outlier Filtering
            for k_S in np.arange(9):
                copy_S = np.zeros(framelen_track, dtype=np.float64)
                for k_len in np.arange(framelen_track):
                    copy_S[k_len] = Back_EKF_u[k_len][k_S, 0]

                # calculate summary statistics
                data_median, data_std = np.median(copy_S), np.std(copy_S)
                # identify outliers
                cut_off = data_std * 2
                lower, upper = data_median - cut_off, data_median + cut_off
                # filter outliers
                for k_len in np.arange(framelen_track):
                    if Back_EKF_u[k_len][k_S, 0] < lower or Back_EKF_u[k_len][k_S, 0] > upper:
                        Back_EKF_u[k_len][k_S, 0] = data_median
            # endregion

            # region Backward smoothing
            for k_back in np.arange(framelen_track - 1, -1, -1):
                if k_back == framelen_track - 1:
                    # EKF initialization
                    EKF_S = Back_EKF_u[k_back]  # 9*1
                    # State vector estimation error covariance matrix
                    EKF_P = Back_EKF_P[k_back]
                else:
                    Back_C_j = np.dot(np.dot(Back_EKF_P[k_back], EKF_F.T), np.linalg.inv(Back_EKF_P_apr[k_back]))
                    Back_y_j = EKF_S - np.dot(EKF_F, Back_EKF_u[k_back])
                    Back_est_u_j = Back_EKF_u[k_back] + np.dot(Back_C_j, Back_y_j)
                    Back_est_v_j = Back_EKF_P[k_back] + \
                                   np.dot(np.dot(Back_C_j, (EKF_P - Back_EKF_P_apr[k_back])), Back_C_j.T)
                    EKF_S = Back_est_u_j
                    EKF_P = Back_est_v_j

                trackLet_Point_backward[k_back, 0] = trackLet_Point_forward[k_back, 0]
                for k_assign in np.arange(1, 10):
                    trackLet_Point_backward[k_back, k_assign] = EKF_S[k_assign - 1, 0]
                for k_assign in np.arange(10, 15):
                    trackLet_Point_backward[k_back, k_assign] = trackLet_Point_forward[k_back, k_assign]
            # endregion
            # print(trackLet_Point_backward)

            # Update trackLet
            trackLet = np.vstack((trackLet, trackLet_Point_backward))
        # endregion
    return count, trackLet


@cc.export('EKF_Predict_MOT', 'Tuple((f8[:, :],f8[:, :]))(i8, '
                              'f8[:, :], f8[:, :], f8[:, :], '
                              'f8[:, :, :], '
                              'f8, f8)')
def EKF_Predict_MOT(k,
                    tracks_position, tracks_velocity, tracks_acceleration,
                    tracks_covariance_matrix,
                    Ts, mov_framelen_track):
    # Predict with previous measurements
    # region EKF_F
    EKF_F = np.array([[1., 0., 0., Ts * mov_framelen_track, 0., 0.,
                       0.5 * ((Ts * mov_framelen_track) ** 2), 0., 0.],
                      [0., 1., 0., 0., Ts * mov_framelen_track, 0., 0.,
                       0.5 * ((Ts * mov_framelen_track) ** 2), 0.],
                      [0., 0., 1., 0., 0., Ts * mov_framelen_track, 0., 0.,
                       0.5 * ((Ts * mov_framelen_track) ** 2)],
                      [0., 0., 0., 1., 0., 0., Ts * mov_framelen_track, 0., 0.],
                      [0., 0., 0., 0., 1., 0., 0., Ts * mov_framelen_track, 0.],
                      [0., 0., 0., 0., 0., 1., 0., 0., Ts * mov_framelen_track],
                      [0., 0., 0., 0., 0., 0., 1., 0., 0.],
                      [0., 0., 0., 0., 0., 0., 0., 1., 0.],
                      [0., 0., 0., 0., 0., 0., 0., 0., 1.]])  # 9x9
    # endregion

    # region EKF_Q
    EKF_Q = np.array([[np.power(Ts * mov_framelen_track, 4) / 4, 0., 0.,
                       np.power(Ts * mov_framelen_track, 3) / 2, 0., 0.,
                       np.power(Ts * mov_framelen_track, 2) / 2, 0., 0.],
                      [0., np.power(Ts * mov_framelen_track, 4) / 4, 0., 0.,
                       np.power(Ts * mov_framelen_track, 4) / 2, 0., 0.,
                       np.power(Ts * mov_framelen_track, 2) / 2, 0.],
                      [0., 0., np.power(Ts * mov_framelen_track, 4) / 4, 0., 0.,
                       np.power(Ts * mov_framelen_track, 3) / 2, 0., 0.,
                       np.power(Ts * mov_framelen_track, 2) / 2],
                      [np.power(Ts * mov_framelen_track, 3) / 2, 0., 0.,
                       np.power(Ts * mov_framelen_track, 2), 0., 0.,
                       Ts * mov_framelen_track,
                       0., 0.],
                      [0., np.power(Ts * mov_framelen_track, 3) / 2, 0., 0.,
                       np.power(Ts * mov_framelen_track, 2), 0., 0.,
                       Ts * mov_framelen_track, 0.],
                      [0., 0., np.power(Ts * mov_framelen_track, 3) / 2, 0., 0.,
                       np.power(Ts * mov_framelen_track, 2), 0., 0.,
                       Ts * mov_framelen_track],
                      [np.power(Ts * mov_framelen_track, 2) / 2, 0., 0.,
                       Ts * mov_framelen_track, 0., 0., 1, 0., 0.],
                      [0., np.power(Ts * mov_framelen_track, 2) / 2, 0., 0.,
                       Ts * mov_framelen_track, 0., 0., 1, 0.],
                      [0., 0., np.power(Ts * mov_framelen_track, 2) / 2, 0., 0.,
                       Ts * mov_framelen_track, 0., 0., 1]]) * (2.0 ** 2)
    # endregion

    EKF_S = np.hstack((tracks_position[k, :], tracks_velocity[k, :], tracks_acceleration[k, :]))
    EKF_S = EKF_S.reshape(-1, 1)  # 9*1

    EKF_P = tracks_covariance_matrix[k, :, :]
    # Prediction Step
    EKF_S_apr = np.dot(EKF_F, EKF_S)
    # region Remove Outliers
    # Y-axis
    if EKF_S_apr[1, 0] < 0:
        EKF_S_apr[1, 0] = 0.1
    # vx
    if EKF_S_apr[3, 0] < -2.5:  # 2 m/s
        EKF_S_apr[3, 0] = -2.5
    if EKF_S_apr[3, 0] > 2.5:  # 2 m/s
        EKF_S_apr[3, 0] = 2.5
    # vy
    if EKF_S_apr[4, 0] < -2.5:  # 2 m/s
        EKF_S_apr[4, 0] = -2.5
    if EKF_S_apr[4, 0] > 2.5:  # 2 m/s
        EKF_S_apr[4, 0] = 2.5
    # vz
    if EKF_S_apr[5, 0] < -1.5:  # 1 m/s
        EKF_S_apr[5, 0] = -1.5
    if EKF_S_apr[5, 0] > 1.5:  # 1 m/s
        EKF_S_apr[5, 0] = 1.5
    # ax
    if EKF_S_apr[6, 0] < -1:  # 1.5 / (5*0.1) = 3 m/s2
        EKF_S_apr[6, 0] = -1
    if EKF_S_apr[6, 0] > 1:  # 1.5 / (5*0.1) = 3 m/s2
        EKF_S_apr[6, 0] = 1
    # ay
    if EKF_S_apr[7, 0] < -1:  # 1.5 / (5*0.1) = 3 m/s2
        EKF_S_apr[7, 0] = -1
    if EKF_S_apr[7, 0] > 1:  # 1.5 / (5*0.1) = 3 m/s2
        EKF_S_apr[7, 0] = 1
    # az
    if EKF_S_apr[8, 0] < -1:  # 1 / (5*0.1) = 2 m/s2
        EKF_S_apr[8, 0] = -1
    if EKF_S_apr[8, 0] > 1:  # 1 / (5*0.1) = 2 m/s2
        EKF_S_apr[8, 0] = 1
    # endregion

    EKF_P_apr = np.dot(np.dot(EKF_F, EKF_P), EKF_F.T) + EKF_Q

    return EKF_S_apr, EKF_P_apr


@cc.export('Compute_Cost', 'f8[:, :](f8[:, :, :], f8[:, :, :], i8, f8)')
def Compute_Cost(pre_trackLet, curr_trackLet, mov_framelen_track, Ts):
    cost = np.zeros((len(pre_trackLet), len(curr_trackLet)), dtype=np.float64)
    for i in range(np.shape(pre_trackLet)[0]):
        copy_pre_trackLet = pre_trackLet[i, :, :]
        for j in range(np.shape(curr_trackLet)[0]):
            copy_curr_trackLet = curr_trackLet[j, :, :]

            x = np.median((copy_pre_trackLet[:, 1] - copy_curr_trackLet[:, 1]) ** 2)
            y = np.median((copy_pre_trackLet[:, 2] - copy_curr_trackLet[:, 2]) ** 2)
            z = np.median((copy_pre_trackLet[:, 3] - copy_curr_trackLet[:, 3]) ** 2)
            vx = np.median(((copy_pre_trackLet[:, 4] - copy_curr_trackLet[:, 4]) * mov_framelen_track * Ts) ** 2)
            vy = np.median(((copy_pre_trackLet[:, 5] - copy_curr_trackLet[:, 5]) * mov_framelen_track * Ts) ** 2)
            vz = np.median(((copy_pre_trackLet[:, 6] - copy_curr_trackLet[:, 6]) * mov_framelen_track * Ts) ** 2)
            accx = np.median(
                ((copy_pre_trackLet[:, 7] - copy_curr_trackLet[:, 7]) * 0.5 * mov_framelen_track * Ts ** 2) ** 2)
            accy = np.median(
                ((copy_pre_trackLet[:, 8] - copy_curr_trackLet[:, 8]) * 0.5 * mov_framelen_track * Ts ** 2) ** 2)
            accz = np.median(
                ((copy_pre_trackLet[:, 9] - copy_curr_trackLet[:, 9]) * 0.5 * mov_framelen_track * Ts ** 2) ** 2)

            cost[i, j] = 0.6 * np.sqrt(x + y + z) + \
                         0.3 * np.sqrt(vx + vy + vz) + \
                         0.1 * np.sqrt(accx + accy + accz)

            # cost[i, j] = np.median(np.sqrt(np.sum((copy_pre_trackLet[:, np.arange(1, 4)] -
            #                                         copy_curr_trackLet[:, np.arange(1, 4)]) ** 2, axis=1)))
    return cost


@cc.export('EKF_Update_MOT', 'Tuple((f8[:, :],f8[:, :]))(i8,'
                             'f8[:], f8[:], f8[:], '
                             'f8[:, :], '
                             'f8[:, :, :], '
                             'i8)')
def EKF_Update_MOT(d,
                   tracks_position, tracks_velocity, tracks_acceleration,
                   tracks_covariance_matrix,
                   curr_trackLet,
                   framelen_track):
    ### Prediction Step ###
    # a. Compute the a -priori state estimate
    EKF_S_apr = np.hstack((tracks_position, tracks_velocity, tracks_acceleration)).reshape(-1, 1)
    EKF_P_apr = tracks_covariance_matrix

    # Measurement matrix based on Prediction
    EKF_H_s_apr = np.array([[
        np.sqrt(tracks_position[0] ** 2 + tracks_position[1] ** 2 + tracks_position[2] ** 2),
        np.arctan2(tracks_position[0], tracks_position[1]),
        np.arctan2(tracks_position[2], np.sqrt(tracks_position[0] ** 2 + tracks_position[1] ** 2)),
        (tracks_position[0] * tracks_velocity[0] +
         tracks_position[1] * tracks_velocity[1] +
         tracks_position[2] * tracks_velocity[2]) / \
        np.sqrt(tracks_position[0] ** 2 + tracks_position[1] ** 2 + tracks_position[2] ** 2)]]).T

    # Actual measurement
    u_opt = np.zeros(4, dtype=np.float64)
    for i in range(4):
        copy_SNR = 0
        copy_u = 0
        for j in range(framelen_track):
            copy_u += curr_trackLet[d, j, 10 + i] * curr_trackLet[d, j, 14]
            copy_SNR += curr_trackLet[d, j, 14]
        u_opt[i] = copy_u / copy_SNR

    EKF_u = np.array([[np.sqrt(u_opt[0] ** 2 + u_opt[1] ** 2 + u_opt[2] ** 2),
                       np.arctan2(u_opt[0], u_opt[1]),
                       np.arctan2(u_opt[2], np.sqrt(u_opt[0] ** 2 + u_opt[1] ** 2)),
                       u_opt[3]]]).T
    EKF_y = EKF_u - EKF_H_s_apr

    # region b. Compute innovation covariance
    # Measurement Covariance Matrix (This is a diagonal Matrix)
    EKF_R = np.array([[0.5, 0., 0., 0.],
                      [0., 0.3, 0., 0.],
                      [0., 0., 0.3, 0.],
                      [0., 0., 0., 1]]) ** 2
    # endregion

    # region Jacobian Matrix
    r_J = np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2 + EKF_S_apr[2, 0] ** 2)
    EKF_J_H = np.zeros((4, 9), dtype=np.float64)
    EKF_J_H[0, 0] = EKF_S_apr[0, 0] / r_J
    EKF_J_H[0, 1] = EKF_S_apr[1, 0] / r_J
    EKF_J_H[0, 2] = EKF_S_apr[2, 0] / r_J

    EKF_J_H[1, 0] = EKF_S_apr[1, 0] / (EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2)
    EKF_J_H[1, 1] = - EKF_S_apr[0, 0] / (EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2)

    EKF_J_H[2, 0] = - EKF_S_apr[0, 0] / (r_J ** 2) * \
                    EKF_S_apr[2, 0] / np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2)
    EKF_J_H[2, 1] = - EKF_S_apr[1, 0] / (r_J ** 2) * \
                    EKF_S_apr[2, 0] / np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2)
    EKF_J_H[2, 2] = np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2) / (r_J ** 2)

    EKF_J_H[3, 0] = (EKF_S_apr[1, 0] * (
            EKF_S_apr[3, 0] * EKF_S_apr[1, 0] - EKF_S_apr[4, 0] * EKF_S_apr[
        0, 0]) +
                     EKF_S_apr[2, 0] * (
                             EKF_S_apr[3, 0] * EKF_S_apr[2, 0] - EKF_S_apr[
                         5, 0] * EKF_S_apr[0, 0])) / \
                    np.power(r_J, 3)
    EKF_J_H[3, 1] = (EKF_S_apr[0, 0] * (
            EKF_S_apr[4, 0] * EKF_S_apr[0, 0] - EKF_S_apr[3, 0] * EKF_S_apr[
        1, 0]) +
                     EKF_S_apr[2, 0] * (
                             EKF_S_apr[4, 0] * EKF_S_apr[2, 0] - EKF_S_apr[
                         5, 0] * EKF_S_apr[1, 0])) / \
                    np.power(r_J, 3)
    EKF_J_H[3, 2] = (EKF_S_apr[0, 0] * (
            EKF_S_apr[5, 0] * EKF_S_apr[0, 0] - EKF_S_apr[3, 0] * EKF_S_apr[
        2, 0]) +
                     EKF_S_apr[1, 0] * (
                             EKF_S_apr[5, 0] * EKF_S_apr[1, 0] - EKF_S_apr[
                         4, 0] * EKF_S_apr[2, 0])) / \
                    np.power(r_J, 3)
    EKF_J_H[3, 3] = EKF_S_apr[0, 0] / r_J
    EKF_J_H[3, 4] = EKF_S_apr[1, 0] / r_J
    EKF_J_H[3, 5] = EKF_S_apr[2, 0] / r_J
    # endregion

    # Innovation covariance
    EKF_C = np.dot(np.dot(EKF_J_H, EKF_P_apr), EKF_J_H.T) + EKF_R

    # c. Compute Kalman gain
    EKF_K = np.dot(np.dot(EKF_P_apr, EKF_J_H.T), np.linalg.inv(EKF_C))

    # d. Compute a-posteriori state vector
    EKF_S = EKF_S_apr + np.dot(EKF_K, EKF_y)
    # region Remove Outliers
    # Y-axis
    if EKF_S[1, 0] < 0:
        EKF_S[1, 0] = 0.1
    # vx
    if EKF_S[3, 0] < -2.5:  # 2 m/s
        EKF_S[3, 0] = -2.5
    if EKF_S[3, 0] > 2.5:  # 2 m/s
        EKF_S[3, 0] = 2.5
    # vy
    if EKF_S[4, 0] < -2.5:  # 2 m/s
        EKF_S[4, 0] = -2.5
    if EKF_S[4, 0] > 2.5:  # 2 m/s
        EKF_S[4, 0] = 2.5
    # vz
    if EKF_S[5, 0] < -1.5:  # 1 m/s
        EKF_S[5, 0] = -1.5
    if EKF_S[5, 0] > 1.5:  # 1 m/s
        EKF_S[5, 0] = 1.5
    # ax
    if EKF_S[6, 0] < -1:  # 1.5 / (5*0.1) = 3 m/s2
        EKF_S[6, 0] = -1
    if EKF_S[6, 0] > 1:  # 1.5 / (5*0.1) = 3 m/s2
        EKF_S[6, 0] = 1
    # ay
    if EKF_S[7, 0] < -1:  # 1.5 / (5*0.1) = 3 m/s2
        EKF_S[7, 0] = -1
    if EKF_S[7, 0] > 1:  # 1.5 / (5*0.1) = 3 m/s2
        EKF_S[7, 0] = 1
    # az
    if EKF_S[8, 0] < -1:  # 1 / (5*0.1) = 2 m/s2
        EKF_S[8, 0] = -1
    if EKF_S[8, 0] > 1:  # 1 / (5*0.1) = 2 m/s2
        EKF_S[8, 0] = 1
    # endregion

    # e. Compute a-posteriori error covariance
    EKF_P = EKF_P_apr - np.dot(np.dot(EKF_K, EKF_J_H), EKF_P_apr)

    return EKF_S, EKF_P
# endregion



# region VS
# @cc.export('aoa_capon', 'Tuple((f8[:, :],f8[:]))(f8[:, :], f8[:, :])')
@cc.export('aoa_capon', 'Tuple((f8[:, :], c16[:, :, :]))(c16[:, :, :], c16[:, :], i8, i8)')
def aoa_capon(Foreground_vsign, steering_vec, gl_numAngleBins, gl_numRangeBins):
    rgAziheatmap = np.zeros((gl_numAngleBins, gl_numRangeBins), dtype=np.float64)
    weights = np.zeros((gl_numRangeBins, gl_numAngleBins, steering_vec.shape[1]), dtype=np.complex128)
    for rg_k in range(gl_numRangeBins):
        x = np.vstack((Foreground_vsign[:, rg_k, 4::2], Foreground_vsign[:, rg_k, 5::2])).T
        # x = np.transpose(x)

        # Covariance matrix
        _, num_adc_samples = x.shape
        Rxx = np.dot(x, np.conjugate(x.T))    # x @ np.conjugate(x.T)
        Rxx = np.divide(Rxx, num_adc_samples)

        # Forward-Backward smoothing
        M = len(Rxx)  # Find number of antenna elements
        J = np.eye(M)  # Generates an identity matrix with row/col size M
        J = np.fliplr(J) * np.ones((M, M), dtype=np.complex128)  # Flips the identity matrix left right
        Rxx = 0.5 * (Rxx + np.dot(np.dot(J, np.conjugate(Rxx)), J))  #R_fb = 0.5 * (Rxx + J * np.conjugate(Rxx) * J)

        # # Calculate Covariance Matrix Rxx
        Rxx_inv = np.linalg.pinv(Rxx)
        # Calculate Covariance Matrix Rxx steering_vec (181, 4)
        first = np.dot(Rxx_inv, steering_vec.T)  # (4, 181)   # Rxx_inv @ steering_vector.T
        den = 1 / np.sum(steering_vec.conj() * first.T, axis=1)  # (181, 1)
        weights[rg_k, :, :] = first.T * den.reshape(-1, 1)  # (181, 4)
        rgAziheatmap[:, rg_k] = np.abs(den)

    return rgAziheatmap, weights

@cc.export('CFAR', 'Tuple((i8[:], i8[:], f8[:]))(f8[:, :], i8, i8, f8)')
def CFAR(rgAziheatmap, guard_len, noise_len, pfa):
    N = 2 * noise_len
    alpha = N * (pfa ** (-1 / N) - 1)
    kernel = np.ones(1 + (2 * guard_len) + (2 * noise_len), dtype=np.float64) / (2 * noise_len)
    kernel[noise_len:noise_len + (2 * guard_len) + 1] = 0
    # Angle direction
    Angle_pass = np.zeros((rgAziheatmap.shape[0], rgAziheatmap.shape[1]), dtype=boolean)
    for k in range(rgAziheatmap.shape[1]):
        data = rgAziheatmap[:, k]
        x = np.hstack((data[:int((len(kernel) - 1) / 2)],
                       data,
                       data[-int((len(kernel) - 1) / 2):]))
        conv = np.convolve(x, kernel)
        threshold = conv[int((len(kernel) - 1) / 2) * 2:-int((len(kernel) - 1) / 2) * 2] * alpha
        Angle_pass[:, k] = (data > threshold)

    # Range direction
    Range_pass = np.zeros((rgAziheatmap.shape[0], rgAziheatmap.shape[1]), dtype=boolean)
    noise_floor_Range = np.zeros((rgAziheatmap.shape[0], rgAziheatmap.shape[1]), dtype=np.float64)
    for k in range(rgAziheatmap.shape[0]):
        data = rgAziheatmap[k, :]
        x = np.hstack((data[:int((len(kernel) - 1) / 2)],
                       data,
                       data[-int((len(kernel) - 1) / 2):]))
        conv = np.convolve(x, kernel)
        noise_floor_Range[k, :] = conv[int((len(kernel) - 1) / 2) * 2:-int((len(kernel) - 1) / 2) * 2]
        threshold = noise_floor_Range[k, :] * alpha
        Range_pass[k, :] = (data > threshold)

    # --- classify peaks and caclulate snrs
    peaks = (Angle_pass & Range_pass)
    SKIP_SIZE = 4
    peaks[:SKIP_SIZE, :] = 0
    peaks[-SKIP_SIZE:, :] = 0
    peaks[:, :SKIP_SIZE] = 0
    peaks[:, -SKIP_SIZE:] = 0
    pairs = np.argwhere(peaks)
    azimuths = pairs.T[0, :]
    ranges = pairs.T[1, :]

    if len(pairs) > 0:
        snrs = np.zeros(len(pairs), dtype=np.float64)
        for k in range(len(pairs)):
            r = pairs[k, 0]
            c = pairs[k, 1]
            snrs[k] = rgAziheatmap[r, c] - noise_floor_Range[r, c]
    else:
        snrs = np.empty(0, dtype=np.float64)

    return azimuths, ranges, snrs


@cc.export('EKF_Predict_VS', 'Tuple((f8[:, :],f8[:, :]))(f8[:], f8[:], f8[:], f8[:, :], f8)')
def EKF_Predict_VS(tracks_vsign_position,
                   tracks_vsign_velocity,
                   tracks_vsign_acceleration,
                   tracks_vsign_covariance_matrix,
                   Ts):
    # Predict with previous measurements
    EKF_S = np.hstack((tracks_vsign_position,
                       tracks_vsign_velocity,
                       tracks_vsign_acceleration))
    EKF_S = EKF_S.reshape(-1, 1)  # 9*1
    EKF_P = tracks_vsign_covariance_matrix

    # Prediction Step
    EKF_F = np.array([[1., 0., Ts, 0., 0.5 * (Ts ** 2), 0.],
                      [0., 1., 0., Ts, 0., 0.5 * (Ts ** 2)],
                      [0., 0., 1., 0., Ts, 0.],
                      [0., 0., 0., 1., 0., Ts],
                      [0., 0., 0., 0., 1., 0.],
                      [0., 0., 0., 0., 0., 1.]])  # 6x6
    # regionEKF_Q
    EKF_Q = np.array([[np.power(Ts, 4) / 4, 0., np.power(Ts, 3) / 2,
                       0., np.power(Ts, 2) / 2, 0.],
                      [0., np.power(Ts, 4) / 4, 0.,
                       np.power(Ts, 4) / 2,
                       0., np.power(Ts, 2) / 2],
                      [np.power(Ts, 3) / 2, 0.,
                       np.power(Ts, 2),
                       0., Ts, 0.],
                      [0., np.power(Ts, 3) / 2, 0.,
                       np.power(Ts, 2), 0., Ts],
                      [np.power(Ts, 2) / 2, 0.,
                       Ts,
                       0., 1, 0.],
                      [0., np.power(Ts, 2) / 2, 0.,
                       Ts, 0., 1]]) * (2 ** 2)
    # endregion
    EKF_S_apr = np.dot(EKF_F, EKF_S)
    # region Outliers
    # y-axis
    if EKF_S_apr[1, 0] < 0:
        EKF_S_apr[1, 0] = 0.1
    # vx
    if EKF_S_apr[2, 0] < -0.25:
        EKF_S_apr[2, 0] = -0.25
    if EKF_S_apr[2, 0] > 0.25:
        EKF_S_apr[2, 0] = 0.25
    # vy
    if EKF_S_apr[3, 0] < -0.25:
        EKF_S_apr[3, 0] = -0.25
    if EKF_S_apr[3, 0] > 0.25:
        EKF_S_apr[3, 0] = 0.25
    # ax
    if EKF_S_apr[4, 0] < -0.25:
        EKF_S_apr[4, 0] = -0.25
    if EKF_S_apr[4, 0] > 0.25:
        EKF_S_apr[4, 0] = 0.25
    # ay
    if EKF_S_apr[5, 0] < -0.25:
        EKF_S_apr[5, 0] = -0.25
    if EKF_S_apr[5, 0] > 0.25:
        EKF_S_apr[5, 0] = 0.25
    # endregion
    EKF_P_apr = np.dot(np.dot(EKF_F, EKF_P), EKF_F.T) + EKF_Q

    return EKF_S_apr, EKF_P_apr


@cc.export('Compute_Cost_VS', 'f8[:, :](f8[:, :], f8[:, :])')
def Compute_Cost_VS(pre_trackLet, curr_trackLet):
    cost = np.zeros((len(pre_trackLet), len(curr_trackLet)), dtype=np.float64)
    for i in range(np.shape(pre_trackLet)[0]):
        for j in range(np.shape(curr_trackLet)[0]):
            delta_x = (pre_trackLet[i, 1] - curr_trackLet[j, 1]) ** 2
            delta_y = (pre_trackLet[i, 2] - curr_trackLet[j, 2]) ** 2
            cost[i, j] = np.sqrt(delta_x + delta_y)
    return cost

@cc.export('EKF_Update_VS', 'Tuple((f8[:, :],f8[:, :]))('
                             'f8[:], f8[:], f8[:], '
                             'f8[:, :], '
                             'f8[:])')
def EKF_Update_VS(tracks_vsign_position,
                  tracks_vsign_velocity,
                  tracks_vsign_acceleration,
                  tracks_vsign_covariance_matrix,
                  curr_trackLet_vsign):

    EKF_S_apr = np.hstack((tracks_vsign_position,
                       tracks_vsign_velocity,
                       tracks_vsign_acceleration))
    EKF_S_apr = EKF_S_apr.reshape(-1, 1)  # 9*1
    EKF_P_apr = tracks_vsign_covariance_matrix

    # 2. Update Step
    # a. Compute measurement residual (i.e., innovation)
    # measurement from prediction
    EKF_H_s_apr = np.array([[
        np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2),
        np.arctan2(EKF_S_apr[0, 0], EKF_S_apr[1, 0]),
        (EKF_S_apr[0, 0] * EKF_S_apr[2, 0] + EKF_S_apr[1, 0] * EKF_S_apr[3, 0]) / \
        np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2)]]).T

    # Actual measurement
    EKF_Pos_X = curr_trackLet_vsign[1]
    EKF_Pos_Y = curr_trackLet_vsign[2]
    EKF_u =np.array([[np.sqrt(EKF_Pos_X ** 2 + EKF_Pos_Y ** 2),
                      np.arctan2(EKF_Pos_X, EKF_Pos_Y),
                      0.1]]).T
    EKF_y = EKF_u - EKF_H_s_apr

    # b. Compute innovation covariance
    # Measurement Covariance Matrix (This is a diagonal Matrix)
    EKF_R = np.array([[0.5, 0., 0.],
                      [0., 0.3, 0.],
                      [0., 0., 0.5]]) ** 2

    # region Jacobian Matrix
    EKF_J_H = np.zeros((3, 6), dtype=np.float64)
    r_J = np.sqrt(EKF_S_apr[0, 0] ** 2 + EKF_S_apr[1, 0] ** 2)

    EKF_J_H[0, 0] = EKF_S_apr[0, 0] / r_J
    EKF_J_H[0, 1] = EKF_S_apr[1, 0] / r_J

    EKF_J_H[1, 0] = EKF_S_apr[1, 0] / (r_J ** 2)
    EKF_J_H[1, 1] = - EKF_S_apr[0, 0] / (r_J ** 2)

    EKF_J_H[2, 0] = (EKF_S_apr[1, 0] * (
            EKF_S_apr[2, 0] * EKF_S_apr[1, 0] - EKF_S_apr[3, 0] * EKF_S_apr[
        0, 0])) / np.power(r_J, 3 / 2)
    EKF_J_H[2, 1] = (EKF_S_apr[0, 0] * (
            EKF_S_apr[3, 0] * EKF_S_apr[0, 0] - EKF_S_apr[2, 0] * EKF_S_apr[
        1, 0])) / np.power(r_J, 3 / 2)
    EKF_J_H[2, 3] = EKF_S_apr[0, 0] / r_J
    EKF_J_H[2, 4] = EKF_S_apr[1, 0] / r_J
    # endregion

    # Group residual Covariance Matrix
    EKF_C = np.dot(np.dot(EKF_J_H, EKF_P_apr), EKF_J_H.T) + EKF_R

    # c. Compute Kalman gain
    EKF_K = np.dot(np.dot(EKF_P_apr, EKF_J_H.T), np.linalg.inv(EKF_C))

    # d. Compute a-posteriori state vector
    EKF_S = EKF_S_apr + np.dot(EKF_K, EKF_y)
    # region Outliers
    # y-axis
    if EKF_S[1, 0] < 0:
        EKF_S[1, 0] = 0.1
    # vx
    if EKF_S[2, 0] < -0.25:
        EKF_S[2, 0] = -0.25
    if EKF_S[2, 0] > 0.25:
        EKF_S[2, 0] = 0.25
    # vy
    if EKF_S[3, 0] < -0.25:
        EKF_S[3, 0] = -0.25
    if EKF_S[3, 0] > 0.25:
        EKF_S[3, 0] = 0.25
    # ax
    if EKF_S[4, 0] < -0.25:
        EKF_S[4, 0] = -0.25
    if EKF_S[4, 0] > 0.25:
        EKF_S[4, 0] = 0.25
    # ay
    if EKF_S[5, 0] < -0.25:
        EKF_S[5, 0] = -0.25
    if EKF_S[5, 0] > 0.25:
        EKF_S[5, 0] = 0.25
    # endregion

    # e. Compute a-posteriori error covariance
    EKF_P = EKF_P_apr - np.dot(np.dot(EKF_K, EKF_J_H), EKF_P_apr)

    return EKF_S, EKF_P


@cc.export('Height_VS', 'Tuple((i8, f8, f8, f8))(i8, f8[:, :], c16[:, :, :], c16[:, :], f8[:], f8[:], f8)')
def Height_VS(k, tracks_vsign_position, cmplx_Foreground_all, steering_vec, rangeArray, thetaArray, radar_Height):
    # Position of Static Person
    Pos_x = tracks_vsign_position[k, 0]
    Pos_y = tracks_vsign_position[k, 1]
    ind_bin = int(np.argmin(np.abs(rangeArray - np.sqrt(Pos_x ** 2 + Pos_y ** 2))))

    # Height of Static Person
    copy_cmplx_Foreground = cmplx_Foreground_all[:, ind_bin, :]
    if (ind_bin - 1) >= 0:
        copy_cmplx_Foreground += cmplx_Foreground_all[:, ind_bin - 1, :]
    if (ind_bin + 1) <= copy_cmplx_Foreground.shape[1]:
        copy_cmplx_Foreground += cmplx_Foreground_all[:, ind_bin + 1, :]

    copy_cmplx_Foreground[:, np.arange(0, 11, 2)] = -1 * copy_cmplx_Foreground[:, np.arange(0, 11, 2)]
    x = np.vstack((copy_cmplx_Foreground[:, np.array([10, 11, 2, 3])],
                   copy_cmplx_Foreground[:, np.array([8, 9, 0, 1])])).T
    # x = copy_cmplx_Foreground[:, np.array([10, 11, 2, 3])].T

    # Covariance matrix
    _, num_adc_samples = x.shape
    Rxx = np.dot(x, np.conjugate(x.T))  # x @ np.conjugate(x.T)
    Rxx = np.divide(Rxx, num_adc_samples)

    # Forward-Backward smoothing
    M = len(Rxx)  # Find number of antenna elements
    J = np.eye(M)  # Generates an identity matrix with row/col size M
    J = np.fliplr(J) * np.ones((M, M), dtype=np.complex128)  # Flips the identity matrix left right
    Rxx = 0.5 * (Rxx + np.dot(np.dot(J, np.conjugate(Rxx)), J))  # R_fb = 0.5 * (Rxx + J * np.conjugate(Rxx) * J)

    # # Calculate Covariance Matrix Rxx
    Rxx_inv = np.linalg.pinv(Rxx)
    # Calculate Covariance Matrix Rxx steering_vec (181, 4)
    first = np.dot(Rxx_inv, steering_vec.T)  # (4, 181)   # Rxx_inv @ steering_vector.T
    rgEleheatmap = np.abs(1 / np.sum(steering_vec.conj() * first.T, axis=1))  # (181, 1)

    # Height
    rgEleheatmap[:20] = 0
    rgEleheatmap[-20:] = 0
    el = thetaArray[int(np.argmax(rgEleheatmap))] * (np.pi / 180)
    Height = radar_Height + rangeArray[ind_bin] * np.sin(el)

    az = np.arctan(Pos_x / Pos_y)
    if np.sin(az) / np.cos(el) > 1:
        phi = np.arcsin(0.9)
    elif np.sin(az) / np.cos(el) < -1:
        phi = np.arcsin(-0.9)
    else:
        phi = np.arcsin(np.sin(az) / np.cos(el))
    dist = np.sqrt(Pos_x ** 2 + Pos_y ** 2)
    Pos_x = dist * np.sin(phi)
    Pos_y = dist * np.cos(phi)

    return ind_bin, Height, Pos_x, Pos_y


@cc.export('PhaseDiff_VS', 'f8[:, :](i8, c16[:, :, :], i8, i8, i8)')
def PhaseDiff_VS(ind_bin, cmplx_Foreground_all, gl_numVirtAnt, gl_numRangeBins, Len_cmplx):
    cmplx_Foreground = cmplx_Foreground_all[:, ind_bin, :]
    if ind_bin > 0 and ind_bin < gl_numRangeBins - 1:
        cmplx_Foreground += cmplx_Foreground_all[:, ind_bin - 1, :]
        cmplx_Foreground += cmplx_Foreground_all[:, ind_bin + 1, :]

    phase_Foreground = np.zeros((gl_numVirtAnt, Len_cmplx), dtype=np.float64)  # gl_numVirtAnt * framelen_vital
    for k_phase in range(gl_numVirtAnt):
        phase_Foreground[k_phase, :] = np.angle(cmplx_Foreground[:, k_phase])

    phase_diff = phase_Foreground[:, np.arange(1, Len_cmplx)] - \
                 phase_Foreground[:, np.arange(0, Len_cmplx - 1)]
    for k_numVirtAnt in range(gl_numVirtAnt):
        for k_diff in range(Len_cmplx - 1):
            if phase_diff[k_numVirtAnt, k_diff] > np.pi:
                phase_diff[k_numVirtAnt, k_diff] = phase_diff[k_numVirtAnt, k_diff] - 2 * np.pi
            elif phase_diff[k_numVirtAnt, k_diff] < - np.pi:
                phase_diff[k_numVirtAnt, k_diff] = phase_diff[k_numVirtAnt, k_diff] + 2 * np.pi
            else:
                phase_diff[k_numVirtAnt, k_diff] = phase_diff[k_numVirtAnt, k_diff]

    return phase_diff


@njit()
@cc.export('binom', 'i8(i8, i8)')
def binom(n, k):
    if 0 <= k <= n:
        a = 1
        b = 1
        for t in range(1, min(k, n - k) + 1):
            a *= n
            b *= t
            n -= 1
        return a // b
    else:
        return 0


@njit()
@cc.export('derivative_n2', 'f8[:](f8[:], f8, i8)')
def derivative_n2(data, dt, filter_length):
    M = int((filter_length - 1) / 2)
    m = int((filter_length - 3) / 2)
    coefs = np.zeros(M, dtype=np.float64)
    for k in range(1, M + 1):
        coefs[k - 1] = (1 / 2 ** (2 * m + 1)) * (binom(2 * m, m - k + 1) - binom(2 * m, m - k - 1))

    result = np.zeros(len(data), dtype=np.float64)
    for i in range(M, len(data) - M):
        copy_result = np.zeros(M, dtype=np.float64)
        for k in range(1, M + 1):
            copy_result[k - 1] = coefs[k - 1] * (data[i + k] - data[i - k])
        result[i] = (1 / dt) * np.sum(copy_result)
    return result

@cc.export('Noise_Suppression', 'f8[:, :](f8[:, :], i8, i8, f8)')
def Noise_Suppression(phase_diff, gl_numVirtAnt, Len_cmplx, Ts):
    # Minimize the impact of noise
    phase_diff_smoothed = np.zeros((gl_numVirtAnt, Len_cmplx - 1 - 4), dtype=np.float64)
    for k_heartbeat in range(gl_numVirtAnt):
        phase_diff_smoothed[k_heartbeat, :] = \
            derivative_n2(phase_diff[k_heartbeat, :], Ts, 5)[np.arange(2, Len_cmplx - 1 - 2)]
    return phase_diff_smoothed

#endregion



if __name__ == "__main__":
    cc.compile()
