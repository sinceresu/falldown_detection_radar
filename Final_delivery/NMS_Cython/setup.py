from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy as np

ext = [Extension("NMS_mmWave", sources=["NMS_mmWave.pyx"])]

setup(
    name="NMS_mmWave",
    cmdclass={'build_ext': build_ext},
    include_dirs=[np.get_include()],
    ext_modules=ext
)
